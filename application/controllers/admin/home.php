<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Home extends CI_Controller {
	function __construct() {
        parent::__construct();

        if($this->session->userdata('user_id') == NULL){
            redirect("/access/login_logout/login", 'location');
        }else{
            if($this->session->userdata('admin_access') == 0){
                show_error('Unauthorized Access');
                
            }
        }
        
        //load models
       $this->load->model('trans_model');
       $this->load->model('user_model');
       $this->load->helper('topup_pay');
    }
    public function index() {
    	//Prepare filters
    	$and_filters = array();
    	$or_filters = array();
    	if($_GET != null){
    		//payment
	    	if($_GET['pay_method'] != ''){
	    		if($_GET['pay_method'] == 'creditCard'){
	    			$or_filters['pay_method'] = 'mastercard';
	    			$or_filters['pay_method'] = 'visa';
	    		}else{
	    			$and_filters['pay_method'] = $_GET['pay_method'];	
	    		}
	    	}

	    	//status
	    	if($_GET['status'] != ''){
	    		$and_filters['status'] = $_GET['status'];	
	    	}

	    	//dates
	    	if($_GET['from'] != ''){
	    		$and_filters['time >='] = trim($_GET['from'])." 00:00:00.000000";
	    	}

	    	if($_GET['to'] != ''){
	    		$and_filters['time <='] = trim($_GET['to'])." 23:59:59.000000";
	    	}

	    	//operators
	    	if($_GET['operator'] != ''){
	    		$and_filters['dest_operator_name'] = $_GET['operator'];
	    	}

    	}
    	//get all tranactions
    	$data['trans'] = $this->trans_model->getAll($and_filters,$or_filters);
        $data['title'] = 'Admin | Home';
    	$data['login'] = 'true';

    	//load views
    	$this->load->view('templates/header',$data);
    	$this->load->view('admin/homeView');
    	$this->load->view('templates/footer');
    }



    public function edit(){
    	//get the  transaction
    	$data['trans'] = $this->trans_model->get_for_edit($_GET['id']);
    	$data['title'] = 'Admin | Home';
        $data['login'] = 'true';

    	
	   	$this->load->view('templates/header',$data);
    	$this->load->view('admin/edit_trans');
    	$this->load->view('templates/footer');
    }

    public function update($id){
    	$this->trans_model->update_pay($id,$_POST);

    	redirect('admin/home/edit?action=true&id='.$id, 'location');
    }


    public function doTrans(){
        //get the transaction
        $data_invoice = $this->trans_model-> get_trans_id($_GET['id']);
        if($data_invoice['dest_operator_name'] == 'tigo'){
            $vendor = ucfirst($data_invoice['dest_operator_name']);
            }else if($data_invoice['dest_operator_name'] == 'airtel'){
                $vendor = 'comviva';
            }else{
                $vendor = $data_invoice['dest_operator_name'];
            }
            $customer_num = $data_invoice['number'];
            $amount = 200;

        $result = send_credit($vendor,$customer_num,$amount);
                
        if($result['ResponseMessage'] == 0){
            $trns = array(
                'transaction_id_max' => $result['TransactionId'],
                'status'=> 'delivered'
            );
            $this->trans_model->update_pay($data_invoice['trans_id'],$trns);
            redirect('admin/home/edit?action=success&id='.$data_invoice['trans_id'], 'location');
        }else{
            echo 'transaction fail';
            die();
        }
    }


    public function statistics(){
        //get tranxaction data
        //Get the networks delivered
        $delivered  = count($this->trans_model->getAll(array('status' => 'delivered'), null));
        $tigo = count($this->trans_model->getAll(array('dest_operator_name' => 'tigo','status' => 'delivered'), null));
        $vodacom = count($this->trans_model->getAll(array('dest_operator_name' => 'vodacom','status' => 'delivered'), null));
        $airtel = count($this->trans_model->getAll(array('dest_operator_name' => 'airtel','status' => 'delivered'), null));
        $zantel = count($this->trans_model->getAll(array('dest_operator_name' => 'zantel','status' => 'delivered'), null));
        
        $data['total_trans'] = $delivered;
        $data['network_data'] = array(
                'tigo' => round(($tigo/$delivered)*100,2),
                'vodacom' => round(($vodacom/$delivered)*100,2),
                'airtel' => round(($airtel/$delivered)*100,2),
                'zantel' => round(($zantel/$delivered)*100,2),
            );

        //getl networks all
        $data['all']  = count($this->trans_model->getAll(null, null));
        $data['network_data_all'] = $this->all_transaction();

        //get charge data
        $data['total_charge'] = $this->trans_model->total_charges();
        $data['charge'] = array(
            'tigo' => $this->monthly_charges('tigo'),
            'vodacom' => $this->monthly_charges('vodacom'),
            'airtel' => $this->monthly_charges('airtel'),
            'zantel' => $this->monthly_charges('zantel')
        );

        //get status data
        $data['month_trans_stat'] =  array(
            'proccesing' => $this->monthly_no_stat('proccesing'),
            'pending' => $this->monthly_no_stat('pending'),
            'delivered' => $this->monthly_no_stat('delivered')
        );


        //get volume
        $data['volume'] = $this->monthly_volume();
        $data['total_volume'] = $this->trans_model->total_volume();

        //user data
        $data['user_volume'] = $this->user_data();
        $data['total_user_volume'] = $this->user_model->get_all();
        //prepare data
        $data['title'] = 'TopUP | statistics';

        //load views
        $this->load->view('templates/header',$data);
        $this->load->view('admin/statsView');
        $this->load->view('templates/footer');
    }



    public function monthly_charges($operator){
        $charges  = array(
            '1' => $this->trans_model->monthly_charges_network(array('dest_operator_name' => $operator,'status' => 'delivered'),array('time' => '2014-01')),
            '2' => $this->trans_model->monthly_charges_network(array('dest_operator_name' => $operator,'status' => 'delivered'),array('time' => '2014-02')),
            '3' => $this->trans_model->monthly_charges_network(array('dest_operator_name' => $operator,'status' => 'delivered'),array('time' => '2014-03')),
            '4' => $this->trans_model->monthly_charges_network(array('dest_operator_name' => $operator,'status' => 'delivered'),array('time' => '2014-04')),
            '5' => $this->trans_model->monthly_charges_network(array('dest_operator_name' => $operator,'status' => 'delivered'),array('time' => '2014-05')),
            '6' => $this->trans_model->monthly_charges_network(array('dest_operator_name' => $operator,'status' => 'delivered'),array('time' => '2014-06')),
            '7' => $this->trans_model->monthly_charges_network(array('dest_operator_name' => $operator,'status' => 'delivered'),array('time' => '2014-07')),
            '8' => $this->trans_model->monthly_charges_network(array('dest_operator_name' => $operator,'status' => 'delivered'),array('time' => '2014-08')),
            '9' => $this->trans_model->monthly_charges_network(array('dest_operator_name' => $operator,'status' => 'delivered'),array('time' => '2014-09')),
            '10' => $this->trans_model->monthly_charges_network(array('dest_operator_name' => $operator,'status' => 'delivered'),array('time' => '2014-10')),
            '11' => $this->trans_model->monthly_charges_network(array('dest_operator_name' => $operator,'status' => 'delivered'),array('time' => '2014-11')),
            '12' => $this->trans_model->monthly_charges_network(array('dest_operator_name' => $operator,'status' => 'delivered'),array('time' => '2014-12')),
        );

        return $charges;
    }


    public function monthly_volume(){
        $volume  = array(
            '1' => $this->trans_model->monthly_volume(array('status' => 'delivered'),array('time' => '2014-01')),
            '2' => $this->trans_model->monthly_volume(array('status' => 'delivered'),array('time' => '2014-02')),
            '3' => $this->trans_model->monthly_volume(array('status' => 'delivered'),array('time' => '2014-03')),
            '4' => $this->trans_model->monthly_volume(array('status' => 'delivered'),array('time' => '2014-04')),
            '5' => $this->trans_model->monthly_volume(array('status' => 'delivered'),array('time' => '2014-05')),
            '6' => $this->trans_model->monthly_volume(array('status' => 'delivered'),array('time' => '2014-06')),
            '7' => $this->trans_model->monthly_volume(array('status' => 'delivered'),array('time' => '2014-07')),
            '8' => $this->trans_model->monthly_volume(array('status' => 'delivered'),array('time' => '2014-08')),
            '9' => $this->trans_model->monthly_volume(array('status' => 'delivered'),array('time' => '2014-09')),
            '10' => $this->trans_model->monthly_volume(array('status' => 'delivered'),array('time' => '2014-10')),
            '11' => $this->trans_model->monthly_volume(array('status' => 'delivered'),array('time' => '2014-11')),
            '12' => $this->trans_model->monthly_volume(array('status' => 'delivered'),array('time' => '2014-12')),

        );

        return $volume;
    }


    public function user_data(){

        $volume['1'] = $this->user_model->mon_register(null,array('time' => '2014-01'));
        $volume['2'] = $this->user_model->mon_register(null,array('time' => '2014-02')) + $volume['1'];
        $volume['3'] = $this->user_model->mon_register(null,array('time' => '2014-03')) + $volume['2'];
        $volume['4'] = $this->user_model->mon_register(null,array('time' => '2014-04')) + $volume['3'];
        $volume['5'] = $this->user_model->mon_register(null,array('time' => '2014-05')) + $volume['4'];
        $volume['6'] = $this->user_model->mon_register(null,array('time' => '2014-06')) + $volume['5'];
        $volume['7'] = $this->user_model->mon_register(null,array('time' => '2014-07')) + $volume['6'];
        $volume['8'] = $this->user_model->mon_register(null,array('time' => '2014-08')) + $volume['7'];
        $volume['9'] = $this->user_model->mon_register(null,array('time' => '2014-09')) + $volume['8'];
        $volume['10'] = $this->user_model->mon_register(null,array('time' => '2014-10')) + $volume['9'];
        $volume['11'] = $this->user_model->mon_register(null,array('time' => '2014-11')) + $volume['10'];
        $volume['12'] = $this->user_model->mon_register(null,array('time' => '2014-12')) + $volume['11'];


        return $volume;
    }


    public function all_transaction(){
        $delivered  = count($this->trans_model->getAll(null, null));
        $tigo = count($this->trans_model->getAll(array('dest_operator_name' => 'tigo'), null));
        $vodacom = count($this->trans_model->getAll(array('dest_operator_name' => 'vodacom'), null));
        $airtel = count($this->trans_model->getAll(array('dest_operator_name' => 'airtel'), null));
        $zantel = count($this->trans_model->getAll(array('dest_operator_name' => 'zantel'), null));
        
        $network_data_all = array(
                'tigo' => round(($tigo/$delivered)*100,2),
                'vodacom' => round(($vodacom/$delivered)*100,2),
                'airtel' => round(($airtel/$delivered)*100,2),
                'zantel' => round(($zantel/$delivered)*100,2),
            );


        return $network_data_all;
    }



    public function monthly_no_stat($status){
        $trans_no  = array(
            '1' => $this->trans_model->monthly_net_stat(array('status' => $status),array('time' => '2014-01')),
            '2' => $this->trans_model->monthly_net_stat(array('status' => $status),array('time' => '2014-02')),
            '3' => $this->trans_model->monthly_net_stat(array('status' => $status),array('time' => '2014-03')),
            '4' => $this->trans_model->monthly_net_stat(array('status' => $status),array('time' => '2014-04')),
            '5' => $this->trans_model->monthly_net_stat(array('status' => $status),array('time' => '2014-05')),
            '6' => $this->trans_model->monthly_net_stat(array('status' => $status),array('time' => '2014-06')),
            '7' => $this->trans_model->monthly_net_stat(array('status' => $status),array('time' => '2014-07')),
            '8' => $this->trans_model->monthly_net_stat(array('status' => $status),array('time' => '2014-08')),
            '9' => $this->trans_model->monthly_net_stat(array('status' => $status),array('time' => '2014-09')),
            '10' => $this->trans_model->monthly_net_stat(array('status' => $status),array('time' => '2014-10')),
            '11' => $this->trans_model->monthly_net_stat(array('status' => $status),array('time' => '2014-11')),
            '12' => $this->trans_model->monthly_net_stat(array('status' => $status),array('time' => '2014-12')),

        );

        return $trans_no;
    }
}