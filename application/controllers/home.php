<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Home extends CI_Controller {
    public function index($def) {
        if($this->session->userdata('user_id') == NULL){
            redirect("/access/login_logout/login", 'location');
        }
        
        //Prepare configuration values for the header
        $data['type'] = 'inverse';
        $data['login'] = TRUE;
        $data['title'] = 'TopUp | Home';
        $data['def'] = $def;
        
        //prepare tranaction data
        $this->load->model('trans_model');
        
        
        $id = $this->session->userdata('user_id');
        $result = $this->trans_model->get_trans($id);
        //
        
        if($result == NULL){
            $data['no_data'] = TRUE;
        }else{
            foreach ($result as $key => $value) {
                $this->load->model('operator_model');
                $result[$key]['dest_operator_id'] = $this->operator_model->match_operator($value['dest_operator_id']);
            }

            $data['trans'] = $result;
        }
        //prepare profile data
        

        //Loading header
        $this->load->view('/templates/header.php',$data);
        
        //loading home body
        $this->load->view('home.php');
        
        //load the footer
        //$this->load->view('/templates/footer.php');
    }
     
}