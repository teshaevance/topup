<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Message extends CI_Controller {
    function __construct() {
        parent::__construct();
        
        //load models
        $this->load->model('message_model');
    }
    public function index(){
        $this->form_validation->set_rules("name","Your name","required");
        $this->form_validation->set_rules("email","Your email","required|valid_email");
        $this->form_validation->set_rules("msg","Your message","required|max_length[200]");
        $this->form_validation->set_message('required','%s is required');
        if ($this->form_validation->run() == FALSE){
                echo validation_errors();        
        }else {
                $data = array(
                    'name' => $_POST['name'],
                    'email' => $_POST['email'],
                    'msg' => $_POST['msg']
                );
                
                $result = $this->message_model->new_msg($data);
                if($result){
                    echo 'true';
                }else{
                    echo 'Something is broken, try again';
                }
        }
    }
}