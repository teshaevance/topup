<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Reg extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->library('email');
        $this->load->model('user_model');
    }
    public function index(){
        $this->form_validation->set_message('required','%s is required');
        if ($this->form_validation->run('reg') == FALSE){
                $data['mode'] = 'sigup';
                $this->load->view('access/login_page',$data);        
        }else {
            $data = array(
                    'first_name' => $_POST['fname'],
                    'last_name' => $_POST['lname'],
                    'phone' => $_POST['phone'],
                    'email' => $_POST['email'],
                    'password' => $_POST['pass']
                );
            
                
                $res = $this->user_model->new_user($data);
                if($res != false){
                    $data['user_data'] = $this->user_model->match_user($_POST['email'],$_POST['pass']);
                    //positive results ? set session : redirect to login with error message
                    if($data['user_data'] != null){
                        $user_data = array(
                           'user_id'  => $data['user_data'][0]['user_id'],
                            'username'  => $data['user_data'][0]['email'],
                            'fname'  => $data['user_data'][0]['first_name'],
                            'lname'  => $data['user_data'][0]['last_name'],
                            'phone'  => $data['user_data'][0]['phone'],
                            'msg_id'  => $data['user_data'][0]['verify_msg_id'],
                            'stat'  => $data['user_data'][0]['verify_stat'],
                            'sms'  => $data['user_data'][0]['free_text']
                        );

                        $this->session->set_userdata($user_data);
                        //send confirmation sms;
                        if($this->session->userdata('phone') != ''){
                            $num = trim($this->session->userdata('phone'));
                            $fname = substr($this->session->userdata('fname'), 0, 10);
                            $msgid = send_verify_sms($num,$fname);
                            $data = array('verify_msg_id' => $msgid);
                            $this->user_model->update_field($this->session->userdata('user_id'),$data);
                            $this->session->set_userdata('msg_id',$msgid);
                            
                        }


                        //send email
                        $this->email->from('karibu@topup.co.tz','TopUp');
                        $this->email->to(trim($this->session->userdata('username'))); 
                        $data['msg'] = "WELCOME!<br/>Thank you for registering with www.topup.co.tz.<br/><br/>Regards,<br/>TopUp Team.";
                        $email = $this->load->view('templates/email',$data,TRUE);
                        $this->email->subject('Welcome to TopUp');
                        $this->email->message($email);  

                        $this->email->send();

                        //echo $this->email->print_debugger();

                        //redirect
                        redirect('/home/index/form', 'location');

                    }
                }else{
                    $data['reg_error'] = 'User exists!Try another email or Recover password';
                    $data['mode'] = 'sigup';
                    $this->load->view('access/login_page',$data);
                }
        }
    }
    
    public function fly(){
        //set operation to fly
        $view_data['operation'] = 'fly';
        $view_data['mode'] = 'sigup';
        //check session ? redirect to home controller : redirect to login form
        if(isset($_POST['email'])){
            if ($this->form_validation->run('reg') == FALSE){          
                $this->load->view('access/login_page',$view_data);
            }else{
            // sets the session variables upon positive results from db         
            $data = array(
                    'first_name' => $_POST['fname'],
                    'last_name' => $_POST['lname'],
                    'phone' => $_POST['phone'],
                    'email' => $_POST['email'],
                    'password' => $_POST['pass']
                );
                $res = $this->user_model->new_user($data);
                if($res != false){
                    $data['user_data'] = $this->user_model->match_user($_POST['email'],$_POST['pass']);
                    //positive results ? set session : redirect to login with error message
                    if($data['user_data'] != null){
                        $user_data = array(
                           'user_id'  => $data['user_data'][0]['user_id'],
                            'username'  => $data['user_data'][0]['email'],
                            'fname'  => $data['user_data'][0]['first_name'],
                            'lname'  => $data['user_data'][0]['last_name'],
                            'phone'  => $data['user_data'][0]['phone'],
                            'msg_id'  => $data['user_data'][0]['verify_msg_id'],
                            'stat'  => $data['user_data'][0]['verify_stat'],
                            'sms'  => $data['user_data'][0]['free_text']
                        );

                        $this->session->set_userdata($user_data);
                        //send confirmation sms;
                        if($this->session->userdata('phone') != ''){
                            $num = trim($this->session->userdata('phone'));
                            $fname = substr($this->session->userdata('fname'), 0, 10);
                            $msgid = send_verify_sms($num,$fname);
                            $data = array('verify_msg_id' => $msgid);
                            $this->user_model->update_field($this->session->userdata('user_id'),$data);
                            $this->session->set_userdata('msg_id',$msgid);
                            
                        }


                        //send email
                        $this->email->from('karibu@topup.co.tz','TopUp');
                        $this->email->to(trim($this->session->userdata('username'))); 
                        $data['msg'] = "WELCOME!<br/>Thank you for registering with www.topup.co.tz.<br/><br/>Regards,<br/>TopUp Team.";
                        $email = $this->load->view('templates/email',$data,TRUE);
                        $this->email->subject('Welcome to TopUp');
                        $this->email->message($email);  

                        $this->email->send();

                        //echo $this->email->print_debugger();

                        //redirect
                        redirect('recharge/confirm/index/true', 'location');

                }
                }else{
                    $view_data['reg_error'] = 'User exists!Try another email or Recover password';
                    $this->load->view('access/login_page',$view_data);
                }
                }
                }else{
                    $this->load->view('access/login_page',$view_data);
                }
        
    }



    public function recover(){
        $data['title'] = 'TopUp | Password Recovery';
        $this->load->view('templates/header',$data);
        $this->load->view('access/recoverView');
    }


    public function password_reset(){
        $this->form_validation->set_rules("email","Email","required|valid_email");
        $this->form_validation->set_message('required',' is required');

        if ($this->form_validation->run() == FALSE){                     
            $data['title'] = 'TopUp | Password Recovery';  
            $this->load->view('templates/header',$data);
            $this->load->view('access/recoverView');
        }else{
            $token = md5(microtime (TRUE)*100000);
           //hash token to be stored on db
           
           $token_to_db = hash('sha256',$token);
           //expire time to be stored on db
           $expire_time = date("Y-m-d H:i:s",time()+1800);
           //array for updating database
           $data = array(
               'token_expire' => $expire_time,
               'hashed_token' => $token_to_db 
           );

           $res = $this->user_model->set_recovery_token($_POST['email'],$data);
           if($res){
            //prep link
            $link = site_url()."/access/reg/new_password/".$data['hashed_token'];
            //send email
            $this->email->from('admin@topup.co.tz','TopUp');
            $this->email->to(trim($_POST['email'])); 
            $data['msg'] = "Click url link to reset passowrd or copy and paste it in your browser<br/><a href='>".$link."'>".$link."</a>";
            $email = $this->load->view('templates/email',$data,TRUE);
            $this->email->subject('TopUp password reset');
            $this->email->message($email);  

            $this->email->send();

            $data['msg'] = 'Email sent';
            $data['title'] = 'TopUp | Password Recovery';
            $this->load->view('templates/header',$data);
            $this->load->view('access/recoverView');

           }
        }
    }

    public function new_password($token){

        if($_POST == null){
            //check token life time
            $userdata = $this->user_model->match_field(array('hashed_token' => $token));
            if(date("Y-m-d H:i:s",time()) > $userdata['token_expire']){
                $data['error'] = 'Your recovery link has expired, please resend';
                $data['title'] = 'TopUp | password reset';
                $this->load->view('templates/header',$data);
                $this->load->view('access/changePassword');
            }else{
                $data['title'] = 'TopUp | password reset';
                $data['token'] = $token;
                $this->load->view('templates/header',$data);
                $this->load->view('access/changePassword');
            }
        }else{
            $this->form_validation->set_rules("password","Password","required|md5");
            $this->form_validation->set_rules("password_con","Password Confirmation","required|matches[password]");
            $this->form_validation->set_message('required','%s is required');

            if ($this->form_validation->run() == FALSE){   
                $data['token'] = $token;                 
                $data['title'] = 'TopUp | Password Reset';  
                $this->load->view('templates/header',$data);
                $this->load->view('access/changePassword');
            }else{
                $userdata = $this->user_model->match_field(array('hashed_token' => $token));
                $res = $this->user_model->update_field($userdata['user_id'],array('password' => $_POST['password']));
                if($res){
                    $data['token'] = $token;                 
                    $data['title'] = 'TopUp | Password Reset';
                    $data['msg'] = 'Password Changed';  
                    $this->load->view('templates/header',$data);
                    $this->load->view('access/changePassword');
                }
            }
    }

}
}