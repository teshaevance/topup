<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login_logout extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model('user_model');
    }
    public function login(){
        //check session ? redirect to home controller : redirect to login form
        if($this->session->userdata('user_id') != NULL){
            redirect("/home/index/form", 'location');
        }else if ($this->form_validation->run('login') == FALSE){          
                $this->load->view('access/login_page');
        }else{
            $data['user_data'] = $this->user_model->match_user($_POST['username'],$_POST['password']);
            //positive results ? set session : redirect to login with error message
            if($data['user_data'] != null){
                $user_data = array(
                   'user_id'  => $data['user_data'][0]['user_id'],
                   'username'  => $data['user_data'][0]['email'],
                   'fname'  => $data['user_data'][0]['first_name'],
                   'lname'  => $data['user_data'][0]['last_name'],
                   'phone'  => $data['user_data'][0]['phone'],
                    'msg_id'  => $data['user_data'][0]['verify_msg_id'],
                    'stat'  => $data['user_data'][0]['verify_stat'],
                    'sms'  => $data['user_data'][0]['free_text'],
                    'admin_access'  => $data['user_data'][0]['admin']
                );
                
                $this->session->set_userdata($user_data);
                if($user_data['admin_access'] == 1){
                    redirect('admin/home/', 'location');
                }else{
                    redirect('/home/index/form', 'location');
                }
            }else{
                $data['error'] = 'Wrong username or password';
                $this->load->view('access/login_page',$data);
            }
      }
 }
     
    public function fly(){
        //set operation to fly
        $view_data['operation'] = 'fly';
        //check session ? redirect to home controller : redirect to login form
        if(isset($_POST['username'])){
            if ($this->form_validation->run('login') == FALSE){          
                $this->load->view('access/login_page',$view_data);
            }else{
                // sets the session variables upon positive results from db         
                $data['user_data'] = $this->user_model->match_user($_POST['username'],$_POST['password']);
                //positive results ? set session : redirect to login with error message
                if($data['user_data'] != null){
                    $user_data = array(
                       'user_id'  => $data['user_data'][0]['user_id'],
                       'username'  => $data['user_data'][0]['email'],
                       'fname'  => $data['user_data'][0]['first_name'],
                       'lname'  => $data['user_data'][0]['last_name'],
                       'phone'  => $data['user_data'][0]['phone'],
                       'msg_id'  => $data['user_data'][0]['verify_msg_id'],
                        'stat'  => $data['user_data'][0]['verify_stat'],
                        'sms'  => $data['user_data'][0]['free_text']
                    );

                    $this->session->set_userdata($user_data);
                    redirect('recharge/confirm/index/true', 'location');
                }else{
                    $view_data['error'] = 'Wrong username or password';
                    $this->load->view('access/login_page',$view_data);
                }
            }
            }else{
                //set current order into session
                $this->session->set_userdata('cur_ord',$_POST); 
                $this->load->view('access/login_page',$view_data);
            }
    }
    
    public function social(){
        //check if user exists inthe database
        $existence = $data['user_data'] = $this->user_model->check_exist($_POST['email']);
        
        //check existence ?  login/create session : register 
        if($existence){
            $data['user_data'] = $this->user_model->match_user($_POST['email'],  md5('social'));
                //positive results ? set session : redirect to login with error message
                if($data['user_data'] != null){
                    $user_data = array(
                       'user_id'  => $data['user_data'][0]['user_id'],
                        'username'  => $data['user_data'][0]['email'],
                        'fname'  => $data['user_data'][0]['first_name'],
                        'lname'  => $data['user_data'][0]['last_name'],
                        'phone'  => $data['user_data'][0]['phone'],
                        'msg_id'  => $data['user_data'][0]['verify_msg_id'],
                        'stat'  => $data['user_data'][0]['verify_stat'],
                        'sms'  => $data['user_data'][0]['free_text']
                    );

                    $this->session->set_userdata($user_data);
                    $result['state'] = 'true'; 
                    if($this->session->userdata('cur_ord') != NULL){
                        $result['url'] = 'recharge/confirm/index/true';
                    }else{
                        $result['url'] = 'home/index/form';
                    }
                    echo json_encode($result);
                }
            
        }else{   
            //prepare data
            $pass = md5('social');
            $data = array(
                'first_name' => $_POST['fname'],
                'last_name' => $_POST['lname'],
                'email' => $_POST['email'],
                'password' => $pass
            );
            $res = $this->user_model->new_user($data);
            if($res != false){
                $data['user_data'] = $this->user_model->match_user($_POST['email'],$pass);
                //positive results ? set session : redirect to login with error message
                if($data['user_data'] != null){
                    $user_data = array(
                       'user_id'  => $data['user_data'][0]['user_id'],
                        'username'  => $data['user_data'][0]['email'],
                        'fname'  => $data['user_data'][0]['first_name'],
                        'lname'  => $data['user_data'][0]['last_name'],
                        'phone'  => $data['user_data'][0]['phone'],
                        'msg_id'  => $data['user_data'][0]['verify_msg_id'],
                        'stat'  => $data['user_data'][0]['verify_stat'],
                        'sms'  => $data['user_data'][0]['free_text']
                    );

                    $this->session->set_userdata($user_data);
                    
                    //send email
                        $this->email->from('karibu@topup.co.tz','TopUp');
                        $this->email->to(trim($this->session->userdata('username'))); 
                        $data['msg'] = "WELCOME!<br/>Thank you for registering with www.topup.co.tz.<br/><br/>Regards,<br/>TopUp Team.";
                        $email = $this->load->view('templates/email',$data,TRUE);
                        $this->email->subject('Welcome to TopUp');
                        $this->email->message($email);  

                        $this->email->send();
                        
                    $result['state'] = 'true';
                    if($this->session->userdata('cur_ord') != NULL){
                        $result['url'] = 'recharge/confirm/index/true';
                    }else{
                        $result['url'] = 'home/index/form';
                    }
                    echo json_encode($result);
                }
            }
        
        
    }
    
}
    
    public function logout(){
        $this->session->sess_destroy();
        redirect('landing','location');
    }
    
}