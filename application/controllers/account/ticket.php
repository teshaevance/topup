<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ticket extends CI_Controller {
    function __construct() {
        parent::__construct();
        
        //load models
        $this->load->model('ticket_model');
    }
    public function index(){
        $this->form_validation->set_rules("email","Your name","required|valid_email");
        $this->form_validation->set_rules("ref","Your order reference number","required");
        $this->form_validation->set_rules("msg","Your message","required|max_length[200]");
        $this->form_validation->set_message('required','%s is required');
        if ($this->form_validation->run() == FALSE){
                echo validation_errors();        
        }else {
                $ref = strtoupper(trim($_POST['ref']));
                $check = $this->form_validation->is_unique($ref,'transaction.transaction_id');
                if($check != 1){
                    $data = array(
                        'email' => $_POST['email'],
                        'order_ref' => strtoupper($_POST['ref']),
                        'msg' => $_POST['msg']
                    );

                    $result = $this->ticket_model->new_tkt($data);
                    if($result){
                        echo 'true';
                    }else{
                        echo 'Something is broken, try again';
                    }
                }else if($check == 1){
                    echo "We do not have your order in record";
                }
        }
    }
}