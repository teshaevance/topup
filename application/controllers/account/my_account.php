<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class My_account extends CI_Controller {
    function __construct() {
        parent::__construct();
        
        //load models
        $this->load->model('user_model');
    }
    public function enter_num(){
        $id = $this->session->userdata['user_id'];
        $data = array(
            'phone' => $_POST['phone']
        );
        $result = $this->user_model->update_field($id,$data);
       
        if($result){
            $this->session->set_userdata('phone',$_POST['phone']);
            if($this->session->userdata('phone') != ''){
                $num = trim($this->session->userdata('phone'));
                $fname = substr($this->session->userdata('fname'), 0, 10);
                $msgid = send_verify_sms($num,$fname);
                $data = array('verify_msg_id' => $msgid);
                $result2 = $this->user_model->update_field($this->session->userdata('user_id'),$data);
                if($result2){
                    $this->session->set_userdata('msg_id',$msgid);
                    echo 'true';
                }else{
                    echo 'Not sent, try again';
                }
            }
            
        }else{
            echo 'Not sent, try again';
        }
    }
    
    public function verify_num(){
        $id = $this->session->userdata['user_id'];
        $data = array(
            'verify_stat' => 1
        );
        $result = verify_sms($_POST['cd'],$this->session->userdata('phone'),trim($this->session->userdata('msg_id')));
        if($result){
          $this->user_model->update_field($id,$data);
          $this->session->set_userdata('stat',1);
          echo 'true';
        }else{
            echo 'Not sent, try again';
        }
    }
    
    public function get_pdf($id) {
        $this->load->helper(array('dompdf', 'file'));
        // page info here, db calls, etc.     
        $this->load->model('trans_model'); 
        $this->load->model('operator_model');
        $data['table'] = $this->trans_model->get_trans_id($id);
        $data['op'] = $this->operator_model->match_operator($data['table']['dest_operator_id']);
        $data['email'] = TRUE;
        $html = $this->load->view('recharge/invoice_pdf',$data,TRUE);
        $name = 'TopUp Invoice-'.$data['table']['transaction_id'];
        pdf_create($html, $name);
    }
    
    
    public function send_free_text() {
        if($this->session->userdata('stat') != 0){
            $this->form_validation->set_rules("sms_num","Mobile number","required|numeric|max_length[12]|min_length[12]");
            $this->form_validation->set_rules("sms_msg","Your message","required");
            $this->form_validation->set_message('required','%s is required');
            if ($this->form_validation->run() == FALSE){
                    echo validation_errors();        
            }else {
                $phone = $this->session->userdata('phone');
                $id = $this->session->userdata('user_id');
                $msg = $_POST['sms_msg']." topUp.co.tz";
                $res = route_sms($_POST['sms_num'],$msg ,FALSE,$phone);
                if($res == TRUE){
                    $new_left = $this->session->userdata('sms') - 1;
                    $data = array(
                        'free_text' => $new_left
                    );
                    if($this->user_model->update_field($id,$data)){
                        $this->session->set_userdata('sms',$new_left);
                        echo 'true';
                    }
                    
                }else{
                    echo 'Message not sent, try again please';
                }
            }
        }else{
            echo 'Something is wrong, your number seems not verified';
        }
    }


    public function change_password(){
        //set validation rules
        $this->form_validation->set_rules("old_password","Old Password","required|trim|md5");
        $this->form_validation->set_rules("password","Password","required|trim|md5");
        $this->form_validation->set_rules("password_con","Password Confirmation","required|matches[password]");
                    
        if ($this->form_validation->run() == FALSE){
                $response['data'] = validation_errors();        
                $response['status'] = 'false';        
        }else{
            //match the old password first
                $res = $this->user_model->match_field(array('password' => $_POST['old_password'], 'user_id' => $this->session->userdata('user_id')));
                if($res){
                    $res2  = $this->user_model->update_field($this->session->userdata('user_id'),array('password' => $_POST['password']));
                    if($res2){
                            $response['data'] = 'Password changed';        
                             $response['status'] = 'true';
                    }else{
                            $response['data'] = 'Error: Password not changed';        
                            $response['status'] = 'false';
                    }
                }else{
                        $response['data'] = 'You have entered an incorrect old password';        
                        $response['status'] = 'false';
                }
        
        }
        header('Content-type: application/json');
        exit(json_encode($response));
    }
}