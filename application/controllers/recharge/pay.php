<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Pay extends CI_Controller {
    function __construct() {
        parent::__construct();
        
        //load models
       $this->load->library('email');
       $this->load->model('trans_model');
    }
    public function index() {
        //table data
        $temp_data = $this->session->flashdata('item');
        $data['table'] = $this->trans_model->get_trans_id($temp_data['trans_id']);
        $data['op'] = $this->session->flashdata('item2');
        

        //Prepare configuration values for the header
        $data['type'] = 'inverse';
        $data['title'] = 'topUp | Order';
        if($this->session->userdata('user_id') == NULL){
            $data['login'] = FALSE;
        }else{
            $data['login'] = TRUE;
        }
        
        //Loading views
        $this->load->view('/templates/header.php',$data);
        $this->load->view('/recharge/invoice.php');
        //load the footer
        $this->load->view('/templates/footer.php');
        
        //send email 
        $this->email->from('karibu@topup.co.tz','TopUp');
        $this->email->to($this->session->userdata('username'));
        $email_data['msg'] = $this->load->view('recharge/invoice_email',$data,TRUE);
        $email = $this->load->view('templates/email',$email_data,TRUE);
        $this->email->subject('TopUp Invoice');
        $this->email->message($email);  

        $this->email->send();

    }
    
    
    public function send_sms() {
        $num = $_POST['sms_num'];
        $msg = $_POST['sms'].' www.topup.co.tz';
        route_sms($num,$msg,TRUE,trim($this->session->userdata('phone')));
    }
}