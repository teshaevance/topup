<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class View_trans extends CI_Controller {
    public function index($id) {
        $this->load->model('trans_model'); 
        $this->load->model('operator_model');
        $data['table'] = $this->trans_model->get_trans_id($id);
        $data['op'] = $this->operator_model->match_operator($data['table']['dest_operator_id']);
        $data['email'] = TRUE;
        
        $this->load->view('recharge/invoice_epv',$data);
        
    }


    public function track($ref){
    	$this->load->model('trans_model'); 
        $this->load->model('operator_model');
        $data['table'] = $this->trans_model->get_trans_ref($ref); 
        $data['email'] = TRUE;

        if($data['table'] == null){
            $response['status'] = 'false';
        }else{        
            $data['op'] = $this->operator_model->match_operator($data['table']['dest_operator_id']);
            $response['result'] = $this->load->view('recharge/invoice_epv',$data,true);
            $response['status'] = 'true';
        }


        //return response
        header('Content-type: application/json');
        exit(json_encode($response));
    }
}