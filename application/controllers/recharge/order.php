<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Order extends CI_Controller {
    public function index() {
        $operator_pre = substr($_POST['num'], 0 , 2);
        //match operators to country
        $this->load->model('operator_model');
        $data['result'] = $this->operator_model->get_operators($_POST['code']);
        
        //match prefix to operator
        $active_op = $this->operator_model->match_pre($operator_pre);
        
        $data['pre'] = array("op_pre"=>$active_op);
        //prepare and send order confirmation

            echo json_encode($data);
        
    }
    
    public function gen_phonecode(){
        $this->load->model('country_model');
        $result = $this->country_model->get_country_field('iso',$_POST['id'],'phonecode');
        echo $result;
    }

    public function cash_list(){
       $this->load->model('operator_model');
       $this->load->model('country_model');
       $operator_pre = substr($_POST['num'], 0 , 2);
       $active_op = $this->operator_model->match_pre($operator_pre);
       $result = $this->operator_model->get_cash_list($active_op);
       $data['list'] = $result;
       
       $data['cash_pref'] = $this->country_model->get_country_field('iso',$_POST['id'],'currency_pref');
       $this->load->view('recharge/cash_list',$data);
       
    }
    public function load_form(){
        $this->load->view('recharge/recharge_form');
    }
}