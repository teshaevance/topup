<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Confirm extends CI_Controller {
    function __construct() {
        parent::__construct();
        
        //load models
       $this->load->model('country_model');
       $this->load->model('operator_model');
       $this->load->model('trans_model');
       $this->load->helper('topup_pay');
    }
    public function index($fly) {
        //decide data based on operation
        if($fly == 'true'){
            $_POST = $this->session->userdata('cur_ord');
        }
        
        
        //prep data
        $w = $this->country_model->get_country_field('iso',trim($_POST['country']),'id');
        $y = $this->country_model->get_country_code($w);
        $z = $this->country_model->get_country_name($w);
        $x = $_POST['amount'];
        $t = $this->operator_model->match_operator($_POST['operator']);
        $m = $this->country_model->get_country_field('id',$w,'currency_pref');
        $ch = ($_POST['amount']*$this->config->item('fraction')) + $this->config->item('fixed_charge');

        if($ch > $this->config->item('fraction')){
            $ch = 50;
        }
        //data to be confirmed
        $data['operator'] = $t;
        $data['cash_pref'] = $m;
        $data['title'] = 'TopUp | Order';
        
        //Prepare configuration values for the header
        $data['type'] = 'inverse';
        if($this->session->userdata('user_id') == NULL){
            $data['login'] = FALSE;
        }else{
            $data['login'] = TRUE;
        }
        
        //generate unique transaction id
        $last_row = $this->trans_model->get_last();
        if($last_row){
            $last_id = $last_row['trans_id']+1;
        }else{
             $last_id = 1;
        }
        $curr = str_shuffle(strtoupper(time().$last_id.random_string('alpha', 4).$this->session->userdata('user_id')));
        $curr = substr($curr, 0, 7);
        $trans_data = array(
                'number' => $y.$_POST['phone'],       
                'dest_country_id' => $w,
                'dest_country_name' => $z,       
                'dest_operator_id' => $_POST['operator'],        
                'dest_operator_name' => $t,        
                'amount' => $_POST['amount'],        
                'airtime' => $_POST['airtime'],        
                'charge' => round($ch,2),                                
                'sender_user_id' => $this->session->userdata('user_id'),
                'transaction_id' => $curr
            );
        $result = $this->trans_model->insert_trans($trans_data);
        $data['curr_trans'] = $this->trans_model->get_trans_id($result['LAST_INSERT_ID()']);
        
        //Loading views
        $this->load->view('/templates/header.php',$data);
        $this->load->view('recharge/order_con');
        $this->load->view('templates/footer');
        
        
    }


    public function trans() {
        //Update data in db
        if($_POST['pay_method'] == 'paypal' || $_POST['pay_method'] == 'mastercard' || $_POST['pay_method'] == 'visa'){
                
                $data = array(
                'pay_method' => $_POST['pay_method'],
                'currency' => 'USD',
                'payment_ref' => 'none', 
                'status' => 'proccesing'
                );
                $this->trans_model->update_pay($_POST['trans_id'],$data);

                /*$paypalres = $this->paypalpay($_POST);
                //
                $trns = array(
                        'transaction_id_max' => $paypalres['TRANSACTIONID'],
                        'status'=> 'delivered'
                    );
                $this->trans_model->update_pay($_POST['trans_id'],$trns);*/
                $data_invoice = $this->trans_model->get_trans_id($_POST['trans_id']);

        }else{
            $data = array(
                'pay_method' => $_POST['pay_method'],
                'payment_ref' => $_POST['payment_ref'],
                'status' => 'proccesing',
                'currency' => 'TZS'

            );
            $this->trans_model->update_pay($_POST['trans_id'],$data);
            $data_invoice = $this->trans_model->get_trans_id($_POST['trans_id']);
            //send the money
            if($data_invoice['payment_ref'] == 'tiketitop'){
                if($data_invoice['dest_operator_name'] == 'tigo'){
                    $vendor = ucfirst($data_invoice['dest_operator_name']);
                }else if($data_invoice['dest_operator_name'] == 'airtel'){
                    $vendor = 'comviva';
                }else{
                    $vendor = $data_invoice['dest_operator_name'];
                }
                $customer_num = $data_invoice['number'];
                $amount = 200;
                
               // echo $vendor;
                $result = send_credit($vendor,$customer_num,$amount);
                
                if($result['ResponseMessage'] == 0){
                    $trns = array(
                        'transaction_id_max' => $result['TransactionId'],
                        'status'=> 'delivered',
                        'balance' => $result['CurrentBalance']
                    );
                    $this->trans_model->update_pay($data_invoice['trans_id'],$trns);
                }else{
                    echo 'transaction fail';
                    die();
                }

            }
            }
        
        
        

        //preapre invoice data
        $this->session->set_flashdata('item', $data_invoice);
        $op = $_POST['operator'];
        $this->session->set_flashdata('item2', $op);
        $this->session->unset_userdata('cur_ord');
        //send invoice sms to receiver
        $num = $data_invoice['number'];
        $fname = $this->session->userdata('fname');
        $lname = $this->session->userdata('lname');
                
        $msg = "You have received Tshs ".$data_invoice['airtime']." of ".$_POST['operator']." airtime from ".$fname." ".$lname." via www.topup.co.tz";
        route_sms($num,$msg);
        
        //send invoice sms to sender
        $num = $this->session->userdata('phone');
                
        $msg = "You have sent Tshs ".$data_invoice['airtime']." of ".$_POST['operator']." airtime to ".$data_invoice['number']." via www.topup.co.tz";
        if( $this->session->userdata('phone') != null){
            route_sms($num,$msg);
        }
            redirect("/recharge/pay", 'location');
        
    }
    
    
    public function get_for_pay($id){
        $data['curr_trans'] = $this->trans_model->get_trans_id($id);
        
        //prep data
        $w = $this->country_model->get_country_field('iso',$data['curr_trans']['dest_country_id'],'id');
        $x = $data['curr_trans']['amount'];
        $t = $this->operator_model->match_operator($data['curr_trans']['dest_operator_id']);
        $u = 0.2 * ($data['curr_trans']['amount']);
        
        //data to be confirmed
        $data['amount'] = round($x, 2);
        $data['country_id'] = $w;
        $data['operator'] = $t;
        $data['charge'] = round($u, 2);
        $data['title'] = 'TopUp | Order';
        
        //Prepare configuration values for the header
        $data['type'] = 'inverse';
        if($this->session->userdata('user_id') == NULL){
            $data['login'] = FALSE;
        }else{
            $data['login'] = TRUE;
        }
        
       // print_r($data['curr_trans']);die();
 
        //Loading views
        $this->load->view('/templates/header.php',$data);
        $this->load->view('recharge/order_con');
        $this->load->view('templates/footer');
    }
    
    
    public function set_curr_ord(){
        $this->session->set_userdata('cur_ord',$_POST);
        print_r($this->session->userdata('cur_ord'));
    }



    public function paypalpay($data){
        //print_r($data);die();
        //fetch config values
        $PayPalConfig = array(
                    'Sandbox' => $this->config->item('sandbox'),
                    'APIUsername' => $this->config->item('api_username'),
                    'APIPassword' => $this->config->item('api_password'),
                    'APISignature' => $this->config->item('api_signature')
                    );

        //load the lib
        $this->load->library('payPal',$PayPalConfig);

        //create object
        $paypal_obj = new PayPal($PayPalConfig);

        //preparing transaction info
        $DPFields = array(
                    'paymentaction' => 'Sale',                      // How you want to obtain payment.  Authorization indidicates the payment is a basic auth subject to settlement with Auth & Capture.  Sale indicates that this is a final sale for which you are requesting payment.  Default is Sale.
                    'ipaddress' => $_SERVER['REMOTE_ADDR'],                             // Required.  IP address of the payer's browser.
                    'returnfmfdetails' => '1'                   // Flag to determine whether you want the results returned by FMF.  1 or 0.  Default is 0.
                );
                
        $CCDetails = array(
                            'creditcardtype' => trim($data['cardType']),                   // Required. Type of credit card.  Visa, MasterCard, Discover, Amex, Maestro, Solo.  If Maestro or Solo, the currency code must be GBP.  In addition, either start date or issue number must be specified.
                            'acct' => $data['cardNumber'],                               // Required.  Credit card number.  No spaces or punctuation.  
                            'expdate' => '022016',                          // Required.  Credit card expiration date.  Format is MMYYYY
                            'cvv2' => $data['securityCode'],                                // Requirements determined by your PayPal account settings.  Security digits for credit card.
                            'startdate' => '',                          // Month and year that Maestro or Solo card was issued.  MMYYYY
                            'issuenumber' => ''                         // Issue number of Maestro or Solo card.  Two numeric digits max.
                        );
                        
        $PayerInfo = array(
                            'email' => 'sandbox@angelleye.com',                                 // Email address of payer.
                            'payerid' => '',                            // Unique PayPal customer ID for payer.
                            'payerstatus' => '',                        // Status of payer.  Values are verified or unverified
                            'business' => ''                            // Payer's business name.
                        );
                        
        $PayerName = array(
                            'salutation' => '',                         // Payer's salutation.  20 char max.
                            'firstname' => 'Tester',                            // Payer's first name.  25 char max.
                            'middlename' => '',                         // Payer's middle name.  25 char max.
                            'lastname' => 'Testerson',                          // Payer's last name.  25 char max.
                            'suffix' => ''                              // Payer's suffix.  12 char max.
                        );
                        
        $BillingAddress = array(
                                'street' => '707 W. Bay Drive',                         // Required.  First street address.
                                'street2' => '',                        // Second street address.
                                'city' => 'Largo',                          // Required.  Name of City.
                                'state' => 'FL',                            // Required. Name of State or Province.
                                'countrycode' => 'US',                  // Required.  Country code.
                                'zip' => '33770',                           // Required.  Postal code of payer.
                                'phonenum' => ''                        // Phone Number of payer.  20 char max.
                            );
                            
        $ShippingAddress = array(
                                'shiptoname' => '',                     // Required if shipping is included.  Person's name associated with this address.  32 char max.
                                'shiptostreet' => '',                   // Required if shipping is included.  First street address.  100 char max.
                                'shiptostreet2' => '',                  // Second street address.  100 char max.
                                'shiptocity' => '',                     // Required if shipping is included.  Name of city.  40 char max.
                                'shiptostate' => '',                    // Required if shipping is included.  Name of state or province.  40 char max.
                                'shiptozip' => '',                      // Required if shipping is included.  Postal code of shipping address.  20 char max.
                                'shiptocountrycode' => '',              // Required if shipping is included.  Country code of shipping address.  2 char max.
                                'shiptophonenum' => ''                  // Phone number for shipping address.  20 char max.
                                );
                            
        $PaymentDetails = array(
                                'amt' => '3.00',                            // Required.  Total amount of order, including shipping, handling, and tax.  
                                'currencycode' => 'USD',                    // Required.  Three-letter currency code.  Default is USD.
                                'itemamt' => '',                        // Required if you include itemized cart details. (L_AMTn, etc.)  Subtotal of items not including S&H, or tax.
                                'shippingamt' => '',                    // Total shipping costs for the order.  If you specify shippingamt, you must also specify itemamt.
                                'handlingamt' => '',                    // Total handling costs for the order.  If you specify handlingamt, you must also specify itemamt.
                                'taxamt' => '',                         // Required if you specify itemized cart tax details. Sum of tax for all items on the order.  Total sales tax. 
                                'desc' => 'Testing Payments Pro DESC Field',                            // Description of the order the customer is purchasing.  127 char max.
                                'custom' => 'TEST',                         // Free-form field for your own use.  256 char max.
                                'invnum' => '',                         // Your own invoice or tracking number
                                'buttonsource' => '',                   // An ID code for use by 3rd party apps to identify transactions.
                                'notifyurl' => ''                       // URL for receiving Instant Payment Notifications.  This overrides what your profile is set to use.
                            );

        $OrderItems = array();      
        $Item    = array(
                                'l_name' => 'Test Widget',                      // Item Name.  127 char max.
                                'l_desc' => 'This is a test widget description.',                       // Item description.  127 char max.
                                'l_amt' => '1.00',                          // Cost of individual item.
                                'l_number' => 'ABC-123',                        // Item Number.  127 char max.
                                'l_qty' => '1',                             // Item quantity.  Must be any positive integer.  
                                'l_taxamt' => '',                       // Item's sales tax amount.
                                'l_ebayitemnumber' => '',               // eBay auction number of item.
                                'l_ebayitemauctiontxnid' => '',         // eBay transaction ID of purchased item.
                                'l_ebayitemorderid' => ''               // eBay order ID for the item.
                        );
        array_push($OrderItems, $Item);

        $Item    = array(
                                'l_name' => 'Test Widget 2',                        // Item Name.  127 char max.
                                'l_desc' => 'This is a test widget description.',                       // Item description.  127 char max.
                                'l_amt' => '2.00',                          // Cost of individual item.
                                'l_number' => 'ABC-XYZ',                        // Item Number.  127 char max.
                                'l_qty' => '1',                             // Item quantity.  Must be any positive integer.  
                                'l_taxamt' => '',                       // Item's sales tax amount.
                                'l_ebayitemnumber' => '',               // eBay auction number of item.
                                'l_ebayitemauctiontxnid' => '',         // eBay transaction ID of purchased item.
                                'l_ebayitemorderid' => ''               // eBay order ID for the item.
                        );
        array_push($OrderItems, $Item);

        $PayPalRequestData = array(
                                   'DPFields' => $DPFields, 
                                   'CCDetails' => $CCDetails, 
                                   'PayerInfo' => $PayerInfo,
                                   'PayerName' => $PayerName, 
                                   'BillingAddress' => $BillingAddress, 
                                   'PaymentDetails' => $PaymentDetails, 
                                   'OrderItems' => $OrderItems
                                   );

        $PayPalResult = $paypal_obj->DoDirectPayment($PayPalRequestData);
        //echo '<pre />';
        //print_r($PayPalResult);
        return $PayPalResult;
    }
}

