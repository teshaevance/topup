<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Landing extends CI_Controller {
    public function index() {
         log_message('error', 'Some variable did not contain a value.');
        //check session ? redirect to home : load landing page
        if($this->session->userdata('user_id') != NULL){
            redirect("/home/index/form", 'location');
        }else{
            //prepare view data
            $view_data['title'] = 'TopUp | Landing';
            
            //load page views
            $this->load->view('/templates/header.php',$view_data);
            $this->load->view('landing.php');
            $this->load->view('/templates/footer.php');
        }
    }


    public function loadNetForm(){
        $response['status'] = 'true';
        $response['result'] = $this->load->view('recharge/network_form','',true);

        header('Content-type: application/json');
        exit(json_encode($response));
    }
}