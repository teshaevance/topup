<?php

/* 
 * Author: Tesha Evance
 * Description: common tasks for forms
 * Comments: exclusive rights to author, consult on problems
 */


/*
 * Author: Tesha Evance
 * Description: shows errors individually and proper styling
 */
  function show_form_error($name){
     if(form_error($name) != null){ 
         echo form_error($name, '<p class="error_text">', '</p>'); 
         echo '<script>$( "div" ).last().addClass( "has-error" );</script>';
     }
  }
  
  //SMS FUNCTIONS
function sms($to,$text,$ajax = FALSE,$frm = 'TopUP.co.tz'){
    //Authentication values
    $user = "jmaccoe";
    $password = "ODW3gNns";
    $api_id = "3470085";
    $baseurl ="http://api.clickatell.com";

    //prepare the text
    $text = urlencode($text);
    // auth call
    $url = "$baseurl/http/auth?user=$user&password=$password&api_id=$api_id";
    $ret = file($url);
    //prepare error log
    $info = fopen('sms_log.txt', 'a');
    // explode our response. return string is on first line of the data returned
    $sess = explode(":",$ret[0]);
    if ($sess[0] == "OK") {
        $sess_id = trim($sess[1]); // remove any whitespace
        $url = "$baseurl/http/sendmsg?session_id=$sess_id&to=$to&text=$text&from=$frm";

        // do sendmsg call
        $ret = file($url);
        $send = explode(":",$ret[0]);

        if ($send[0] == "ID") {
            fwrite($info, "Type: send");
            fwrite($info, "Number: $to\n");
            fwrite($info, "successnmessage ID: ". $send[1]);
            fwrite($info, "\n=============================\n");
            if($ajax == TRUE){
                echo 'true';
            }
            fclose($info);
            return TRUE;
        } else {
           fwrite($info, "Type: send");
           fwrite($info, "Number: $to\n");
           fwrite($info, "send message failed");
           fwrite($info, "\n=============================\n");
           if($ajax == TRUE){
                echo 'false';
            }
        }
    } else {
        fwrite($info, "Type: send");
        fwrite($info, "Number: $to\n");
        fwrite($info, "Authentication failure: ". $ret[0]);
        fwrite($info, "\n=============================\n");
        if($ajax == TRUE){
                echo 'false';
            }
    }
    fclose($info);
}
function send_verify_sms($to,$fname){
    //Authentication values
    $user = "jmaccoe";
    $password = "ODW3gNns";
    $api_id = "3470085";
    $baseurl ="http://api.clickatell.com";
    //prepare text
    $text = "Welcome $fname, Your verification code is";
    $text = urlencode($text);
    // send verify code
    $url = "$baseurl/http/sendotp?user=$user&password=$password&api_id=$api_id&to=$to&text=$text+%23OTP%23.+topUp.co.tz&from=TopUP.co.tz&lifetime=7200";
    $ret = file($url);
    //prepare error log
    $info = fopen('sms_log.txt', 'a');
    // explode our response. return string is on first line of the data returned
        $send = explode(":",$ret[0]);

        if ($send[0] == "ID") {
            fwrite($info, "Type: send verify code\n");
            fwrite($info, "Number: $to\n");
            fwrite($info, "successnmessage ID: ". $send[1]);
            fwrite($info, "\n=============================\n");
            fclose($info);
            return $send[1];
            
        }else {
           fwrite($info, "Type: send verify code\n");
           fwrite($info, "Number: $to\n");
           fwrite($info, "send message failed");
           fwrite($info, "\n=============================\n");
           fclose($info);
           return FALSE;
        }

}
function verify_sms($otp,$to,$msgid){
    //Authentication values
    $user = "jmaccoe";
    $password = "ODW3gNns";
    $api_id = "3470085";
    $baseurl ="http://api.clickatell.com";

    // send verify code
    $url = "$baseurl/http/verifyotp.php?user=$user&password=$password&api_id=$api_id&to=$to&otp=$otp&apiMsgId=$msgid&sender_id=1";
    $ret = file($url);
    //prepare error log
    $info = fopen('sms_log.txt', 'a');
    // explode our response. return string is on first line of the data returned
        $send = explode(":",$ret[0]);
        //print_r($send);
        if ($send[0] == "OK") {
            fwrite($info, "Type:verify code\n");
            fwrite($info, "Number: $to\n");
            fwrite($info, "successnmessage ID: ". $send[1]);
            fwrite($info, "\n=============================\n");
            fclose($info);
            return TRUE;
            
        }else {
           fwrite($info, "Type: verify code\n");
           fwrite($info, "Number: $to\n");
           fwrite($info, "send message failed");
           fwrite($info, "\n=============================\n");
           fclose($info);
           return FALSE;
        }
        

}

//ROUTE SMS FUNCTIONS
function route_sms($to,$message,$ajax = FALSE,$from = 'TopUP.co.tz'){
  //Congiguraton Values
  $username = 'jonsoft';
  $password = 'md54r6vw';
  $smpp_server = 'smsplus1.routesms.com';
  $smpp_port = '2345';


  //Prepare message
  $message = urlencode($message);


  //Sms send Call
  $http_request = "http://$smpp_server:8080/bulksms/bulksms?username=$username&password=$password&type=0&dlr=0&destination=$to&source=$from&message=$message";
  
  $response = file($http_request);

  //Handle Response
  $result = explode('|', $response[0]);
  if($result[0] == '1701'){
    if($ajax == TRUE){
            echo 'true';
        }
        return TRUE;
  }else{
    if($ajax == TRUE){
            echo 'false';
        }
  }
}
