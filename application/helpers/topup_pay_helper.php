<?php 
function get_balance(){
    //PREPARE SOME DAT SOME CURL OPTION SETUP
    //Remote URL
    $url = "http://41.217.201.73:2060/apiroutersrv/apirouter.asmx";
    
    //Actual xml payload
    $soap_request = '<?xml version="1.0" encoding="utf-8"?>
    <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
      <soap:Body>
        <GetBalance xmlns="http://tempuri.org/">
          <x509PEMCertificate>ssteam</x509PEMCertificate>
          <retailerCode>API0000033</retailerCode>
        </GetBalance>
      </soap:Body>
    </soap:Envelope>';
    
    //Preapre Headers
    $header = array(
        "Content-type: text/xml;charset=\"utf-8\"",
        "Accept: text/xml",
        "Cache-Control: no-cache",
        "Pragma: no-cache",
        "SOAPAction: \"http://tempuri.org/GetBalance\"",
        "Content-length: ".strlen($soap_request),
    );
    
    //Initialize a curl session
    $topup_do = curl_init();
 
    //Setup curl sessions' options
    curl_setopt($topup_do, CURLOPT_URL,            $url);
    curl_setopt($topup_do, CURLOPT_RETURNTRANSFER, true );
    curl_setopt($topup_do, CURLOPT_POST,           true );
    curl_setopt($topup_do, CURLOPT_POSTFIELDS,     $soap_request);
    curl_setopt($topup_do, CURLOPT_HTTPHEADER,     $header);
    
    //Execute the curl setup 
    $result = curl_exec($topup_do);
    //echo $result;
    
    
    //Handle the result 
    //Was a error returned ? handle error : Confirm Delivery
    if($result === false) {
        $err = 'Curl error: ' . curl_error($topup_do);
        curl_close($topup_do);
        print $err;
    }else {
        curl_close($topup_do);
        
        //Parse the resulting html
        $p = xml_parser_create();
        xml_parse_into_struct($p, $result, $vals, $index);
        xml_parser_free($p);
        
        //create restult array
        $response = array(
            'ResponseCode' => $vals[4]['value'],
            'ResponseMessage' => $vals[5]['value'],
            'CurrentBalance' => $vals[6]['value'],
        );
        
        //return result
        return $response;
    }
}




function send_credit($vendor,$customer_num,$amount){
    //PREPARE SOME DAT SOME CURL OPTION SETUP
    //Remote URL
    $url = "http://41.75.211.132:2060/apiroutersrv/apirouter.asmx";
    
    //Actual xml payload
    $soap_request = '<?xml version="1.0" encoding="utf-8"?>
    <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
      <soap:Body>
          <DoTransaction xmlns="http://tempuri.org/">
              <x509PEMCertificate>ssteam</x509PEMCertificate>
              <retailerCode>API0000033</retailerCode>
              <vendorShortName>'.$vendor.'</vendorShortName>
              <CustomerSubscriptionId>'.$customer_num.'</CustomerSubscriptionId>
              <amount>'.$amount.'</amount>
          </DoTransaction>
      </soap:Body>
    </soap:Envelope>';
    
    //Preapre Headers
    $header = array(
        "Content-type: text/xml;charset=\"utf-8\"",
        "Accept: text/xml",
        "Cache-Control: no-cache",
        "Pragma: no-cache",
        "SOAPAction: \"http://tempuri.org/DoTransaction\"",
        "Content-length: ".strlen($soap_request),
    );
    
    //Initialize a curl session
    $topup_do = curl_init();
 
    //Setup curl sessions' options
    curl_setopt($topup_do, CURLOPT_URL,            $url);
    curl_setopt($topup_do, CURLOPT_RETURNTRANSFER, true );
    curl_setopt($topup_do, CURLOPT_POST,           true );
    curl_setopt($topup_do, CURLOPT_POSTFIELDS,     $soap_request);
    curl_setopt($topup_do, CURLOPT_HTTPHEADER,     $header);
    
    //Execute the curl setup 
    $result = curl_exec($topup_do);
    
    //Handle the result 
    //Was a error returned ? handle error : Confirm Delivery
    if($result === false) {
        $err = 'Curl error: ' . curl_error($topup_do);
        curl_close($topup_do);
        print $err;
    }else {
        curl_close($topup_do);
        
        //Parse the resulting html
        $p = xml_parser_create();
        xml_parse_into_struct($p, $result, $vals, $index);
        xml_parser_free($p);
        
      
        //create restult array
        $response = array(
            'ResponseCode' => $vals[4]['value'],
            'ResponseMessage' => $vals[5]['value'],
            'TransactionId' => $vals[6]['value'],
            'CurrentBalance' => $vals[7]['value'],
        );
        
        //return result
        return $response;
    }
}
