<!DOCTYPE html>
<!-- 
#Author: Tesha
#Description: Header file containing menu bar.
-->
<!-- php scripts -->
<?php $fname = $this->session->userdata('fname'); ?>

<html lang="en">
    <head id="head">
        <title><?php echo $title; ?></title>
        
        <!-- Meta-data -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!-- Spreadsheet files -->
        <link type="text/css" rel="Stylesheet" href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css" media='screen'>
        <link type="text/css" rel="Stylesheet" href="<?php echo base_url(); ?>assets/font-awesome-4.0.3/css/font-awesome.min.css" media='screen'>
        <link type="text/css" rel="Stylesheet" href="<?php echo base_url(); ?>assets/bootstrapformhelpers/css/bootstrap-formhelpers.min.css">
        <link type="text/css" rel="Stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap_tweaks.css">
        <link type="text/css" rel="Stylesheet" href="<?php echo base_url(); ?>assets/css/main.css">


        
        <!-- Scripts -->
        <script src="<?php echo base_url(); ?>assets/jquery/jquery-1.11.0.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/bootstrapformhelpers/js/bootstrap-formhelpers.min.js"></script>
        
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        
        
    </head>
    <body data-spy="scroll" data-target="#navigation" data-offset="100">  
        <header id="navigation" class="navbar navbar-fixed-top navbar-inverse " role="banner">
        <div style="height: 4px; background-color: #FFFFFF" class="hidden-xs"></div>
        <div class="container">
        <div class="row">
            <div class="col-sm-offset-1 navbar-header">               
                <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".bs-navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>          
                <a href="<?php echo site_url(); ?>/landing" class="nav_brand navbar-brand" style="font-size: 160%;">TopUp</a>
            </div>
            <form class="navbar-form col-sm-3">
                <div class="form-group">
                    <div class="">
                        <div  id="country" class="bfh-selectbox bfh-countries" data-name="country" data-filter="true" data-flags="true" data-available="TZ,KE,UG" data-country="TZ"></div>
                    </div>
                </div>
            </form>
            <nav class="collapse navbar-collapse bs-navbar-collapse col-sm-offset-4 col-md-offset-6" role="navigation">
                <?php if($fname != NULL){ ?>                   
                    <a href="<?php echo site_url(); ?>/access/login_logout/logout" class="btn btn-sys navbar-btn btn-block visible-xs" role="button">Logout</a>
                <?php } else { ?>
                    <a href="<?php echo site_url(); ?>/access/login_logout/login" class="btn btn-sys navbar-btn btn-block visible-xs" role="button">Login</a>
                <?php } ?>
                <ul class="nav navbar-nav ">
                    <?php $url = $this->uri->segment(1); ?>
                      <!-- Display home for registered user and for non registered user -->
                      <?php if(isset($login) && $login == TRUE) { ?>
                            <li><a class="hidden" href="#head_wrap"></a></li>
                            <li><a class="nav_link" href="<?php echo site_url(); ?>/home/index/form">HOME<span class="glyphicon glyphicon-chevron-right pull-right visible-xs"></span></a></li>
                            <li><a class="nav_link" href="<?php echo site_url(); ?>/home/index/order">MY ORDERS<span class="glyphicon glyphicon-chevron-right pull-right visible-xs"></span></a></li>
                            <li><a class="nav_link" href="<?php echo site_url(); ?>/home/index/profile"><span class="glyphicon push_right_bit glyphicon-user"></span><?php echo $this->session->userdata('fname')."\t - ";?>ACCOUNT<span class="glyphicon glyphicon-chevron-right pull-right visible-xs"></span></a></li>
                 </ul>
                            <a href="<?php echo site_url(); ?>/access/login_logout/logout" id="nav_login" class="btn btn-grn navbar-btn hidden-xs push_left_bit" role="button">LOGOUT</a>
                      <?php } else { ?>
                            <li><a class="hidden" href="#head_wrap"></a></li>
                            <li><a class="nav_link" href="<?php if(!($url == 'landing' || strlen($url) == 0) ){echo site_url().'/landing/index/';} ?>#about">ABOUT US <span class="glyphicon glyphicon-chevron-right pull-right visible-xs"></span></a></li>
                            <li><a class="nav_link" href="<?php if(!($url == 'landing' || strlen($url) == 0) ){echo site_url().'/';} ?>#price">HOW IT WORKS<span class="glyphicon glyphicon-chevron-right pull-right visible-xs"></span></a></li>
                            <li><a class="nav_link" href="<?php if(!($url == 'landing' || strlen($url) == 0) ){echo site_url().'/';} ?>#support">SUPPORT <span class="glyphicon glyphicon-chevron-right pull-right visible-xs"></span></a></li>
                            <li><a class="nav_link" href="<?php echo site_url(); ?>/access/reg/">SIGN UP <span class="glyphicon glyphicon-chevron-right pull-right visible-xs"></span></a></li> 
                </ul>
                            <a href="<?php echo site_url(); ?>/access/login_logout/login" id="nav_login" class="btn btn-grn navbar-btn hidden-xs" role="button">LOGIN</a>
                      <?php } ?>
            </nav>
        </div>        
        </div>  
      </header>
      <!--back top top btn -->  
      <a href="#" class="back-to-top"><span class="glyphicon glyphicon-chevron-up"></span></a>  
        
<!-- Scripts -->
<script>
 jQuery(document).ready(function() {
    //same page navigation
    $('.navbar-nav li').click(function(){
        $('html, body').animate({
            scrollTop: $( $(this).children('a').attr('href') ).offset().top - 72
        }, 1000);
        $('.nav_link').parent().removeClass('active');
        $(this).addClass('active');
        return false;
    });


    //back to top btn
        var offset = 220;
        var duration = 500;
        jQuery(window).scroll(function() {
            if (jQuery(this).scrollTop() > offset) {
                jQuery('.back-to-top').fadeIn(duration);
            } else {
                jQuery('.back-to-top').fadeOut(duration);
            }
        });
        
        jQuery('.back-to-top').click(function(event) {
            event.preventDefault();
            jQuery('html, body').animate({scrollTop: 0}, duration);
            return false;
        });

    $("#instuctions_voda").show();
    $("#instuctions_tigo").hide();
    $("#instuctions_airtel").hide();
    $("#instuctions_paypal").hide();
    $("#instuctions_visa").hide();
    $("#instuctions_mastercard").show();
    
    $("#mobile_payment_methods").change(function(){

        if(this.value=="MPESA")
            {
                $("#instuctions_tigo").hide();
                $("#instuctions_airtel").hide();
                $("#instuctions_voda").show();
            }
        else if(this.value=="TIGO PESA")
            {
                $("#instuctions_voda").hide();
                $("#instuctions_airtel").hide();
                $("#instuctions_tigo").show();
            }
        else if(this.value=="AIRTEL MONEY")
            {
                $("#instuctions_tigo").hide();
                $("#instuctions_voda").hide();
                $("#instuctions_airtel").show();
            }
     });
    $("#online_payment_methods").change(function(){
        if(this.value=="MASTERCARD")
            {
                $("#instuctions_paypal").hide();
                $("#instuctions_visa").hide();
                $("#instuctions_mastercard").show();
            }
        else if(this.value=="VISA")
            {
                $("#instuctions_paypal").hide();
                $("#instuctions_visa").show();
                $("#instuctions_mastercard").hide();
            }
        else if(this.value=="PAYPAL")
            {
                $("#instuctions_paypal").show();
                $("#instuctions_visa").hide();
                $("#instuctions_mastercard").hide();
            }
     });   
});
</script>