<style>
    .my_thumb{
        background-color: #FFFFFF;
        border: 1px solid #DDDDDD;
        border-radius: 4px;
        display: block;
        padding: 2px;
        transition: all 0.2s ease-in-out 0s;
    }
</style>
<div id="support" class="dark">
<div class="container">
    <h2 style="margin-top: 10px;" class="col-sm-offset-1">&nbsp;Contact Us</h2>
    <div class="contact-info col-sm-5 col-sm-offset-1">	
                <p class="col-sm-12"><span class="glyphicon glyphicon-map-marker push_right_bit"></span>Address:7th Floor, Amani Place, Ohio St, Dar es Salaam.<p>
                <p class="col-sm-6"><span class="glyphicon glyphicon-earphone push_right_bit"></span>Phone: +255 754 555588</p>
                <p class="col-sm-6"><span class="glyphicon glyphicon-envelope push_right_bit"></span>Email: karibu@topup.co.tz</p>
            <div class="clearfix"></div>
            <div style="background-color: #fff;">
                <iframe id="map" class="col-sm-12" width="445" height="305" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?q=Amani%20Place%2C%20Ohio%20Street%2C%20Dar%20es%20Salaam%2C%20Tanzania&key=AIzaSyDb4i8grUA4NNZlxw4oO1d1xdE6kAPyqmI"></iframe> 
            </div>
    </div>
    
    <div class="col-sm-5 " id="contact_panes">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs">
            <li class="active"><a href="#_social" data-toggle="tab">We are Social</a></li>
            <li><a href="#touch" data-toggle="tab">Say HELLO!</a></li>
            <li><a href="#touch_sup" data-toggle="tab">Contact Support</a></li>     
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <div class="tab-pane" id="touch">
                <form id="msg_form" role="form" action="<?php echo site_url(); ?>/message" method="POST">
                    <h3 class="_bottom">Send Us a Message</h3>
                    <div id="msg_error" class="alert alert-success hidden"></div>
                      <div class="form-group">
                        <label for="name">Full Name</label>
                        <input name="name" type="text" class="form-control" id="fname" placeholder="">
                      </div>
                      <div class="form-group">
                        <label for="email">Email address</label>
                        <input name="email" type="email" class="form-control" id="email" placeholder="" >
                      </div>
                      <div class="form-group">
                          <label for="msg">Message</label>
                          <textarea name="msg" class="form-control" rows="3"></textarea>
                      </div>
                      <button id="snd_msg" type="submit" class="btn btn-primary pull-right">Send Message</button>
                </form>
            </div>
            <div class="tab-pane" id="touch_sup">
                <form id="tkt_form" role="form" action="<?php echo site_url(); ?>/ticket" method="POST">
                    <h3 class="_bottom">Got a Problem with an Order?</h3>
                     <div id="tkt_error" class="alert alert-success hidden"></div>
                      <div class="form-group">
                        <label for="email">Email Address</label>
                        <input name="email" type="text" class="form-control" id="email" placeholder="" value="<?php if($this->session->userdata('username') != ''){ echo $this->session->userdata('username'); } ?>">
                      </div>
                      <div class="form-group">
                        <label for="ref">Order Reference Number</label>
                        <input name="ref" type="text" class="form-control" id="ref" placeholder="">
                      </div>
                      <div class="form-group">
                          <label for="msg">Message</label>
                        <textarea name="msg" class="form-control" rows="3"></textarea>
                      </div>
                      <button id="snd_tkt" type="submit" class="btn btn-primary pull-right">Send Message</button>
                </form>
            </div>
            <div class="tab-pane active" id="_social">
                <br/>
                <div class="col-sm-5">
                &nbsp;
                <a href="https://www.facebook.com/JonSoftGroup" target="_blank"><span style="color: #555" class="fa-stack fa-lg">
                        <i class="fa fa-circle fa-stack-2x fa-inverse"></i>
                        <i class="fa fa-facebook fa-stack-1x"></i>
                    </span>&nbsp; Facebook
                </a>
                <div class="clearfix"></div>
                &nbsp;
                <a href="#" target="_blank"><span style="color: #555" class="fa-stack fa-lg">
                        <i class="fa fa-circle fa-stack-2x fa-inverse"></i>
                        <i class="fa fa-google-plus fa-stack-1x"></i>
                    </span>&nbsp; Google+
                </a>
                <div class="clearfix"></div>
                &nbsp;
                <a href="https://twitter.com/JonSoftGroup" target="_blank"><span style="color: #555" class="fa-stack fa-lg">
                        <i class="fa fa-circle fa-stack-2x fa-inverse"></i>
                        <i class="fa fa-twitter fa-stack-1x"></i>
                   </span>&nbsp; Twitter
                </a>
                <div class="clearfix"></div>
                &nbsp;
                <a href="http://www.linkedin.com/company/jonsoft-group" target="_blank"><span style="color: #555" class="fa-stack fa-lg">
                        <i class="fa fa-circle fa-stack-2x fa-inverse"></i>
                        <i class="fa fa-linkedin fa-stack-1x"></i>
                   </span>&nbsp; LinkedIn
                </a>
                <div class="clearfix"></div>
                &nbsp;
                <a href="http://www.linkedin.com/company/jonsoft-group" target="_blank"><span style="color: #555" class="fa-stack fa-lg">
                        <i class="fa fa-circle fa-stack-2x fa-inverse"></i>
                        <i class="fa fa-instagram fa-stack-1x"></i>
                   </span>&nbsp; Instagram
                </a>
                </div>
                <div class="col-sm-7">
                    <h4 style="margin-top: 0px;" class=" _bottom text-center">For Latest From JonSoft</h4>
                    <a href="https://twitter.com/JonSoftGroup" class="twitter-follow-button center-block" data-show-count="false" data-size="large">Follow @JonSoftGroup</a><br/>
                    <div class="col-sm-offset-1">
                        <iframe src="//www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2FJonSoftGroup&amp;width&amp;height=258&amp;colorscheme=dark&amp;show_faces=true&amp;header=false&amp;stream=false&amp;show_border=false&amp;appId=1438568719719845" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:258px;" allowTransparency="true"></iframe>
                    </div>
                </div>
            </div>    
    </div>  
</div>  
<div class="clearfix"></div>
    <div class="_hr"><hr/></div>
    <div clas="col-sm-12">
        <p class="featured col-sm-2 text-right">Works with:</p>
        <div class="">
            <a href="#" target="_blank" >
                <img src="<?php echo base_url();?>assets/images/payment_banner.png" alt="mpesa">
            </a>
        </div>
        
    </div>
    </div>
</div> 
 

<div id="footer">
    <div id="rights">
        <div class="container text-center">
            © Copyright 2014 by JonSoft Group. All Rights Reserved
        </div>
    </div>
</div>

    </body>
</html>

<script>
$(document).ready(function(){
    $('#_social a').on("mouseenter", function () {
                $(this).children('span').children('.fa-stack-1x').addClass('fa-spin');
    });
    $('#_social a').on("mouseleave", function () {
                $(this).children('span').children('.fa-stack-1x').removeClass('fa-spin');
    });
    $("#snd_msg").click(function(){
        ajax_form('msg_form','message','msg_error');
        return false;
     });
    $("#snd_tkt").click(function(){
        ajax_form('tkt_form','account/ticket','tkt_error');
        return false;
     });
    function ajax_form(form,con,error){
         var t = "<?php echo site_url(); ?>";
         var c = t+"/"+con;
         $('#'+error).removeClass('alert-danger');
         $('#'+error).addClass('alert-success');
         $('#'+error).html('<img style="height: 30px;" class="push_right_bit" src="<?php echo base_url(); ?>/assets/images/ajax-loader.gif">Sending....');
         $('#'+error).fadeIn().removeClass('hidden');
            setTimeout(function(){
                 $.post( c, $("#"+form).serialize()).done(function(data) {
                        if(data == 'true'){
                            $('#'+error).html('<strong>Done!</strong> Message Sent..');
                        }else{                   
                            $('#'+error).removeClass('alert-success');
                            $('#'+error).addClass('alert-danger');
                            $('#'+error).html(data);
                            $('#'+error).removeClass('hidden');
                        }
                        //setTimeout(function(){$('#'+error).fadeOut(5000);},10000);
                });
             },400);
    }   
  });
  
  //socail media
!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');

</script>