<style>
    @import url(http://fonts.googleapis.com/css?family=Open+Sans);
    .home_head{
        margin-top: 5px;
        font-family: 'Open Sans', sans-serif;
        font-size: 130%;
    }
    .home_sub{
        font-family: 'Open Sans', sans-serif;
        font-size: 115%;
    }
    #home_wrap a.list-group-item{
        font-family: 'Open Sans', sans-serif;
        font-size: 110%;
    }
       .home_tab{
    padding: 5px 5px 5px 5px !important;
    border: #F8F8F8 solid 1px; 
    border-radius: 3px;
    min-height: 480px;
}
</style>
<div class="container" style="margin-bottom: 5px;">
    <div class="row" id="home_wrap">
        <div class="col-sm-3">
            <p class="home_head">My Dashboard</p>
            <hr style="margin-top: 0px; border: none; height: 3px; background:#428BCA;">
            <div class="list-group">
                <a href="#form" class="list-group-item">Send Credit Now<span class="glyphicon glyphicon-chevron-right pull-right"></span></a>
                <a href="#order" class="list-group-item">My Orders<span class="glyphicon glyphicon-chevron-right pull-right"></span></a>
                <a href="#free_sms" class="list-group-item">Send Free SMS<span class="glyphicon glyphicon-chevron-right pull-right"></span></a>
                <a href="#profile" class="list-group-item">My Profile<span class="glyphicon glyphicon-chevron-right pull-right"></span></a>
                <?php if($this->session->userdata('username') == 'admin@topup.co.tz'){ ?>
                    <a href="<?php echo site_url('admin/home'); ?>" class="list-group-item">Admin Home<span class="glyphicon glyphicon-chevron-right pull-right"></span></a>
                <?php } ?>
              </div>
        </div>
        <div class="col-sm-9">
            <div class="col-sm-11">
            <p id="home_title" class="home_head">Send Credit Now</p>
            <hr style="margin-top: 0px; border: none; height: 3px; background:#428BCA;">
            <?php 
            if($this->session->userdata('phone') == ''){
                $this->load->view('account/enter_num');
                $data['phn'] = FALSE;
                $this->load->view('account/verify_num',$data);  
            }else if ($this->session->userdata('stat') == 0) {
                $this->load->view('account/verify_num');  
            } ?>
            <div id="form" class="home_tab  hidden col-sm-10 col-sm-offset-1">
                <?php  $this->load->view('recharge/recharge_form'); ?>
            </div>
            <div class="clearfix"></div>
            <div id="order" class="home_tab hidden">
                <?php  $this->load->view('account/trans_list'); ?>
            </div>
             <div class="clearfix"></div>
            <div id="free_sms" class="home_tab hidden">
                <?php  $this->load->view('account/free_sms'); ?>
            </div>
        <div class="clearfix"></div> 
            <div id="profile" class="home_tab hidden">
                <?php  $this->load->view('account/profile'); ?>
            </div>
        <div class="clearfix"></div> 
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $( "#country" ).trigger("hidden.bfhselectbox");
        $("<?php echo '#'.$def; ?>").removeClass('hidden');
        $('.list-group-item').click(function(){
            $('.home_tab').addClass('hidden');
            $('#home_title').html($(this).text());
            $($(this).attr('href')).removeClass('hidden');
            return false;
        });
        
        
        
    });
</script>