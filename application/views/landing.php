<style>
    #land_banner{
        min-height: 560px;
        background-size:100% 100%;
        background-repeat:no-repeat;
    }
    
    #form_slide{
        margin-top: 5px;
        border: #CCCCCC solid 1px;
        border-radius: 3px;
        background-color: #FFFFFf;
        opacity: 0.95;
        min-height: 450px; 
        font-size: 110%;
    }
    
    #login_wrap{
        margin-top: 5px;
        font-size: 110%;
    }
    
    .net_link{
        width: 83px;
        height: 55px;
        margin-bottom: 5px;
    }
    
    #net_wrap{
        border-top: #CCCCCC solid 1px;
        border-bottom: #CCCCCC solid 1px;
        padding-top: 5px;
    }

    .slide_img{
        min-height: 450px;
    }

    .carousel-caption{
        color: #000;
    }

    .carousel.fade {
  opacity: 1;
    }
    .carousel.fade .item {
      -moz-transition: opacity ease-in-out .7s;
      -o-transition: opacity ease-in-out .7s;
      -webkit-transition: opacity ease-in-out .7s;
      transition: opacity ease-in-out .7s;
      left: 0 !important;
      opacity: 0;
      top:0;
      position:absolute;
      width: 100%;
      display:block !important;
      z-index:1;
    }
    .carousel.fade .item:first-child {
      top:auto;
      position:relative;
    }
    .carousel.fade .item.active {
      opacity: 1;
      -moz-transition: opacity ease-in-out .7s;
      -o-transition: opacity ease-in-out .7s;
      -webkit-transition: opacity ease-in-out .7s;
      transition: opacity ease-in-out .7s;
      z-index:2;
    }
</style>

<div class="container-fluid" id="land_banner">
    <!-- Recharge Form --> 
    <div id="form_slide_wrap" class="col-sm-6 bottom_10">
        <div class="col-sm-10 col-sm-offset-1" id="form_slide">
            <h3 class="text-center" style="margin-bottom: 0px;">Enter Recharge Details</h3>
            <?php $this->load->view('recharge/recharge_form'); ?>
        </div>
    </div>

    <!-- Faders -->
    <div class="col-sm-6">
        <div id="my_carousel" class="carousel fade " data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#my_carousel" data-slide-to="0" class="active"></li>
                <li data-target="#my_carousel" data-slide-to="1"></li>
                <li data-target="#my_carousel" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner" id="slides" >
                <div class="item active">
                    <img class="img-responsive slide_img" src="<?php  echo base_url(); ?>/assets/images/slide_1.png" alt="First slide">
                        <div class="carousel-caption">
                            <h4><span class="glyphicon glyphicon-signal push_right_bit"></span>A wide List of supported OPERATORS.</h4>
                            <h4><span class="glyphicon  glyphicon glyphicon-info-sign push_right_bit"></span>AMOUNT received known before sending</h4>
                            <h4><span class="glyphicon glyphicon-refresh push_right_bit"></span>Instant order PROCESSING & DELIVERY</h4>
                            <p><a class="btn btn-lg btn-primary" href="<?php echo site_url('access/reg'); ?>" role="button">Sign up today</a></p>
                        </div>
                </div>
                <div class="item">
                    <img class="img-responsive slide_img" src="<?php  echo base_url(); ?>/assets/images/slide_2.png" alt="First slide">
                        <div class="carousel-caption">
                            <h4><span class="glyphicon glyphicon-comment push_right_bit"></span>EXCELLENT customer service</h4>
                            <h4><span class="glyphicon glyphicon-shopping-cart push_right_bit"></span>Accepting PAYPAL, VISA or MASTERCARD</h4>
                            <h4><span class="glyphicon glyphicon-gift push_right_bit"></span>100 FREE SMS to Tanzania!</h4>
                            <p><a class="btn btn-lg btn-primary" href="<?php echo site_url('access/reg'); ?>" role="button">Sign up today</a></p>
                        </div>
                </div><div class="item">
                    <img class="img-responsive slide_img" src="<?php  echo base_url(); ?>/assets/images/slide_3.png" alt="First slide">
                        <div class="carousel-caption">
                            <p><a class="btn btn-lg btn-primary" href="<?php echo site_url('access/reg'); ?>" role="button">Get Started Now</a></p>
                        </div>
                </div>
            </div>
        </div>
    </div>
    <div id='login_wrap' class="col-sm-5 bottom_10 hidden">
        <div class="col-sm-11">
            <h4 class="text-info text-center bottom_10">Please login to proceed to payment</h4>
            <?php $data['operation'] = 'fly';
            $this->load->view('access/login_box',$data);
            ?>
        </div>
    </div>

    <div id='login_wrap_norm' class="col-sm-5 bottom_10 hidden">
        <div class="col-sm-11">
            <h4 class="text-info text-center bottom_10">Please login to proceed to payment</h4>
            <?php
                 $data['operation'] = 'not_fly';
                $this->load->view('access/login_box2',$data);
            ?>
        </div>
    </div>
</div>

<div id='net_wrap'>
    <div class="col-sm-3 col-sm-offset-1 text-right">
        <h3 class="bottom_10">Network Services</h3>
    </div>
    <div class="col-sm-7">
    <a href=""><img id="tigo" class="net_link net_loader" src="<?php echo base_url('assets/images/tigo.png'); ?>"></a>
    <a href=""><img id="airtel" class="net_link net_loader" src="<?php echo base_url('assets/images/vodacom.png'); ?>"></a>
    <a href=""><img id="vodacom" class="net_link net_loader" src="<?php echo base_url('assets/images/airtel.png'); ?>"></a>
    <a href=""><img id="zantel" class="net_link net_loader" src="<?php echo base_url('assets/images/zantel.png'); ?>"></a>
    <a href=""><img id="sasatel" class="net_link net_loader" src="<?php echo base_url('assets/images/sasatel.png'); ?>"></a>
    </div>
    <div class="clearfix"></div>
</div>
<div class="container">
    <div id="about" class="same_page">
        <div class="well">
            <legend>About Us</legend>
            <p>
                We are giving East Africans and friends of East Africa overseas the better way to "TopUP"  respective prepaid services at convinience of internet connection. Paying with Mobile Money, Paypal and major credit and most debit cards. 
            </p>
            <p>
                TopUP is a division of JonSoft Group, For more than seven years, we have been delivering unparalleled quality and reliability within telecommunications and banking domains.
                Positioned at the forefront of the Internet & Mobile payments eco-system, JG has established itself as the innovative market leader and has nurtured relationships with tier one carriers and major banks in Africa while providing extraordinary quality and service.
            </p>
        </div>
    </div>

    <div id="price" class="same_page">
        <div class="well">
            <legend>How it Works?</legend>
            <p class="featured">
                TopUp allows you to reload family & friends or your own prepaid service , from anywhere in the world!
            </p>
            <p class="featured">
                Top Up in 5 easy steps:</p>
                <ul>
                    <li>Register for free</li>
                    <li>Select a top up value</li>
                    <li>Select your prefared gateway</li>
                    <li>Pay and view your receipt</li>
                    <li>The Prepaid service will receve reaload and confirmation</li>
                </ul>
                
            <br/>
        </div>
    </div>
</div>

<script>
    //Fit Screen to window
    $('#land_banner').css('min-height',function(){
            var h = $(window).height() - 140;
            $('#form_slide').css('margin-top',function(){
                var x = (h-475)/2;
                if(x > 0){
                    return x;
                }
            });
            $('#login_wrap').css('margin-top',function(){
                var x = (h-475)/2;
                if(x > 0){
                    return x;
                }
            });
            return h;
    });

    //repalce the recharge form with the network form
    $('.net_loader').click(function(){
        var url = "<?php echo site_url('/landing/loadNetForm'); ?>";
        $.post( url, { operator: $(this).attr('id')} ).done(function( data ) {
                if(data.status = 'true'){
                    $('#form_slide').html(data.result);
                }  
        },'json');

        return false;
    });

    $('#nav_login').click(function(){
        $('#my_carousel').addClass('hidden');
        $('#login_wrap').addClass('hidden');
        $('#login_wrap_norm').fadeIn().removeClass('hidden');
        $('#signin_btn2').tab('show');
        return false;
    });

    $('#nav_sign').click(function(){
        $('#my_carousel').addClass('hidden');
        $('#login_wrap').addClass('hidden');
        $('#login_wrap_norm').fadeIn().removeClass('hidden');
        $('#signup_btn2').tab('show');
        return false;
    });
</script>