<script>
    var curr_net;
</script>
<style>
    @import url(http://fonts.googleapis.com/css?family=Open+Sans);
    h4, #footer{
        font-family: 'Open Sans', sans-serif;
    }
    .featured{  
        font-family: 'Open Sans', sans-serif;
        font-size: 130%;
    }
    
    .net_link{  
        border:#fff solid 2px;
        border-radius: 3px;
        margin-right: 10px;
        opacity: 0.8;
    }
    .net_link:hover{  
       border: solid 2px;
       opacity: 1;
    }
    
    .carousel-indicators > li{
        border:#ffffff solid 2px !important;
    }
    .carousel-indicators > .active{
        border:#428bca solid 2px !important;
        background-color: #fff !important;
    }
</style>
<div class="container-fluid" id="head_wrap" >
<div id="my_carousel" class="carousel slide " data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <li data-target="#my_carousel" data-slide-to="0" class="active"></li>
        <li data-target="#my_carousel" data-slide-to="1"></li>
        <li data-target="#my_carousel" data-slide-to="2"></li>
    </ol>

<!-- Recharge Form --> 
<div id="form_slide_wrap" class="col-sm-6 col-sm-offset-1">
    <div class="col-sm-10" id="form_slide">
        <h4 class="text-center" style="margin-bottom: 0px;">Enter Recharge Details</h4>
        <?php $this->load->view('recharge/recharge_form'); ?>
    </div>
</div>

<!-- Fade Show -->
  <div id='login_wrap' class="col-sm-5 hidden" style="min-height: 450px;">
    <?php $this->load->view('access/login_box'); ?>
</div>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" id="slides">
     <div class="item active">
         <img class="col-sm-7 pull-right img-responsive center-block slide_img" src="<?php  echo base_url(); ?>/assets/images/slide_1.png" alt="First slide">
            <div class="col-sm-7 pull-right">
            <div class="carousel-caption">
                <p><a class="btn btn-lg btn-primary" href="<?php echo site_url('access/reg'); ?>" role="button">Sign up today</a></p>
            </div>
            </div>
        </div>
        <div class="item">
            <img class="img-responsive center-block col-sm-7 pull-right slide_img" src="<?php echo base_url(); ?>/assets/images/slide_2.png" alt="First slide">
            <div class="col-sm-7 pull-right">
            <div class="carousel-caption">
                <p class="text-primary">pay with mpesa</p>
            </div>
            </div>
        </div>
        <div class="item">
            <img class="img-responsive center-block col-sm-7 pull-right slide_img" src="<?php echo base_url(); ?>/assets/images/slide_3.png" alt="First slide">
            <div class="col-sm-7 pull-right">
            <div class="carousel-caption">
                <p class="text-primary">pay with paypal</p>
                <p><a class="btn btn-lg btn-primary" href="#" role="button">Sign up today</a></p>
            </div>
            </div>
            </div>   
          </div>
          <a class="slide_btn ad_left hidden" href="#my_carousel" data-slide="prev"><i class="fa fa-chevron-left fa-lg"></i></a>
          <a class="slide_btn hidden" href="#my_carousel" data-slide="next"><i class="fa fa-chevron-right fa-lg"></i></a>
            
          
    <div id="net_slide_wrap" class="col-sm-7 hidden pull-right">
        <div class="col-sm-10" id="form_slide">
            <h3 class="text-center">Select Network Service<span><button id='slide_close_net' type="button" class="close" aria-hidden="true">&times;</button></span></h3>
            <?php $this->load->view('recharge/network_form'); ?>
        </div>
    </div>
</div>     
</div>
<div class="clearfix"></div>
<div class="container" style="height: 85px;  padding: 5px;">
    <div class="col-sm-3 text-right">
        <h3>Network Services</h3>
    </div>
    <div class="col-sm-7">
    <a href=""><img id="tigo" class="net_link net_loader" src="<?php echo base_url('assets/images/tigo.png'); ?>"></a>
    <a href=""><img id="airtel" class="net_link net_loader" src="<?php echo base_url('assets/images/vodacom.png'); ?>"></a>
    <a href=""><img id="vodacom" class="net_link net_loader" src="<?php echo base_url('assets/images/airtel.png'); ?>"></a>
    <a href=""><img id="zantel" class="net_link net_loader" src="<?php echo base_url('assets/images/zantel.png'); ?>"></a>
    <a href=""><img id="sasatel" class="net_link net_loader" src="<?php echo base_url('assets/images/sasatel.png'); ?>"></a>
    </div>
</div>
<div class="clearfix"></div>
<hr style="margin: 0px;" />

<div id="about" class="same_page">
    <div class="container">
         <h2>About Us.</h2>
        <p>We are giving East Africans and friends of East Africa overseas the better way to "TopUP"  respective prepaid services at convinience of internet connection. Paying with Mobile Money, Paypal and major credit and most debit cards. </p>
        
        
        <p>TopUP is a division of JonSoft Group, For more than seven years, we have been delivering unparalleled quality and reliability within telecommunications and banking domains.

Positioned at the forefront of the Internet & Mobile payments eco-system, JG has established itself as the innovative market leader and has nurtured relationships with tier one carriers and major banks in Africa while providing extraordinary quality and service.</p>
    
    </div>
</div>
<div id="price" class="same_page_1">
    <div class="container text-right">
       <h2>How it Works?</h2>
       <p class="featured">TopUp allows you to reload family & friends or your own prepaid service , from anywhere in the world!</p>
<br>
Top Up in 5 easy steps:
<br>
    Register for free.<br>
    Select a top up value.<br>
    Select your prefared gateway.<br>
    Pay and view your receipt.<br>
    The Prepaid service will receve reaload and confirmation.
    </div>
</div>

<script>
    $('.item').css('min-height',450);
    $('.item').css('height',function(){ 
            return $(window).height() - 160;
    });
    $('#form_slide').css('min-height',function(){ 
            return $(window).height() - 170;
    });
    
    $(document).ready(function(){
         
        $('#my_carousel').on('slide.bs.carousel', function () {
                $('.slide_img').addClass('trans');
        });

        $('#my_carousel').on('slid.bs.carousel', function () {
                    $('.slide_img').removeClass('trans');
        });
        $('#my_carousel').on("mouseenter", function () {
                    $('.slide_btn').fadeIn(500).removeClass('hidden');
        });
        $('#my_carousel').on("mouseleave", function () {
                    $('.slide_btn').hide(500);       
        });

        $(".loader").click(function(){
           $('#net_slide_wrap').fadeOut().addClass('hidden');
           $('#slides').addClass('hidden');
           $( "#my_carousel").unbind( "mouseenter" );
           $( "#my_carousel").unbind( "mouseleave" );
           $('.slide_btn').addClass('hidden');
           $('.carousel-indicators').addClass('hidden');
           $( "#country" ).trigger("hidden.bfhselectbox");
           $('#form_slide_wrap').fadeIn().removeClass('hidden');
           return false;
       });
       
        $(".net_loader").click(function(){
           $('#form_slide_wrap').fadeOut().addClass('hidden');
           $('#slides').addClass('hidden');
           $( "#my_carousel").unbind( "mouseenter" );
           $( "#my_carousel").unbind( "mouseleave" );
           $('.slide_btn').addClass('hidden');
           $('.carousel-indicators').addClass('hidden');
           $( "#country_net" ).trigger("hidden.bfhselectbox");
           $('#net_slide_wrap').fadeIn().removeClass('hidden');
           curr_net = $(this).attr('id');
           return false;
       });

       $('#slide_close').click(function(){
                $('#form_slide_wrap').fadeOut().addClass('hidden');
                $('#slides').fadeIn().removeClass('hidden');
                $('.carousel-indicators').removeClass('hidden');

                $('#my_carousel').on("mouseenter", function () {
                    $('.slide_btn').fadeIn(500).removeClass('hidden');
                });
                $('#my_carousel').on("mouseleave", function () {
                    $('.slide_btn').hide(500);       
                });
           return false;
       });
       
       $('#slide_close_net').click(function(){
                $('#net_slide_wrap').fadeOut().addClass('hidden');
                $('#slides').fadeIn().removeClass('hidden');
                $('.carousel-indicators').removeClass('hidden');

                $('#my_carousel').on("mouseenter", function () {
                    $('.slide_btn').fadeIn(500).removeClass('hidden');
                });
                $('#my_carousel').on("mouseleave", function () {
                    $('.slide_btn').hide(500);       
                });
           return false;
       });
       
       $("#send_now").click(function(){
           $('#slides').addClass('hidden');
           $( "#my_carousel").unbind( "mouseenter" );
           $( "#my_carousel").unbind( "mouseleave" );
           $('.slide_btn').addClass('hidden');
           $('.carousel-indicators').addClass('hidden');
           $('#login_wrap').fadeIn().removeClass('hidden');
           return false;
       });
    });
</script>

