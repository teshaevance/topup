<!-- Side Bar -->
<div class="col-sm-2">
		<p class="">Dashboard</p>
	    <hr style="margin-top: 0px; border: none; height: 2px; background:#428BCA;">

		<div class="list-group">
		  <a href="<?php echo site_url('/admin/home'); ?>" class="list-group-item">Transactions<span class="glyphicon glyphicon-chevron-right pull-right"></span></a>
		  <a href="<?php echo site_url('/admin/home/statistics'); ?>" class="list-group-item">Statistics<span class="glyphicon glyphicon-chevron-right pull-right"></span></a>
		  <a href="<?php echo site_url('/home/index/form'); ?>" class="list-group-item">Send Credit<span class="glyphicon glyphicon-chevron-right pull-right"></span></a>
          <a href="<?php echo site_url('/home/index/order'); ?>" class="list-group-item">My Orders<span class="glyphicon glyphicon-chevron-right pull-right"></span></a>
          <a href="<?php echo site_url('/home/index/free_sms'); ?>" class="list-group-item">Send Free SMS<span class="glyphicon glyphicon-chevron-right pull-right"></span></a>
          <a href="<?php echo site_url('/home/index/profile'); ?>" class="list-group-item">My Profile<span class="glyphicon glyphicon-chevron-right pull-right"></span></a>
		</div>
	</div>

<div class="col-sm-10">
	<p class="">Transaction Statistics</p>
	<hr style="margin-top: 0px; border: none; height: 2px; background:#428BCA;">

	<!-- Nav tabs -->
	<ul class="nav nav-pills">
	  <li class="active"><a href="#network" data-toggle="tab">Networks Delivery Status: <?php echo $total_trans; ?></a></li>
	  <li><a href="#trans_stat" data-toggle="tab">Transactions Delivery Status: <?php echo $all; ?></a></li>
	  <li><a href="#charges" data-toggle="tab">TopUp Revenue:USD <?php echo $total_charge; ?></a></li>
	  <li><a href="#volume" data-toggle="tab">Total Volume:USD <?php echo $total_volume; ?></a></li>
	  <li><a href="#users" data-toggle="tab">Users: <?php echo $total_user_volume; ?></a></li>
	</ul>

	<hr style="margin-top: 3px; border: none; height: 1px; background:#428BCA;">
	<!-- Tab panes -->
	<div class="tab-content">
	  <div class="tab-pane active" id="network">
	  		<div id="net_chart_all" class="col-sm-6" style=" height: 400px; margin: 0 auto"></div>
	  		<div id="net_chart" class="col-sm-6" style=" height: 400px; margin: 0 auto"></div>	
	  </div>
	  <div class="tab-pane" id="trans_stat">
	  		<div id="trans_chart" class="col-sm-9" style=" height: 400px; margin: 0 auto"></div>
	  </div>
	  <div class="tab-pane" id="charges">
	  		<div id="charge_chart" class="col-sm-9" style=" height: 400px; margin: 0 auto"></div>
	  </div>
	  <div class="tab-pane" id="volume">
	  		<div id="volume_chart" class="col-sm-9" style=" height: 400px; margin: 0 auto"></div>
	  </div>
	  <div class="tab-pane" id="users">
	  		<div id="user_chart" class="col-sm-9" style=" height: 400px; margin: 0 auto"></div>
	  </div>
	</div>
	
	
</div>


<div class="clearfix"></div>
<hr/>


<script src="http://code.highcharts.com/highcharts.js"></script>
<script src="http://code.highcharts.com/modules/exporting.js"></script>
<script>
	//network data delivered
	var tigo = <?php echo $network_data['tigo'] ?>;
	var vodacom = <?php echo $network_data['vodacom'] ?>;
	var airtel = <?php echo $network_data['airtel'] ?>;
	var zantel = <?php echo $network_data['zantel'] ?>;

	//network data all
	var tigo_all = <?php echo $network_data_all['tigo'] ?>;
	var vodacom_all = <?php echo $network_data_all['vodacom'] ?>;
	var airtel_all = <?php echo $network_data_all['airtel'] ?>;
	var zantel_all = <?php echo $network_data_all['zantel'] ?>;


	//network chart
	$(function () {
	    var colors = Highcharts.getOptions().colors,
	        categories = ['Tanzania'],
	        data = [{
	            y: 100,
	            color: colors[0],
	            drilldown: {
	                name: 'Tanzania Operators',
	                categories: ['Vodacom', 'Airtel', 'Tigo', 'Zantel'],
	                data: [vodacom, airtel, tigo, zantel],
	                color: colors[0]
	            }
		        }],


		        browserData = [],
		        versionsData = [],
		        i,
		        j,
		        dataLen = data.length,
		        drillDataLen,
		        brightness;


		    // Build the data arrays
		    for (i = 0; i < dataLen; i += 1) {

		        // add browser data
		        browserData.push({
		            name: categories[i],
		            y: data[i].y,
		            color: data[i].color
		        });

		        // add version data
		        drillDataLen = data[i].drilldown.data.length;
		        for (j = 0; j < drillDataLen; j += 1) {
		            brightness = 0.2 - (j / drillDataLen) / 5;
		            versionsData.push({
		                name: data[i].drilldown.categories[j],
		                y: data[i].drilldown.data[j],
		                color: Highcharts.Color(data[i].color).brighten(brightness).get()
		            });
		        }
		    }

		    // Create the chart
		    $('#net_chart').highcharts({
		        chart: {
		            type: 'pie'
		        },
		        title: {
		            text: 'Delivered Transactions Per Network'
		        },
		        yAxis: {
		            title: {
		                text: 'Percent tranactions per network'
		            }
		        },
		        plotOptions: {
		            pie: {
		                shadow: false,
		                center: ['50%', '50%']
		            }
		        },
		        tooltip: {
		            valueSuffix: '%'
		        },
		        series: [{
		            name: 'Country',
		            data: browserData,
		            size: '60%',
		            dataLabels: {
		                formatter: function () {
		                    return this.y > 5 ? this.point.name : null;
		                },
		                color: 'white',
		                distance: -30
		            }
		        }, {
		            name: 'Operator',
		            data: versionsData,
		            size: '80%',
		            innerSize: '60%',
		            dataLabels: {
		                formatter: function () {
		                    // display only if larger than 1
		                    return this.y > 1 ? '<b>' + this.point.name + ':</b> ' + this.y + '%'  : null;
		                }
		            }
		        }]
		    });
	});

	//All network chart
	$(function () {
	    var colors = Highcharts.getOptions().colors,
	        categories = ['Tanzania'],
	        data = [{
	            y: 100,
	            color: colors[0],
	            drilldown: {
	                name: 'Tanzania Operators',
	                categories: ['Vodacom', 'Airtel', 'Tigo', 'Zantel'],
	                data: [vodacom_all, airtel_all, tigo_all, zantel_all],
	                color: colors[0]
	            }
		        }],


		        browserData = [],
		        versionsData = [],
		        i,
		        j,
		        dataLen = data.length,
		        drillDataLen,
		        brightness;


		    // Build the data arrays
		    for (i = 0; i < dataLen; i += 1) {

		        // add browser data
		        browserData.push({
		            name: categories[i],
		            y: data[i].y,
		            color: data[i].color
		        });

		        // add version data
		        drillDataLen = data[i].drilldown.data.length;
		        for (j = 0; j < drillDataLen; j += 1) {
		            brightness = 0.2 - (j / drillDataLen) / 5;
		            versionsData.push({
		                name: data[i].drilldown.categories[j],
		                y: data[i].drilldown.data[j],
		                color: Highcharts.Color(data[i].color).brighten(brightness).get()
		            });
		        }
		    }

		    // Create the chart
		    $('#net_chart_all').highcharts({
		        chart: {
		            type: 'pie'
		        },
		        title: {
		            text: 'All Transactions Per Network'
		        },
		        yAxis: {
		            title: {
		                text: 'Percent tranactions per network'
		            }
		        },
		        plotOptions: {
		            pie: {
		                shadow: false,
		                center: ['50%', '50%']
		            }
		        },
		        tooltip: {
		            valueSuffix: '%'
		        },
		        series: [{
		            name: 'Country',
		            data: browserData,
		            size: '60%',
		            dataLabels: {
		                formatter: function () {
		                    return this.y > 5 ? this.point.name : null;
		                },
		                color: 'white',
		                distance: -30
		            }
		        }, {
		            name: 'Operator',
		            data: versionsData,
		            size: '80%',
		            innerSize: '60%',
		            dataLabels: {
		                formatter: function () {
		                    // display only if larger than 1
		                    return this.y > 1 ? '<b>' + this.point.name + ':</b> ' + this.y + '%'  : null;
		                }
		            }
		        }]
		    });
	});


	//other chart
	$(function () {
    $('#charge_chart').highcharts({
        title: {
            text: 'Total Monthly Charges per Network',
            x: -20 //center
        },
        subtitle: {
            text: '',
            x: -20
        },
        xAxis: {
            categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
        },
        yAxis: {
            title: {
                text: 'Total charges in USD'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: ' USD'
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{
            name: 'Tigo',
            data: [<?php echo $charge['tigo']['1'].','.$charge['tigo']['2'].','.$charge['tigo']['3'].','.$charge['tigo']['4'].','.$charge['tigo']['5'].','.$charge['tigo']['6'].','.$charge['tigo']['7'].','.$charge['tigo']['8'].','.$charge['tigo']['9'].','.$charge['tigo']['10'].','.$charge['tigo']['11'].','.$charge['tigo']['12']; ?>]
        }, {
            name: 'Vodacom',
            data: [<?php echo $charge['vodacom']['1'].','.$charge['vodacom']['2'].','.$charge['vodacom']['3'].','.$charge['vodacom']['4'].','.$charge['vodacom']['5'].','.$charge['vodacom']['6'].','.$charge['vodacom']['7'].','.$charge['vodacom']['8'].','.$charge['vodacom']['9'].','.$charge['vodacom']['10'].','.$charge['vodacom']['11'].','.$charge['vodacom']['12']; ?>]
        }, {
            name: 'Airtel',
            data: [<?php echo $charge['airtel']['1'].','.$charge['airtel']['2'].','.$charge['airtel']['3'].','.$charge['airtel']['4'].','.$charge['airtel']['5'].','.$charge['airtel']['6'].','.$charge['airtel']['7'].','.$charge['airtel']['8'].','.$charge['airtel']['9'].','.$charge['airtel']['10'].','.$charge['airtel']['11'].','.$charge['airtel']['12']; ?>]  
        }, {
            name: 'Zantel',
            data: [<?php echo $charge['zantel']['1'].','.$charge['zantel']['2'].','.$charge['zantel']['3'].','.$charge['zantel']['4'].','.$charge['zantel']['5'].','.$charge['zantel']['6'].','.$charge['zantel']['7'].','.$charge['zantel']['8'].','.$charge['zantel']['9'].','.$charge['zantel']['10'].','.$charge['zantel']['11'].','.$charge['zantel']['12']; ?>]
        }]
    });
});

//trans status chart
$(function () {
    $('#trans_chart').highcharts({
        title: {
            text: 'Number of Transactions per Status',
            x: -20 //center
        },
        subtitle: {
            text: '',
            x: -20
        },
        xAxis: {
            categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
        },
        yAxis: {
            title: {
                text: 'Number of Transactions'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: ' transactions'
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{
            name: 'Pending',
            data: [<?php echo $month_trans_stat['pending']['1'].','.$month_trans_stat['pending']['2'].','.$month_trans_stat['pending']['3'].','.$month_trans_stat['pending']['4'].','.$month_trans_stat['pending']['5'].','.$month_trans_stat['pending']['6'].','.$month_trans_stat['pending']['7'].','.$month_trans_stat['pending']['8'].','.$month_trans_stat['pending']['9'].','.$month_trans_stat['pending']['10'].','.$month_trans_stat['pending']['11'].','.$month_trans_stat['pending']['12']; ?>]
        }, {
            name: 'Proccesing',
            data: [<?php echo $month_trans_stat['proccesing']['1'].','.$month_trans_stat['proccesing']['2'].','.$month_trans_stat['proccesing']['3'].','.$month_trans_stat['proccesing']['4'].','.$month_trans_stat['proccesing']['5'].','.$month_trans_stat['proccesing']['6'].','.$month_trans_stat['proccesing']['7'].','.$month_trans_stat['proccesing']['8'].','.$month_trans_stat['proccesing']['9'].','.$month_trans_stat['proccesing']['10'].','.$month_trans_stat['proccesing']['11'].','.$month_trans_stat['proccesing']['12']; ?>]
        }, {
            name: 'Delivered',
            data: [<?php echo $month_trans_stat['delivered']['1'].','.$month_trans_stat['delivered']['2'].','.$month_trans_stat['delivered']['3'].','.$month_trans_stat['delivered']['4'].','.$month_trans_stat['delivered']['5'].','.$month_trans_stat['delivered']['6'].','.$month_trans_stat['delivered']['7'].','.$month_trans_stat['delivered']['8'].','.$month_trans_stat['delivered']['9'].','.$month_trans_stat['delivered']['10'].','.$month_trans_stat['delivered']['11'].','.$month_trans_stat['delivered']['12']; ?>]  
        }]
    });
});


//volume
$(function () {
    $('#volume_chart').highcharts({
        title: {
            text: 'Monthly Transaction Volume',
            x: -20 //center
        },
        subtitle: {
            text: '',
            x: -20
        },
        xAxis: {
            categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
        },
        yAxis: {
            title: {
                text: 'Total volume in USD'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: ' USD'
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{
            name: 'Transaction volume',
            data: [<?php echo $volume['1'].','.$volume['2'].','.$volume['3'].','.$volume['4'].','.$volume['5'].','.$volume['6'].','.$volume['7'].','.$volume['8'].','.$volume['9'].','.$volume['10'].','.$volume['11'].','.$volume['12']; ?>]
        }]
    });
});



//volume
$(function () {
    $('#user_chart').highcharts({
        title: {
            text: 'Cummulative Number of Users per Month',
            x: -20 //center
        },
        subtitle: {
            text: '',
            x: -20
        },
        xAxis: {
            categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
        },
        yAxis: {
            title: {
                text: 'Number of users'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: 'users'
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{
            name: 'Number of users per month',
            data: [<?php echo $user_volume['1'].','.$user_volume['2'].','.$user_volume['3'].','.$user_volume['4'].','.$user_volume['5'].','.$user_volume['6'].','.$user_volume['7'].','.$user_volume['8'].','.$user_volume['9'].','.$user_volume['10'].','.$user_volume['11'].','.$user_volume['12']; ?>]
        }]
    });
});
</script>


