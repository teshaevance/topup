
<!-- Scripts -->
<link type="text/css" rel="Stylesheet" href="<?php echo base_url(); ?>assets/jquery/datatable/jquery.bootstrap.datatable.css">
<script src="<?php echo base_url(); ?>assets/jquery/datatable/jquery.dataTables.js"></script>
<script src="<?php echo base_url(); ?>assets/jquery/datatable/jquery.bootstrap.datatable.js"></script>

<!-- Side Bar -->
<div class="col-sm-2">
		<p class="">Dashboard</p>
	    <hr style="margin-top: 0px; border: none; height: 3px; background:#428BCA;">

		<div class="list-group">
		  <a href="<?php echo site_url('/admin/home'); ?>" class="list-group-item">Transactions<span class="glyphicon glyphicon-chevron-right pull-right"></span></a>
		  <a href="<?php echo site_url('/admin/home/statistics'); ?>" class="list-group-item">Statistics<span class="glyphicon glyphicon-chevron-right pull-right"></span></a>
		  <a href="<?php echo site_url('/home/index/form'); ?>" class="list-group-item">Send Credit<span class="glyphicon glyphicon-chevron-right pull-right"></span></a>
          <a href="<?php echo site_url('/home/index/order'); ?>" class="list-group-item">My Orders<span class="glyphicon glyphicon-chevron-right pull-right"></span></a>
          <a href="<?php echo site_url('/home/index/free_sms'); ?>" class="list-group-item">Send Free SMS<span class="glyphicon glyphicon-chevron-right pull-right"></span></a>
          <a href="<?php echo site_url('/home/index/profile'); ?>" class="list-group-item">My Profile<span class="glyphicon glyphicon-chevron-right pull-right"></span></a>
		</div>
	</div>

<!-- Transaction Table -->
<div id="transaction_table_wrap" class="col-sm-10" style="padding: 10px 3px 0px 0px;">
	<div class="panel panel-success">	
  		<div class="panel-body" style="padding: 5px;">
  			<form role="from" action="<?php echo site_url('admin/home'); ?>" method="GET">
	    		<div class="col-sm-2" style="padding: 3px;" >From 
	    			<div class="bfh-datepicker" data-name="from" data-format="y-m-d" data-date=""></div>
	    		</div>
	    		<div class="col-sm-2" style="padding: 3px;">To
	    			<div class="bfh-datepicker" data-name="to" data-format="y-m-d" data-date=""></div>
	    		</div>
	    		<div class="col-sm-2" style="padding: 3px;">Payment Method
	    			<select class="form-control" name="pay_method">
	    				<option></option>
	    				<option value="mpesa">Mpesa</option>
	    				<option value="tigopesa">TigoPesa</option>
	    				<option value="airtelmoney">Airtel Money</option>
	    				<option value="paypal">Paypal</option>
	    				<option value="creditCard">Credit Card</option>
	    			</select>
	    		</div>
	    		<div class="col-sm-2" style="padding: 3px;">Operator
	    			<select class="form-control" name="operator">
	    				<option></option>
	    				<option value="tigo">Tigo</option>
	    				<option value="vodacom">Vodacom</option>
	    				<option value="airtel">Airtel</option>
	    				<option value="zantel">Zantel</option>
	    			</select>
	    		</div>
	    		<div class="col-sm-2" style="padding: 3px;">Status
		    		<select class="form-control" name="status">
		    			<option></option>
		    			<option value="pending">Pending</option>
		    			<option value="proccesing">Processing</option>
		    			<option value="delivered">Delivered</option>
		    		</select>
	    		</div>
	    		<div class="col-sm-2" style="padding: 3px;">
	    			<br/>
	    			<button type="submit" class="btn btn-primary btn-block">Filter</button>
	    		</div>
    		</form>
  		</div> 		
	</div>
	<?php if($_GET != null){ ?>
		<div class="alert alert-success">
			<a class="alert-link">Current Filters:</a>
			<?php 
				foreach ($_GET as $key => $value) {
					if($value != ''){
						echo ucfirst($key).' : '.$value.' &nbsp;&nbsp;';
					}
				}
			?>
		</div>
	<?php } ?>

    <table id="transaction_table" class="table table-striped table-bordered table-condensed" cellspacing="0" width="100%">
            <thead>
            <tr>
                <th>Reference</th>
                <th>Sender's Email</th>
	            <th>Receiver</th>
	            <th>Amount sent</th>
	            <th>Payment method</th>
	            <th>Balance</th>
	            <th>3rd Delivery ID</th>
	            <th>Operator</th>
	            <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            <?php 
            	foreach ($trans as $key => $value) {
	                echo '<tr>';
	                echo '<td>'.$value['transaction_id'].'</td>';
	                echo '<td>'.$value['email'].'</td>';
	                echo '<td>'.$value['number'].'</td>';
	                echo '<td> TZS '.$value['airtime'].'</td>';
	                echo '<td>'.$value['pay_method'].'</td>';
	                echo '<td>'.$value['balance'].'</td>';
	                echo '<td>'.$value['transaction_id_max'].'</td>';
	                echo '<td>'.$value['dest_operator_name'].'</td>';
	                echo '<td><a id="'.$value['trans_id'].'" class="view_trans btn btn-xs btn-primary">view</a>';
	                echo  	 '<a href="'.site_url("admin/home/edit?id=").$value['trans_id'].'" class="edit_trans btn btn-xs btn-warning push_left_bit">Edit</a></td>';
	                echo '</tr>';
            	}
            ?>
            </tbody>
    </table>
</div>


<div class="clearfix"></div>
<hr/>

<!-- Single Transactons Details -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="panel panel-default">
  		<div class="panel-body" id="single_trans">
    		
  		</div>
	</div>
    </div>
  </div>
</div>

<!-- Script -->
<script>
    $(document).ready(function(){
    //data tables
    $('#transaction_table').dataTable();

    //view a single transaction
    $(".view_trans").click(function(){
        var x = $(this).attr('id');
        $.post( "<?php echo site_url(); ?>/recharge/view_trans/index/"+x, function( data ) {
            $("#single_trans" ).html(data);
            $('.bs-example-modal-lg').modal('show');
        });
        return false;
    });

    });
</script>