<?php //echo '<pre>'; print_r($trans); echo '</pre>'; ?>
<div class="container-fluid">
	<!-- Side Bar -->
	<div class="col-sm-3">
		<p class="">Dashboard</p>
	    <hr style="margin-top: 0px; border: none; height: 3px; background:#428BCA;">

		<div class="list-group">
		  <a href="<?php echo site_url('/admin/home'); ?>" class="list-group-item">Transactions<span class="glyphicon glyphicon-chevron-right pull-right"></span></a>
		  <a href="<?php echo site_url('/admin/home/statistics'); ?>" class="list-group-item">Statistics<span class="glyphicon glyphicon-chevron-right pull-right"></span></a>
		  <a href="<?php echo site_url('/home/index/form'); ?>" class="list-group-item">Send Credit<span class="glyphicon glyphicon-chevron-right pull-right"></span></a>
          <a href="<?php echo site_url('/home/index/order'); ?>" class="list-group-item">My Orders<span class="glyphicon glyphicon-chevron-right pull-right"></span></a>
          <a href="<?php echo site_url('/home/index/free_sms'); ?>" class="list-group-item">Send Free SMS<span class="glyphicon glyphicon-chevron-right pull-right"></span></a>
          <a href="<?php echo site_url('/home/index/profile'); ?>" class="list-group-item">My Profile<span class="glyphicon glyphicon-chevron-right pull-right"></span></a>
		</div>
	</div>

	<div class="col-sm-8">
		<div class="panel panel-primary">
		  <div class="panel-heading">
		    <h3 class="panel-title text-center">Transaction: #<?php echo $trans['transaction_id']; ?></h3>
		  </div>
		  <div class="panel-body">
		  	<?php if(isset($_GET['action']) && $_GET['action'] == 'true'){ ?>
		  		<div class="alert alert-success">
		  			Changes Saved
		  		</div>
		 	<?php }else if(isset($_GET['action']) && $_GET['action'] == 'success'){ ?>
		 		<div class="alert alert-success">
		  			Transaction Successful
		  		</div>
		 	<?php } ?>
		    <form class="form-horizontal" role="form" action="<?php echo site_url(); ?>/admin/home/update/<?php echo $trans['trans_id'] ?>" method="POST">
		    	<h4 class="bottom_10">Transaction Details</h4>
		    	<hr/>
		    	<div class="form-group col-sm-6">
    				<label class="col-sm-4 control-label">Country</label>
			    		<div class="col-sm-8">
				      	<p class="form-control-static"><?php echo $trans['dest_country_name']; ?></p>
				    </div>
			  	</div>
			  	<div class="form-group col-sm-6">
    				<label class="col-sm-4 control-label">Operator</label>
			    		<div class="col-sm-8">
				      	<p class="form-control-static"><?php echo $trans['dest_operator_name']; ?></p>
				    </div>
			  	</div>
			  	<div class="form-group col-sm-6">
    				<label class="col-sm-4 control-label">Receiving Number</label>
			    		<div class="col-sm-8">
				      	<p class="form-control-static"><?php echo $trans['number'] ?></p>
				    </div>
			  	</div>
			  	<div class="form-group col-sm-6">
    				<label class="col-sm-4 control-label">Payment Method</label>
			    		<div class="col-sm-8">
				      	<p class="form-control-static"><?php echo $trans['pay_method'] ?></p>
				    </div>
			  	</div>
			  	<div class="form-group col-sm-6">
    				<label class="col-sm-4 control-label">First Name</label>
			    		<div class="col-sm-8">
				      	<input type="text" class='form-control' name="firstName" value="<?php echo $trans['firstName'] ?>">
				    </div>
			  	</div>
			  	<div class="form-group col-sm-6">
    				<label class="col-sm-4 control-label">Last Name</label>
			    		<div class="col-sm-8">
				      	<input type="text" class='form-control' name="lastName" value="<?php echo $trans['lastName'] ?>">
				    </div>
			  	</div>
			  	<div class="form-group col-sm-6">
    				<label class="col-sm-4 control-label">Airtime Sent</label>
			    		<div class="col-sm-8">
				      	<p class="form-control-static"><?php echo 'TZS '.$trans['airtime'] ?></p>
				    </div>
			  	</div>
			  	<div class="form-group col-sm-6">
    				<label class="col-sm-4 control-label">Charges</label>
			    		<div class="col-sm-8">
				      	<p class="form-control-static"><?php echo 'USD '.$trans['charge'] ?></p>
				    </div>
			  	</div>
			  	<hr class="col-sm-12"  style="margin-top: 5px; margin-bottom: 10px" />
			  	<div class="form-group col-sm-6">
    				<label class="col-sm-4 control-label">Payment Reference</label>
			    		<div class="col-sm-8">
			    		<input type="text" class="form-control" name="payment_ref" value="<?php echo $trans['payment_ref'] ?>" >
				    </div>
			  	</div>
			  	<div class="form-group col-sm-6">
    				<label class="col-sm-6 control-label">Third Party Transaction ID</label>
			    		<div class="col-sm-6">
			    		<input type="text" class="form-control" name="transaction_id_max" value="<?php echo $trans['transaction_id_max'] ?>" >
				    </div>
			  	</div>
			  	<div class="form-group col-sm-6">
    				<label class="col-sm-4 control-label">Status </label>
    				<div class="col-sm-8">
    					<p class="form-control-static"><?php echo $trans['status'] ?></p>
    				</div>
    			</div>

			    <div class="form-group col-sm-6">
			    	<label class="col-sm-6 control-label">Change Status </label>
			    		<div class="col-sm-6">
			    		<select class="form-control" name="status">
			    			<option value="proccesing">Processing</option>
			    			<option value="pending">Pending</option>
			    			<option value="delivered">Delivered</option>
			    		</select>
				    </div>
			  	</div>
			  	
			  	<div class="clearfix"></div>
			  	<h4 class="bottom_10">Sender Details</h4>
			  	<hr/>
			  	<div class="form-group col-sm-6">
    				<label class="col-sm-4 control-label">Name</label>
			    		<div class="col-sm-8">
				      	<p class="form-control-static"><?php echo $trans['first_name'].' '.$trans['last_name'] ?></p>
				    </div>
			  	</div>
			  	<div class="form-group col-sm-6">
    				<label class="col-sm-4 control-label">Email</label>
			    		<div class="col-sm-8">
				      	<p class="form-control-static"><?php echo $trans['email'] ?></p>
				    </div>
			  	</div>
			  	<div class="form-group col-sm-6">
    				<label class="col-sm-4 control-label">Number</label>
			    		<div class="col-sm-8">
				      	<p class="form-control-static"><?php echo $trans['phone'] ?></p>
				    </div>
			  	</div>
			  	<div class="clearfix"></div>
			  	<button type="submit" class="btn btn-primary pull-right push_left_bit">Save Changes</button>
			  	<?php if($trans['status'] == 'pending' || $trans['status'] == 'proccesing'){ ?>
			    		<a href="<?php echo site_url('/admin/home/doTrans?id=').$trans['trans_id']; ?>" class="btn btn-success pull-right push_left_bit">Do Transaction</a>
			  	<?php } ?>
			  	<a href="<?php echo site_url('/admin/home'); ?>" class="btn btn-warning pull-right">Cancel</a>
		    </form>
		  </div>
		</div>
	</div>
</div>
<hr/>