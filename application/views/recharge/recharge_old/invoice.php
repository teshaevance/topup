<div class="container">
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2 bottom_10 top_10" id="invoice">
            <div class="alert alert-info text-center" style="margin-right: -15px; margin-left: -15px; font-size: 115%"><p>TopUp Airtime Invoice<a id="print" href="<?php echo site_url().'/account/my_account/get_pdf/'.$table['trans_id']; ?>"><span class="pull-right push_left_bit">PDF</span><span class="glyphicon glyphicon-print pull-right"></span></a></div>
        <div class="col-sm-4">
            <h5><strong>SOLD TO:</strong></h5>
            <p><?php echo $this->session->userdata('fname')." ".$this->session->userdata('lname')."<br/>";
                echo $this->session->userdata('username')."<br/>";
                echo $this->session->userdata('phone')."<br/>"; ?>
            <br/>
            
            <h5><strong>SUPPLIED TO</strong></h5>
            <P><?php echo "+".$table['number']; ?></P>
            <br/>
            
        </div>
        <div class="col-sm-4 col-sm-offset-1 text-right top_10 te">
            <br/>
            <p> Transaction number<br/>
                Transaction date<br/>
                Payment method<br/>
                Status</p>
        </div>
        <div class="col-sm-3 border_left top_10">
            <br/>
            <p><?php echo $table['transaction_id']."<br/>";
                     echo $table['time']."<br/>";
                     echo ucfirst($table['pay_method'])."<br/>"; 
                     echo $table['status'];?><br/>
            <br/><br/><br/>
        </div>
        <?php  if(!isset($email)){ ?>
        <div class="clearfix"></div>
        <div class="col-sm-4">
            <h5><strong>FREE personalized message</strong></h5>
            <span>(Upto 143 characters)</span>
        </div>
        
        <div class="col-sm-5 no_pad">
            <form id='personal_sms' role='form' method="POST" action="<?php echo site_url(); ?>/recharge/pay/send_sms">
                <textarea id='sms' name="sms" class="form-control" rows="2"></textarea>
                <input class='hidden' type="text" name='sms_num' value="<?php echo $table['number']; ?>">
                <p id='char_count' class="text-right text-success">You have 143 characters</p>
            </form>
        </div>
        <div class="col-sm-2" style="position: relative">
            <br/><br/>
            <button id='send_sms' class="btn btn-sys btn-block">Send</button>
        </div>
        <div class="col-sm-1 no_pad">
            <br/><br/>
            <div id='load' class="no_pad"></div>
        </div>
        <?php } ?>
        <div class="clearfix"></div>
        <div class="table-responsive">
            <table class="table" id="invoice_table">
                <tr style="background-color: #ddd ;border: #999 solid 1px">
                    <th>QUANTITY</th>
                    <th>DESCRIPTION</th>
                    <th>Amount Received</th>
                    <th>Amount Billed</th>
                </tr>
                <tr>
                    <td class="text-center">1</td>
                    <td>Airtime | <?php echo ucfirst($op).' - '.ucfirst($table['dest_country_name']); ?><br/><br/><br/></td>
                    <td class="text-right"><?php echo 'TZS '.$table['amount']; ?></td>
                    <td class="text-right">
                        <?php if($table['pay_method'] != 'paypal') {
                            echo 'TZS '.$table['charge'];
                        }else{
                            echo 'USD '.round((($table['charge'])/1650),2);
                        } ?>                      
                    </td>
                </tr>
                <tr>
                    <td colspan="2"></td>
                    <td class="no_pad">
                        <ul class="list-group text-right">
                            <li class="list-group-item">Sub Total</li>
                            <li class="list-group-item">Charges</li>
                            <li class="list-group-item">Tax</li>
                        </ul>
                    </td>
                    <td class="no_pad">
                        <ul class="list-group text-right">
                            <li class="list-group-item">
                                <?php if($table['pay_method'] != 'paypal') {
                                    echo 'TZS '.$table['amount'];
                                }else{
                                    echo 'USD '.round((($table['amount'])/1650),2);
                                } ?>
                            </li>
                            <li class="list-group-item">
                                <?php if($table['pay_method'] != 'paypal') {
                                    echo 'TZS '.$table['charge'];
                                }else{
                                    echo 'USD '.round((($table['charge'])/1650),2);
                                } ?>  
                            </li>
                            <li class="list-group-item">0</li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="border: none !important;"></td>
                    <td class="text-right">
                        <?php if($table['pay_method'] != 'paypal') {
                             echo 'TZS '.($table['amount']+$table['charge']);
                        }else{
                            echo 'USD '.round(($table['amount']+$table['charge'])/1650,2);
                        } ?>    
                    <br/></td>
                </tr>
            </table>
        </div>
        <div class="row">
            <div class="col-sm-5">
                <strong>For any inquries of this order</strong><br/>
                Click here to open ticket<br/>
                Or email support@topup.co.tz<br/>
                Call us with tickect number: +255 754 55588
                <a>www.topup.co.tz</a>
            </div>
            <div class="col-sm-5 col-sm-offset-2">
                <strong>With Thanks</strong><br/>
                Accounts Receivable<br/>
                JonSoft Group (www.jonsoft.co.tz)<br/>
                7th Floor, Amani Place<br/>
                Ohio Street, Dar es Salaam, Tanzania.
            </div>
        </div>
        </div>
    </div>
</div>
<script>
    var characters = 143;
    var send = false;
    var sent = false;
    $( "#sms" ).on("paste keyup cut",function() { 
        //($(this).val().length);
        if($(this).val().length > characters ){
            $('#char_count').removeClass('text-success');
            $('#char_count').addClass('text-danger');
            $('#char_count').html('You have exceeded number of characters');
            send = false;
        }else{
            var left = characters - $(this).val().length;
            $('#char_count').removeClass('text-danger');
            $('#char_count').addClass('text-success');
            $('#char_count').html(left+' characters left');
            send = true;
        }

    });
    $("#send_sms").click(function(){
            if(send == true && sent == false){
            var t = "<?php echo site_url(); ?>";
            var c = t+"/recharge/pay/send_sms";
            $('#load').html('<img style="height: 35px;" class="center-block" src="<?php echo base_url(); ?>/assets/images/ajax-loader.gif">');
            setTimeout(function(){
            $.post( c, $("#personal_sms").serialize()).done(function(data) {
                if(data == 'true'){
                        $('#load').addClass('text-success');
                        $('#load').html('SMS sent');
                        sent = true;
                }else{
                    $('#load').addClass('text-error');
                    $('#load').html('Not sent, try again');
                }
            });}, 300);
    }
            return false;
        
        });

</script>