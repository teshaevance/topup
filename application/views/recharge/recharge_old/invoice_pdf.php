<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
    <body style="margin: 2px; padding: 0;">
<table align="center" style="width: 100%; font-family: Arial, sans-serif; border-style: solid; border-width: 1px;" cellpadding="0" cellspacing="0">
    <tr style="margin:3px; background-color: #D9EDF7; "><td style=" height: 30px; text-align: center; color: #245269; font-size: 20px; padding: 5px 5px 5px 5px; border-bottom-style: solid; border-bottom-width: 1px; border-color: #31708F">TopUp Airtime Invoice</td></tr>
    <tr><td style="font-size: 0; line-height: 0;" height="20">&nbsp;</td></tr>
    <tr>
        <td>
            <table style="width: 100%;">
                <tr>
                    <td style="width: 30%; padding: 10px 0px 0px 10px; " valign="top">
                        <b>SOLD TO:</b><br/>
                        <?php echo $this->session->userdata('fname')." ".$this->session->userdata('lname')."<br/>";
                            echo $this->session->userdata('username')."<br/>";
                            echo $this->session->userdata('phone')."<br/>"; ?>
                        <br/>

                        <b>SUPPLIED TO:</b><br/>
                        <?php echo "+".$table['number']; ?>
                        <br/>
                    </td>
                    <td style="width: 45%; text-align: right; padding: 10px 10px 0px 0px; border-right-style: solid; border-right-width: 1px;" valign="top">
                        Transaction number<br/>
                        Transaction date<br/>
                        Payment method<br/>
                        Status
                    </td>
                    <td style="width: 25%;text-align: left; padding: 10px 0px 0px 10px;" valign="top">
                        <?php   echo $table['transaction_id']."<br/>";
                                echo $table['time']."<br/>";
                                echo ucfirst($table['pay_method'])."<br/>";
                                echo $table['status']?>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr><td style="font-size: 0; line-height: 0;" height="10">&nbsp;</td></tr>
    <tr>
        <td>
            <table style="width: 98%; border-style: solid; border-width: 1px;" align="center">
                <tr style="background-color: #F8F8F8;">
                    <th style="width: 15%; text-align: center;  border-right-style: solid; border-right-width: 1px">QUANTITY</th>
                    <th style="width: 45%; text-align: center;  border-right-style: solid; border-right-width: 1px">DESCRIPTION</th>
                    <th style="width: 20%; text-align: center;  border-right-style: solid; border-right-width: 1px">Amount Received</th>
                    <th style="width: 20%; text-align: center;">Amount Billed</th>
                </tr> 
                <tr>
                    <td style="text-align: center; padding-top: 10px; border-right-style: solid; border-right-width: 1px" height="120" valign="top">1</td>
                    <td style="text-align: center; padding-top: 10px;  border-right-style: solid; border-right-width: 1px" height="120" valign="top">Airtime | <?php echo ucfirst($op).' - '.ucfirst($table['dest_country_name']); ?></td>
                    <td style="text-align: right; padding-top: 10px;  border-right-style: solid; border-right-width: 1px" height="120" valign="top"><?php echo 'TZS '.$table['amount']; ?></td>
                    <td style="text-align: right; padding-top: 10px;" height="120" valign="top">
                        <?php if($table['pay_method'] != 'paypal') {
                            echo 'TZS '.$table['charge'];
                        }else{
                            echo 'USD '.round((($table['charge'])/1650),2);
                        } ?>   
                    </td>  
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table style="width: 40.5%; margin-right: 3px;" align="right" border="1" cellpadding="2" cellspacing="0">
                <tr>
                    <td style="width: 50%; text-align: right;" height="25">Sub Total</td>
                    <td style="width: 50%; text-align: right;" height="25">
                        <?php if($table['pay_method'] != 'paypal') {
                            echo 'TZS '.$table['amount'];
                        }else{
                            echo 'USD '.round((($table['amount'])/1650),2);
                        } ?>
                    </td>
                </tr> 
                <tr>
                    <td style="width: 50%; text-align: right;" height="25">Charges</td>
                    <td style="width: 50%; text-align: right;" height="25">
                        <?php if($table['pay_method'] != 'paypal') {
                            echo 'TZS '.$table['charge'];
                        }else{
                            echo 'USD '.round((($table['charge'])/1650),2);
                        } ?>  
                    </td>
                </tr> 
                <tr>
                    <td style="width: 50%; text-align: right;" height="25">Tax</td>
                    <td style="width: 50%; text-align: right;" height="25">0</td>
                </tr>
                <tr>
                    <td style="width: 50%; text-align: right;" height="25"><b>Total</b></td>
                    <td style="width: 50%; text-align: right;" height="25"><b>
                        <?php if($table['pay_method'] != 'paypal') {
                             echo 'TZS '.($table['amount']+$table['charge']);
                        }else{
                            echo 'USD '.round(($table['amount']+$table['charge'])/1650,2);
                        } ?>      
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr><td style="font-size: 0; line-height: 0;" height="130">&nbsp;</td></tr>
    <tr>
        <td>
            <table style="width: 100%">
                <tr>
                    <td style="text-align: center; width: 50%;" valign="top">
                        For any inquries of this order<br/>
                        Click here to open ticket<br/>
                        Or email support@topup.co.tz<br/>
                        Call us with ticket number: +255 754 55588<br/>
                        www.topup.co.tz  
                    </td>
                    <td style="text-align: center; width: 50%;" valign="top">
                        With Thanks<br/>
                        Accounts Receivable<br/>
                        JonSoft Group (www.jonsoft.co.tz)<br/>
                        7th Floor, Amani Place<br/>
                        Ohio Street, Dar es Salaam, Tanzania.
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    
</table>
<table style="width: 100%" align="center" cellpadding="0" cellspacing="0">
    <tr><td style="font-size: 0; line-height: 0;" height="10">&nbsp;</td></tr>
    <tr style="height: 55px; background-color: #f8f8f8; "><td style="text-align: center; font-size: 15px; padding: 10px 10px 8px 5px;">© Copyright 2014 by JonSoft Group,Amani Place, Ohio St, Dar es Salaam. All Rights Reserved </td></tr>
</table>
    </body>
</html>