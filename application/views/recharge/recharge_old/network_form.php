<form id='net_form' class="form-horizontal" role="form" action="<?php if($this->session->userdata('user_id') != NULL){echo site_url().'/recharge/confirm/index/false';}else{echo site_url().'/access/login_logout/fly';}?>" method="post">          
    <div class="form-group">
        <label for="country_net" class="col-sm-3 control-label">Country</label>
        <div class="col-sm-8">
            <div  id="country_net" class="bfh-selectbox bfh-countries" data-name="country" data-filter="true" data-flags="true" data-available="TZ,KE,UG" data-country="TZ"></div>
        </div>
    </div>
    
    <div class="col-sm-12"><hr></div>
    <div id='phone_sup_wrap_net' class="form-group">
      <label for="phone" class="col-sm-3 control-label" style="color: black !important;">Mobile Number</label>
        <div class="col-sm-8">
            <div class="input-group hidden" id='phone_wrap_net' data-toggle="tooltip" data-placement="top" title="Do not start with Zero, International codes automatically added by system: Eg. 653222222">
 
            </div>
        </div>
    </div>

    <div class="_hr col-sm-12"><hr></div>
    
    <div class="form-group">
        <div class="text-center" id="op_msg_net"></div>
        <label id="op_label_net" for="operator" class="col-sm-3 control-label">Operator</label>
        <div id="op_icons_net"></div>
    </div>
    <div class="col-sm-12"><hr></div>
    <div class="clearfix"></div>
    <div class="form-group hidden serv" id="tigo_">  
       <label class="control-label col-sm-3">Services</label>
      <div class="radio col-sm-6">
        <label>
          <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>
          Tigo Extreme
        </label>
      </div>
      <div class="radio col-sm-6 col-sm-offset-3">
        <label>
          <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
          Tigo Megabox
        </label>
      </div>
    </div>
    <div class="form-group hidden serv" id="vodacom_">  
        <label class="control-label col-sm-3">Services</label>
      <div class="radio col-sm-6">
        <label>
          <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>
          Vodacom Cheka
        </label>
      </div>
      <div class="radio col-sm-6 col-sm-offset-3">
        <label>
          <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
          Vodacom Internet
        </label>
      </div>
    </div>
    <div class="form-group hidden serv" id="airtel_">  
        <label class="control-label col-sm-3">Services</label>
      <div class="radio col-sm-6">
        <label>
          <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>
          Airtel yatosha 500tsh
        </label>
      </div>
      <div class="radio col-sm-6 col-sm-offset-3">
        <label>
          <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
          Airtel Yatosha 1500tsh
        </label>
      </div>
    </div>
    <div class="form-group" id="conti">
        <button class="btn btn-primary col-sm-2 col-sm-offset-9">Continue</button>
    </div>
    
</form>

<script>
//trigger country change action 
$(document).ready(function(){  
    var maxLen = 10;
        $('#phone_num_net').keypress(function(event){
            var Length = $("#phone_num_net").val().length;
            if(Length >= maxLen){
                if (event.which !== 8) {
                    return false;
                }
            }
        });
    $('#country_net').on("hidden.bfhselectbox" , function () {
        $('#phone_wrap_net').tooltip();
        //diable others
        $('#cash_list_net').addClass('hidden');
        $('#op_icons_net').html('');
        $('#op_msg_net').html('');
        $('#am_msg_net').addClass('hidden');
       $('.serv').addClass('hidden');
       $('#conti').addClass('hidden');
        
        var country_code = $(this).val();
        $("#phone_wrap_net").html('<div class="col-sm-1 col-sm-offset-8" ><img style="height: 35px;" class="center-block" src="<?php echo base_url(); ?>/assets/images/ajax-loader.gif"></div>');
        $('#phone_wrap_net').removeClass('hidden');    
        setTimeout(function(){
            $.post( "<?php echo site_url() ?>/recharge/order/gen_phonecode", { id: country_code } ).done(function( data ) {
                if(data != ''){
                var pre_len = data.length;
                //Write the phone text field
                var j = '<span class="input-group-addon">+'+ data +'</span>'+' <input id="phone_num_net" name="phone" type="text" class="form-control">';
                $('#phone_wrap_net').html(j);
                $('#phone_wrap_net').append('<span id="stat_net" class="hidden input-group-addon"><span class="glyphicon glyphicon-ok"</span>');
                $('#phone_wrap_net').append('<span id="statt_net" class="hidden input-group-addon"><span class="glyphicon glyphicon-remove"</span>');
                //limit the number of characters
                $('#phone_num_net').keypress(function(event){
                    var Length = $("#phone_num_net").val().length;
                    if(Length >= (12-pre_len)){
                        if (event.which !== 8) {
                            return false;
                        }
                    }
                });
                
                //Check the number and validate
                $( "#phone_num_net" ).on("keyup cut paste",function() {
                    $('.serv').addClass('hidden');
                    $('#conti').addClass('hidden');
                    $('#phone_wrap_net').tooltip('hide');
                    setTimeout(function(){
                    if($.isNumeric($('#phone_num_net').val()) === true && $('#phone_num_net').val().length === (12-pre_len)){
                            $('#phone_sup_wrap_net').removeClass('has-error');
                            $('#phone_sup_wrap_net').addClass('has-success');
                            $('#phone_sup_wrap_net').addClass('has-feedback');                               
                            $('#stat_net').removeClass('hidden');
                            $('#statt_net').addClass('hidden');
                            var number = $('#phone_num_net').val();
                            get_ops(country_code,number);
                        }else{
                            
                            $('#phone_sup_wrap_net').addClass('has-error');
                            $('#phone_sup_wrap_net').addClass('has-feedback');
                            $('#cash_list_net').addClass('hidden');
                            $('#op_icons_net').html('');
                            $('#op_msg_net').html('');
                            $('#am_msg_net').addClass('hidden');

                            $('#statt_net').removeClass('hidden');
                            $('#stat_net').addClass('hidden');
                        } 
                      },30);  
                });  
                }else{
                    var j = '<span class="input-group-addon">+XXX</span>'+' <input id="phone_num_net" name="phone" type="text" class="form-control">';
                    $('#phone_wrap_net').html(j);
                    $("#op_icons_net").html('<p class="form-control-static col-sm-7">Sorry! No operators supported for this country yet</p>');
            }
            }); 
            
                
        },100); 
    });
    
    function get_ops(country_code,num){
        // get caash per country
            $.post( "<?php echo site_url() ?>/recharge/order/cash_list", { id: country_code, num: num } ).done(function( data ) {
                $('#cash_list_net').html(data);
            });
            
            //get operators
            $("#op_icons_net").html('<div class="col-sm-1 col-sm-offset-3" ><img style="height: 35px;" class="center-block" src="<?php echo base_url(); ?>/assets/images/ajax-loader.gif"></div>');
            setTimeout(function(){
                $.post( "<?php echo site_url() ?>/recharge/order", { code: country_code, num: num  } ).done(function( data ) {
                var operator_json = JSON.parse(data);
                $("#op_icons_net").html('');
                if(operator_json['pre'].op_pre !== null){
                    var m = operator_json['pre'].op_pre;
                    $('#am_label_net').removeClass('hidden');
                for(var i=0;i < operator_json['result'].length;i++){
                    var z = "<?php echo base_url(); ?>";
                    var w = operator_json['result'][i].operator_name;
                    var u = operator_json['result'][i].operator_id;
                    var k = operator_json['result'][i].country_id;                  
                    var x = "<div style='padding: 10px 5px' class='radio radio-inline no_indent col-sm-2 col-xs-5 center-block'><label><input type='radio' class='hidden' name='operator'" + " id='operator' value='"+u+" ' >";
                    var y = "<img id='"+w+"' src="+z+"assets/images/"+w+".png" + " alt='"+w+"' class='img-thumbnail operator'></label></div>";
                    if(u === m){
                        $('#op_msg_net').html('You have entered a '+operator_json['result'][i].operator_name+' number.');
                        curr_net = operator_json['result'][i].operator_name;
                         var x = "<div style='padding: 10px 5px' class='radio radio-inline no_indent col-sm-2 col-xs-5 center-block'><label><input type='radio' class='hidden' name='operator'" + " id='operator' value='"+u+"' checked>";
                         var y = "<img id='"+w+"' src="+z+"assets/images/"+w+".png" + " alt='"+w+"' class='img-thumbnail operator operator_active'></label></div>";
                        $("#op_icons_net").append(x+y);
                    }else{
                        $("#op_icons_net").append(x+y);
                    }  
                }
                $('#'+curr_net+'_').removeClass('hidden');
                $('#conti').removeClass('hidden');
                $('#cash_list_net').fadeIn('slow').removeClass('hidden');     
                $(".cash").click(function(){
                    $(".cash").removeClass('cash_active');
                    $(this).addClass('cash_active');
                    $(this).prev('input').attr('checked','checked');

                    $('#order_form_net').submit();
                return false;
                });
                }else{
                    $("#op_icons_net").html('<p class="form-control-static col-sm-7">Sorry! Unrecognized operator</p>');
                
                }
                });
                },300);
    }
});
</script>
    