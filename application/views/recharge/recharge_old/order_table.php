<h4 style="margin: 2px;">Your Order<span><?php echo ': #'.$curr_trans['transaction_id']; ?></span></h4>
              <div class="table-responsive">
              <table class="table" id="order">
                    <tr>
                        <td>Operator</td>
                        <td><?php echo ucfirst($operator).' - '.ucfirst($curr_trans['dest_country_name']); ?></td>
                    </tr>
                    <tr>
                        <td>Number to Receive</td>
                        <td><?php echo $curr_trans['number']; ?></td>                     
                    </tr>
                    <tr>
                        <td>Amount to be Received</td>
                        <td><?php echo $curr_trans['currency'].' '.$curr_trans['amount']; ?></td>
                    </tr>
                    <tr>
                        <td>Charge</td>
                        <td class="charge"><?php echo $curr_trans['currency'].' '.$curr_trans['charge']; ?></td>
                    </tr>
                    <tr>
                        <td><strong>Total</strong></td>
                        <td><strong class="total"><?php  echo $curr_trans['currency'].' '.strval($curr_trans['charge']+$curr_trans['amount']); ?></strong></td>
                    </tr>
            </table>
        </div>