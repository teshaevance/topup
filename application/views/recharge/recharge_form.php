<form id='order_form' class="form-horizontal" role="form" action="<?php if($this->session->userdata('user_id') != NULL){echo site_url().'/recharge/confirm/index/false';}else{echo site_url().'/access/login_logout/fly';}?>" method="post">             
    <div class="col-sm-12 no_pad"><hr style="margin-bottom: 10px; margin-top: 5px;"></div>
    <!-- mobile number Input -->
    <div id='phone_sup_wrap' class="form-group">
      <label for="phone" class="col-sm-3 control-label" style="color: black !important;">Mobile Number</label>
        <div class="col-sm-8">
            <div class="input-group hidden" id='phone_wrap' data-toggle="tooltip" data-placement="top" title="Do not start with Zero, International codes automatically added by system: Eg. 653222222">
 
            </div>
        </div>
    </div>

    <div class="col-sm-10 col-sm-offset-1 no_pad"><hr class="no_mag"></div>
    <div class="clearfix"></div>
    
    <div class="form-group" id='op_wrap'>
        <div class="text-center col-sm-offset-2" id="op_msg"></div>
        <label id="op_label" for="operator" class="col-sm-3 control-label">Operator</label>
        <div id="op_icons"></div>  
    </div>
    
    <hr class="col-sm-9 col-sm-offset-1" style="margin-top: 5px; margin-bottom: 10px;"/>
    
    <div class="form-group bottom_10" id='cash_div'>
        <div class="col-sm-6">
            <label class="col-sm-6 control-label">You Send</label>
            <div class="col-sm-6">
                <input name='amount' id='amount' class="form-control" type="text" placeholder="USD">
            </div>
            <div class="clearfix"></div>
            <br/>
            <label class="col-sm-6 control-label">Amount Received</label>
            <div class="col-sm-6">
                <input name='airtime' id='receive' class="form-control" type="text" placeholder="TZS">
            </div>
        </div>
        <div class="col-sm-6">
            <div class="panel panel-primary" style="margin-bottom: 0px; border-radius: 2px;">
            <div class="panel-heading" style="padding: 5px; border-radius: 0px;">
                <h3 class="panel-title text-center">Recharge Details</h3>
            </div>
            <div class="panel-body">
              Rate: 1USD = TZS 1650<br/>
              Charges: USD <span id='charges'>0</span><br/>
              Total: USD <span id='info_amount'>0</span>
            </div>
          </div>
        </div>
    </div>
    
    <hr style="margin-top: 5px; margin-bottom: 10px;"/>
    
    <div class="form-group">
        <div id='msg_send' class="alert text-center alert-info hidden col-sm-6 col-sm-offset-3" style="padding: 10px; margin-bottom: 4px;"></div>
        <button id='send_now' class="btn btn-lg btn-grn col-sm-4 col-sm-offset-4">Send Now!</button>
    </div>
    <input id="temp_country" type="hidden" name="country">
    <input id="temp_operator" type="hidden" name="operator" value="hello">
</form>

<script>
//charge values
var fixed = "<?php echo $this->config->item('fixed_charge'); ?>";
var fraction = "<?php echo $this->config->item('fraction'); ?>";
var rate = "<?php echo $this->config->item('ex_rate'); ?>";
var free_range = "<?php echo $this->config->item('free_charge'); ?>";
var max_charge = "<?php echo $this->config->item('max_charge'); ?>";

//variables
var post_url ="<?php echo site_url('recharge/confirm/set_curr_ord');  ?>";
var num = false;
var mny = false;
//trigger country change action 
$(document).ready(function(){  
    
    //On change country
    $('#country').on("hidden.bfhselectbox" , function () {
        $('#phone_wrap').tooltip();
        //diable others
        $('#op_icons').html('');
        $('#op_msg').html('');
        $('#am_msg').addClass('hidden');
        $('#send_now').addClass('hidden');
          
        var country_code = $(this).val();
        $("#phone_wrap").html('<div class="col-sm-1 col-sm-offset-8" ><img style="height: 35px;" class="center-block" src="<?php echo base_url(); ?>/assets/images/ajax-loader.gif"></div>');
        $('#phone_wrap').removeClass('hidden');    
        setTimeout(function(){
            $.post( "<?php echo site_url() ?>/recharge/order/gen_phonecode", { id: country_code } ).done(function( data ) {
                if(data != ''){
                var pre_len = data.length;
                //Write the phone text field
                var j = '<span class="input-group-addon">+'+ data +'</span>'+' <input id="phone_num" name="phone" type="text" class="form-control">';
                $('#phone_wrap').html(j);
                $('#phone_wrap').append('<span id="stat" class="hidden input-group-addon"><span class="glyphicon glyphicon-ok"</span>');
                $('#phone_wrap').append('<span id="statt" class="hidden input-group-addon"><span class="glyphicon glyphicon-remove"</span>');
                //limit the number of characters
                $('#phone_num').keypress(function(event){
                    var Length = $("#phone_num").val().length;
                    if(Length >= (12-pre_len)){
                        if (event.which !== 8) {
                            return false;
                        }
                    }
                });
                
                //Check the number and validate
                $( "#phone_num" ).on("keyup cut paste",function() {
                    $('#phone_wrap').tooltip('hide');
                    setTimeout(function(){
                    if($.isNumeric($('#phone_num').val()) === true && $('#phone_num').val().length === (12-pre_len)){
                            $('#phone_sup_wrap').removeClass('has-error');
                            $('#phone_sup_wrap').addClass('has-success');
                            $('#phone_sup_wrap').addClass('has-feedback');                               
                            $('#stat').removeClass('hidden');
                            $('#statt').addClass('hidden');
                            var number = $('#phone_num').val();
                            num = true;
                            get_ops(country_code,number);
                        }else{
                            $('#phone_sup_wrap').addClass('has-error');
                            $('#phone_sup_wrap').addClass('has-feedback');
                            $('#op_icons').html('');
                            $('#op_msg').html('');
                            $('#am_msg').addClass('hidden');

                            $('#statt').removeClass('hidden');
                            $('#stat').addClass('hidden');
                            num = false;
                        } 
                      },30);  
                });  
                }else{
                    var j = '<span class="input-group-addon">+XXX</span>'+' <input id="phone_num" name="phone" type="text" class="form-control">';
                    $('#phone_wrap').html(j);
                    $("#op_icons").html('<p class="form-control-static col-sm-7">Sorry! No operators supported for this country yet</p>');
            }
            });        
        },100); 
    });

    function get_ops(country_code,num){       
    //get operators
    $("#op_icons").html('<div class="col-sm-1 col-sm-offset-3" ><img style="height: 35px; margin-top: 15px;" class="center-block" src="<?php echo base_url(); ?>/assets/images/ajax-loader.gif"></div>');
    setTimeout(function(){
        $.post( "<?php echo site_url() ?>/recharge/order", { code: country_code, num: num  } ).done(function( data ) {
        var operator_json = JSON.parse(data);
        $("#op_icons").html('');
        if(operator_json['pre'].op_pre !== null){
            var m = operator_json['pre'].op_pre;
            $('#am_label').removeClass('hidden');
            for(var i=0;i < operator_json['result'].length;i++){
                var z = "<?php echo base_url(); ?>";
                var w = operator_json['result'][i].operator_name;
                var u = operator_json['result'][i].operator_id;
                var k = operator_json['result'][i].country_id;                  
                var x = "<div style='padding: 10px 5px' class='radio radio-inline no_indent col-sm-2 col-xs-5 center-block'><label><input type='radio' class='hidden' " + " id='operator' value='"+u+" ' >";
                var y = "<img id='"+w+"' src="+z+"assets/images/"+w+".png" + " alt='"+w+"' class='img-thumbnail operator'></label></div>";
                if(u === m){
                    $('#op_msg').html('You have entered a '+operator_json['result'][i].operator_name+' number.');
                     var x = "<div style='padding: 5px 0px' class='radio radio-inline no_indent col-sm-2 col-xs-5 center-block'><label><input type='radio' class='hidden' " + " id='operator' value='"+u+"' checked='checked'>";
                     var y = "<img id='"+w+"' src="+z+"assets/images/"+w+".png" + " alt='"+w+"' class='img-thumbnail operator operator_active'></label></div>";
                    $('#temp_operator').attr('value', u);
                    $("#op_icons").append(x+y);
                }else{
                    $("#op_icons").append(x+y);
                }  
        }   
        }else{
            $("#op_icons").html('<p class="form-control-static col-sm-7">Sorry! Unrecognized operator</p>');
        }
        });
        },300);
    }
    //Calculator
    $( "#amount" ).on("keyup cut paste",function() {
        if($(this).val() < 5 ){
            $('#receive').attr('value','');
            $('#receive').val('');
            $('#info_amount').html('0'); 
            
            $('#msg_send').html('Minimum paying amount is USD 5');
            $('#msg_send').removeClass('hidden');
            $('#send_now').addClass('hidden');
        }else{
            $('#msg_send').addClass('hidden');

            var pay = $(this).val();
            var airtime = pay*rate;

            if(pay <= parseInt(free_range)){
                var charges = 0;
            }else{
                var duty = (parseFloat(fraction)*parseFloat(pay)).toFixed(2);
                var charges = Math.round(((parseFloat(fixed) + parseFloat(duty))*100)     ) / 100;
                if(charges > max_charge ){
                    charges =  50;
                }
            }


            var total = parseFloat(pay)  + charges ;
            $('#receive').attr('value',airtime); 
            $('#receive').val(airtime); 
            $('#info_amount').html(total); 
            
            $('#charges').html(charges);
            $('#send_now').fadeIn().removeClass('hidden'); 
        }
    });
    
    //Calculator
    $( "#receive" ).on("keyup",function() {;
        if($(this).val() < 8250 ){
            $('#amount').attr('value','');
            $('#amount').val('');
            $('#info_amount').html('0'); 
            
            $('#msg_send').html('Minimum receiving amount is TZS 8250');
            $('#msg_send').removeClass('hidden');
            $('#send_now').addClass('hidden');
        }else{
            $('#msg_send').addClass('hidden');
            var airtime = $(this).val();
            var pay = airtime/rate;

            if(pay <= parseInt(free_range)){
                var charges = 0;
            }else{
                var duty = (parseFloat(fraction)*pay.toFixed(2)).toFixed(2);
                var charges = Math.round(((parseFloat(fixed) + parseFloat(duty))*100)) / 100;
                if(charges > max_charge ){
                    charges =  50;
                }
            }

            var total = (parseFloat(pay.toFixed(2)) + charges).toFixed(2);

            $('#amount').attr('value',pay.toFixed(2)); 
            $('#amount').val(pay.toFixed(2)); 
            $('#info_amount').html(total); 
            $('#charges').html(charges); 
            $('#send_now').fadeIn().removeClass('hidden'); 
        }
    });
    
    //send now
    <?php if($this->uri->segment(1) != 'home' ){ ?>
    $('#send_now').click(function(){
        if(num == false){
            $('#msg_send').html('Please enter mobile number');
            $('#msg_send').removeClass('hidden');
            $('#msg_send').removeClass('alert-info');
            $('#msg_send').addClass('alert-danger');
        }else{
            //send ajax call
            //alert($('[name="operator"]', '#order_form').val());
            $.post( post_url, { country: $('#country').val(), operator: $('[name="operator"]', '#order_form').val(), amount: $('#amount').val(), airtime: $('#receive').val(),phone: $('#phone_num').val() } ).done(function( data ) {
                 
           },'json');
            $('#msg_send').addClass('hidden');
            $('#msg_send').addClass('alert-info');
            $('#msg_send').removeClass('alert-danger');
            $('#my_carousel').addClass('hidden');
            $('#login_wrap').fadeIn().removeClass('hidden');
            
        }
        return false;
    });

    <?php } else { ?>
        $('#send_now').click(function(){
        if(num == false){
            $('#msg_send').html('Please enter mobile number');
            $('#msg_send').removeClass('hidden');
            $('#msg_send').removeClass('alert-info');
            $('#msg_send').addClass('alert-danger');
        }else{
            //send ajax call
            $('#temp_country').attr('value', $('#country').val());
            $('#order_form').submit();
        }
        return false;
    });
    <?php } ?>
    
    //auto trigger country
    $( "#country" ).trigger("hidden.bfhselectbox");
});
</script>

    