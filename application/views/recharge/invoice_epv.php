<?php 
    if($table['currency'] == 'USD'){
        $airtime = $table['amount'];
        $charge =$table['charge'];
        $total = $airtime + $charge;

    }else{

        $airtime = $table['airtime'];
        $charge = $table['charge'] * (float)$this->config->item('ex_rate');
        $total = $airtime + $charge;
    }
?>
<div class="col-sm-12 bottom_10 top_10" id="invoice_epv">
    <div class="alert alert-info text-center" style="margin-right: -15px; margin-left: -15px; font-size: 115%">TopUp Airtime Invoice<a id="print" href="<?php echo site_url().'/account/my_account/get_pdf/'.$table['trans_id']; ?>"><span class="pull-right push_left_bit">PDF</span><span class="glyphicon glyphicon-print pull-right"></span></a></div>
        <div class="col-sm-4">
            <h5><strong>SOLD TO:</strong></h5>
            <p><?php echo $table['first_name']." ".$table['last_name']."<br/>";
                echo $table['email']."<br/>";
                echo "+".$table['phone']."<br/>"; 
                ?>
            <br/>
            
            <h5><strong>SUPPLIED TO:</strong></h5>
            <P><?php echo "+".$table['number']."<br/>";
                     echo $table['firstName']." ".$table['lastName'];
                ?>
            </P>
            <br/>
            
        </div>
        <div class="col-sm-4 col-sm-offset-1 text-right top_10 te">
            <br/>
            <p> Transaction number<br/>
                Transaction date<br/>
                Payment method<br/>
                Status</p>
        </div>
        <div class="col-sm-3 border_left top_10">
            <br/>
            <p><?php echo $table['transaction_id']."<br/>";
                     echo $table['time']."<br/>";
                     echo ucfirst($table['pay_method'])."<br/>";
                     echo $table['status']?><br/>
            <br/><br/><br/>
        </div>
        <div class="clearfix"></div>
        <div class="table-responsive">
            <table class="table" id="invoice_table">
                <tr style="background-color: #ddd ;border: #999 solid 1px">
                    <th>QUANTITY</th>
                    <th>DESCRIPTION</th>
                    <th>Amount Received</th>
                    <th>Amount Billed</th>
                </tr>
                <tr>
                    <td class="text-center">1</td>
                    <td>Airtime | <?php echo ucfirst($op).' - '.ucfirst($table['dest_country_name']); ?><br/><br/><br/></td>
                    <td class="text-right"><?php echo 'TZS '.$table['airtime'] ?></td></td>
                    <td class="text-right">
                        <?php 
                             echo $table['currency'].' '.$airtime;
                         ?>                    
                    </td>
                </tr>
                <tr>
                    <td colspan="2"></td>
                    <td class="no_pad">
                        <ul class="list-group text-right">
                            <li class="list-group-item">Sub Total</li>
                            <li class="list-group-item">Charges</li>
                            <li class="list-group-item">Tax</li>
                        </ul>
                    </td>
                    <td class="no_pad">
                        <ul class="list-group text-right">
                            <li class="list-group-item">
                                 <?php 
                                     echo $table['currency'].' '.$airtime;
                                 ?>
                            </li>
                            <li class="list-group-item">
                                <?php 
                                     echo $table['currency'].' '.$charge;
                                 ?>   
                            </li>
                            <li class="list-group-item">0</li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="border: none !important;"></td>
                    <td class="text-right">
                        <?php 
                             echo $table['currency'].' '.($total);
                        ?>    
                    <br/></td>
                </tr>
            </table>
        </div>
        <div class="row">
            <div class="col-sm-5 text-center">
                <strong>For any inquries of this order</strong><br/>
                Click here to open ticket<br/>
                Or email support@topup.co.tz<br/>
                Call us with ticket number: +255 754 55588
                <a>www.topup.co.tz</a>
            </div>
            <div class="col-sm-5 col-sm-offset-2 text-center">
                <strong>With Thanks</strong><br/>
                Accounts Receivable<br/>
                JonSoft Group (www.jonsoft.co.tz)<br/>
                7th Floor, Amani Place<br/>
                Ohio Street, Dar es Salaam, Tanzania.
            </div>
        </div>
        </div>
<div class="clearfix"></div>
