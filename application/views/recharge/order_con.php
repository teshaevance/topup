<?php $ref_key = strtoupper(random_string('alnum', 4)); ?>
<div class="container bottom_10">
<div class="row">
    <div class="col-sm-8 col-sm-offset-2">
        <div class="row">            
                    <legend><h3>Choose Payment Method</h3></legend>
                    <div class="tabbable">
                    <ul class="nav nav-tabs">
                      <li class="active">
                        <a href="#1" data-toggle="tab">Through Mobile Money</a>
                      </li>
                      <li>
                        <a href="#2" data-toggle="tab">Through Online Payment</a>
                      </li>
                    </ul>
                    <div class="tab-content">
                    
                        <div class="tab-pane active" id="1">
                            <form id="order_form_mobile" class="form-horizontal" action="<?php echo site_url();?>/recharge/confirm/trans" method="post">
                        <input class="hidden" type="text" name="trans_id" value="<?php echo $curr_trans['trans_id']; ?>">
                        <input class="hidden" type="text" name="number" value="<?php echo $curr_trans['number']; ?>">
                        <input class="hidden" type="text" name="country_id" value="<?php echo $curr_trans['dest_country_id'];?>">
                        <input class="hidden" type="text" name="operator" value="<?php echo $operator; ?>">
                        <input class="hidden" type="text" name="amount" value="<?php echo $curr_trans['amount']; ?>">
                        <input class="hidden" type="text" name="user" value="<?php echo $this->session->userdata('user_id'); ?>"> 
                            <br/>
                            <div class="panel panel-default">
                              <div class="panel-body">
                                    <div class="col-sm-12">
                                        <h3>Your Order #: <span class="label label-warning"><?php echo $curr_trans['transaction_id']; ?></span></h3>
                                        <div >
                                            <table class="table table-striped table-hovered" id="order">
                                                <tbody>
                                                    <tr>
                                                        <td>Operator</td>
                                                        <td><?php echo ucfirst($operator).' - '.ucfirst($curr_trans['dest_country_name']); ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Number to Receive</td>
                                                        <td><?php echo $curr_trans['number']; ?></td>                     
                                                    </tr>
                                                    <tr>
                                                        <td>Amount to be Received</td>
                                                        <td><?php echo 'TZS '.$curr_trans['airtime']; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Charge</td>
                                                        <td class="charge"><?php echo 'TZS '.($curr_trans['charge']*$this->config->item('ex_rate')); ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align:right;"><strong>Total</strong></td>
                                                        <td><strong class="total"><?php  echo 'TZS '.($curr_trans['amount']+$curr_trans['charge'])*$this->config->item('ex_rate'); ?></strong></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <hr/>
                                        <div class="col-sm-6 col-sm-offset-0">
                                                <fieldset>
                                                    <div class="control-group">                                         
                                                        <label for="username">Payment Method</label>
                                                        <div class="controls">
                                                            <Select name="pay_method" id="pay_method" class="form-control">
                                                                <OPTION value="mpesa">MPesa</OPTION>
                                                                <OPTION value="tigopesa">Tigo Pesa</OPTION>
                                                                <OPTION value="airtelmoney">Airtel Money</OPTION>
                                                            </Select>
                                                        </div> <!-- /controls -->               
                                                    </div> <!-- /control-group -->
                                                    <br/>
                                                    <div class="control-group">                                         
                                                        <label class="control-label" for="username">Verification Code</label>
                                                        <div class="controls">
                                                            <input type="text" class="form-control" name="payment_ref" id="payment_ref" value="" required>
                                                            <p class="help-block">Verification Code you received on your phone.</p>
                                                        </div> <!-- /controls -->               
                                                    </div> <!-- /control-group -->
                                                </fieldset>

                                        </div>
                                        <div class="span"></div>
                                        <div class="col-sm-6 col-sm-offset-0 alert-success" id="payment_instructions" style='padding:5px;'>
                                            <div id="instuctions_tigo">
                                                <h5 style='text-align:center;'>TO ADD MONEY WITH TIGO PESA</h5>
                                                <ol>
                                                    <li>Dial *150*01#</li>
                                                    <li>Select 4 for “PAYMENTS”</li>
                                                    <li>Select 2 for ENTER Business Number</li>
                                                    <li>Enter Business Number: #355555</li>
                                                    <li>Enter Reference number: 2XLA</li>
                                                    <li>Enter amount you want to pay: 5.25</li>
                                                    <li>Enter business name: 355555</li>
                                                    <li>Amount in Tshs 5.25</li>
                                                    <li>Enter PIN to confirm</li>
                                                </ol>
                                                <div style="text-align:center">
                                                    <span class="panel">
                                                    You will receive message confirming transaction success, below is an example:
                                                    ” Bill Pay successful. The details are : Txnld XXXXXXXX.YYYY.C00543, Bill number TIKETI, Transaction amount: TZS 14,000 Tsh”
                                                    </span>
                                                </div>
                                            </div>
                                            <div id="instuctions_voda">
                                                <h5 style='text-align:center;'>TO ADD MONEY WITH MPESA</h5>
                                                <ol>
                                                    <li>Dial *150*00#</li>
                                                    <li>Select 4. Business Payments</li>
                                                    <li>Select 1. Enter Business Number : 355555</li>
                                                    <li>Enter Reference No: IF8K</li>
                                                    <li>Enter Amount: 5.25</li>
                                                    <li>Enter your Vodacom M-PESA PIN</li>
                                                </ol>
                                                <div style="text-align:center">
                                                    <span class="panel">
                                                    You should receive an SMS, Below example
                                                    “X03FU201 Confirmed. Tshs 5.25 sent to business JONSOFT GROUP for account TIKETI on March 2, 2014″
                                                    </span>
                                                </div>
                                            </div>
                                            <div id="instuctions_airtel">
                                                <h5 style='text-align:center;'>TO ADD MONEY WITH AIRTEL MONEY</h5>
                                                <ol>
                                                    <li>Dial *150*60#</li>
                                                    <li>Select 1 for ENGLISH / Select 2 for KISWAHILI</li>
                                                    <li>Select 4 MAKE PAYMENTS</li>
                                                    <li>Select OTHERS</li>
                                                    <li>Enter business name: 355555</li>
                                                    <li>Amount in Tshs 5.25</li>
                                                    <li>Select 1. YES to Send 5.25 Tshs to 355555</li>
                                                    <li>Enter PIN</li>
                                                    <li>Enter Reference as: XWHR</li>
                                                </ol>
                                                <div style="text-align:center">
                                                    <span class="panel">
                                                        You will receive message confirming transaction success, below is an example:
                                                        “Reference number 1311024598315 you have sent 5.25 TZS to 355555.”
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                              </div>
                            </div>
                            </form>
                        </div>
                        

                            <div class="tab-pane" id="2">
                            <form id="order_form_online" class="form-horizontal" action="<?php echo site_url();?>/recharge/confirm/trans" method="post">
                            <input class="hidden" type="text" name="trans_id" value="<?php echo $curr_trans['trans_id']; ?>">
                            <input class="hidden" type="text" name="number" value="<?php echo $curr_trans['number']; ?>">
                            <input class="hidden" type="text" name="country_id" value="<?php echo $curr_trans['dest_country_id'];?>">
                            <input class="hidden" type="text" name="operator" value="<?php echo $operator; ?>">
                            <input class="hidden" type="text" name="amount" value="<?php echo $curr_trans['amount']; ?>">
                            <input class="hidden" type="text" name="user" value="<?php echo $this->session->userdata('user_id'); ?>"> 
                            <br/>
                            <div class="panel panel-default">
                              <div class="panel-body">
                                    <div class="col-sm-12">
                                        <div>
                                            <h3>Your Order #: <span class="label label-warning"><?php echo $curr_trans['transaction_id']; ?></span></h3>
                                            <div >
                                                <table class="table table-striped table-hovered" id="order">
                                                    <tbody>
                                                        <tr>
                                                            <td>Operator</td>
                                                            <td><?php echo ucfirst($operator).' - '.ucfirst($curr_trans['dest_country_name']); ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Number to Receive</td>
                                                            <td><?php echo $curr_trans['number']; ?></td>                     
                                                        </tr>
                                                        <tr>
                                                            <td>Amount to be Received</td>
                                                            <td><?php echo 'USD '.$curr_trans['amount']; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Charge</td>
                                                            <td class="charge"><?php echo 'USD '.$curr_trans['charge']; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:right;"><strong>Total</strong></td>
                                                            <td><strong class="total"><?php  echo 'USD '.strval($curr_trans['charge']+$curr_trans['amount']); ?></strong></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <hr/>
                                        <div class="col-sm-4 col-sm-offset-0">
                                                <fieldset>
                                                    <div class="control-group">                                         
                                                        <label for="username">Payment Method</label>
                                                        <div class="controls">
                                                            <Select name="pay_method" id="online_payment_methods" class="form-control">
                                                                <OPTION value="mastercard">Mastercard</OPTION>
                                                                <OPTION value="visa">VISA</OPTION>
                                                                <OPTION value="aypal">Paypal</OPTION>
                                                            </Select>
                                                        </div> <!-- /controls -->               
                                                    </div> <!-- /control-group -->
                                                    <br/>
                                                </fieldset>
                                        </div>
                                        <div class="span"></div>
                                        <div class="col-sm-8 col-sm-offset-0">
                                            <div class="panel panel-default">
                                                <div class="panel-body" style="padding: 25px;">
                                                    <div id="instuctions_mastercard">
                                                            <h5>Mastercard</h5><hr/>
                                                            <div class="form-group">
                                                                <label>Card Holder's Name*</label>
                                                                <input type="text" name='cardHolderName' class="form-control" id="cardHolderName">
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Card Number*</label>
                                                                <input type="text" name='cardNumber' class="form-control" id="cardNumber">
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Security Code*</label>
                                                                <input type="text" name='securityCode' class="form-control" id="securityCode">
                                                            </div>
                                                            <span class="col-sm-offset-3 help-block">Fields marked with * are required.</span>
                                                    </div>
                                                    <div id="instuctions_visa">
                                                      
                                                            <h5>Visa</h5><hr/>
                                                            <div class="form-group">
                                                                <label>Card Holder's Name*</label>
                                                                <input type="text" name='cardHolderName' class="form-control" id="cardHolderName">
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Card Number*</label>
                                                                <input type="text" name='cardNumber' class="form-control" id="cardNumber">
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Security Code*</label>
                                                                <input type="text" name='securityCode' class="form-control" id="securityCode">
                                                            </div>
                                                            <span class="col-sm-offset-3 help-block">Fields marked with * are required.</span>
                                                     
                                                    </div>
                                                    <div id="instuctions_paypal">
                                                     
                                                            <h5>Paypal</h5><hr/>
                                                            <div class="form-group">
                                                                <label>Paypal Email*</label>
                                                                <input type="text" name='paypal_email' class="form-control" id="cardHolderName">
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Paypal Password*</label>
                                                                <input type="password" name='paypal_password' class="form-control" id="cardNumber">
                                                            </div>
                                                            <span class="col-sm-offset-3 help-block">Fields marked with * are required.</span>
                                                     
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </form>
                        </div>
                        
                    </div>
                </div>
        </div>
        <div>
            <button id="submit" type="button" class=" btn  btn-grn pull-right">Confirm<span class="glyphicon glyphicon-arrow-right push_left_bit"></span></button>
            <?php if($login == FALSE){ ?>
            <a href="<?php echo site_url() ?>/landing" class="btn btn-grn pull-left" role="button"><span class="glyphicon glyphicon-arrow-left push_right_bit"></span>Back</a>           
            <?php }else { ?>
            <a href="<?php echo site_url() ?>/home" class="btn btn-grn pull-left" role="button"><span class="glyphicon glyphicon-arrow-left push_right_bit"></span>Back</a>           
            <?php } ?>
        </div> 
    </div>         
</div>
</div>

<script>
var meth = 'null';
$('.panel-default').on('shown.bs.collapse', function (e) {
   $('.method').prop('checked',false);
   $(this).children('div').children('div').children('label').children('.method').prop('checked',true);
   
   meth = $(e.target).prev('div').children('div').children('label').children('.method').attr('id');
    //scroll functions
    $('html, body').animate({
        scrollTop: $(e.target).offset().top - 175
    }, 600);
    });
    
    $('#submit').click(function(){
        if($('#1').hasClass('active')){
            //check for reference number
            if($('#payment_ref').val().length > 0){
                 $('#order_form_mobile').submit();
            }else{
                alert('please fill in the payment reference number');
            }
        }else if($('#2').hasClass('active')){
            if($.isNumeric($('#cardNumber').val()) === true ){
                if($('#cardHolderName').val().length >= (6)){
                    if($('#securityCode').val().length != (0)){
                        $('#order_form_online').submit();
                    }else{
                        alert('Please enter the cards security code');
                    }
                }else{
                    alert('Please enter the card holder"s name');
                }                        
            }else{
                alert('Please enter a valid card number');
            }
           
        }

        return false;
    });
    
    /*$('#submit').click(function(){
        if($('#mpesa').is(':checked') || $('#tigopesa').is(':checked') || $('#airtelmoney').is(':checked') || $('#paypal').is(':checked')){
            var num = jQuery.trim($("#"+meth+"_num").val());
            if(num.length > 0 || meth == 'paypal'){
                if($('#state').val() == 'false'){
                    $('#myModal').modal();
                }else{
                    $('#order_form').submit();
                 }
            }else{
                $("#"+meth+"_num").parent('div').addClass('has-error');
                alert('please fill in the payment reference number');
            }
        }else{
            alert('please choose a payment method to place order!');
        }
        return false;
        });*/
    
</script>
