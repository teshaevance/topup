<?php $ref_key = strtoupper(random_string('alnum', 4)); ?>
<div class="container bottom_10">
<div class="row">
    <div class="col-sm-8 col-sm-offset-2">
        <div>
            <form id="order_form" class="form-horizontal" action="<?php echo site_url();?>/recharge/confirm/trans" method="post">
            <input class="hidden" type="text" name="trans_id" value="<?php echo $curr_trans['trans_id']; ?>">
            <input class="hidden" type="text" name="number" value="<?php echo $curr_trans['number']; ?>">
            <input class="hidden" type="text" name="country_id" value="<?php echo $curr_trans['dest_country_id'];?>">
            <input class="hidden" type="text" name="operator" value="<?php echo $operator; ?>">
            <input class="hidden" type="text" name="amount" value="<?php echo $curr_trans['amount']; ?>">
            <input class="hidden" type="text" name="user" value="<?php echo $this->session->userdata('user_id'); ?>"> 

            <div>
                <h2>Choose Payment Method</h2>
                <div class="panel-group" id="accordion">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="radio" >
                              <label data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                  <input class="method" type="radio" name="pay_method" id="mpesa" value="mpesa" >
                                  Vodacom M-PESA | Pay Bill #355555
                                  <img style="display: inline-block" class="img-responsive" src="<?php echo base_url(); ?>assets/images/mpesa2.png">
                              </label>
                            </div>

                        </div>
                        <div id="collapseOne" class="panel-collapse collapse">
                            <div class="panel-body">
                                    <?php $this->load->view('/recharge/order_table'); ?>
                                    <p>From Vodacom Mobile Phone</p>
                                    <p>Dial *150*00#<br />
                                    Select 4. Business Payments<br />
                                    Select 1. Enter Business Number : 355555<br />
                                    Enter Reference No: <?php echo $ref_key; ?><br />
                                    Enter Amount: <span class="amount"><?php echo strval($curr_trans['charge']+$curr_trans['amount']); ?></span><br />
                                    Enter your Vodacom M-PESA PIN</p>
                                    <p>You should receive an SMS, Below example</p>
                                    <p>&#8220;X03FU201 Confirmed. Tshs <span class="amount"><?php echo strval($curr_trans['charge']+$curr_trans['amount']); ?></span> sent to business JONSOFT GROUP for account TIKETI on  March 2, 2014&#8243;</p>
                                     <div class="form-group">
                                        <label for="mpesa">Enter number</label>
                                        <input type="text" class="form-control" name="mpesa" id='mpesa_num'>
                                      </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="radio">
                              <label  data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                  <input class="method" type="radio" name="pay_method" id="tigopesa" value="tigopesa">
                                  TiGO Pesa | Pay Bill #355555
                                  <img style="display: inline-block" class="img-responsive" src="<?php echo base_url(); ?>assets/images/tigopesa.png">
                              </label>
                            </div>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse">
                            <div class="panel-body">
                               <?php $this->load->view('/recharge/order_table'); ?>
                               <p>From Tigo Mobile Phone</p>
                                <p>Dial *150*01#<br />
                                Select 4 for “PAYMENTS”<br />
                                Select 2 for ENTER Business Number<br/>
                                Enter Business Number: #355555<br />
                                Enter Reference number: <?php echo $ref_key; ?><br />
                                Enter amount you want to pay: <span class="amount"><?php echo strval($curr_trans['charge']+$curr_trans['amount']); ?></span><br />
                                Enter business name: 355555<br />
                                Amount in Tshs <span class="amount"><?php echo strval($curr_trans['charge']+$curr_trans['amount']); ?></span></p>
                                <p>Enter PIN to confirm</p>
                                <p>You will receive message confirming transaction success, below is an example:</p>
                                <p>&#8221; Bill Pay successful. The details are : Txnld XXXXXXXX.YYYY.C00543, Bill number TIKETI, Transaction amount: <span class="amount">TZS 14,000</span> Tsh&#8221;</p>
                                <div class="form-group">
                                    <label for="tigopesa">Enter number</label>
                                    <input type="text" class="form-control" name="tigopesa" id='tigopesa_num'>
                                  </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="radio" >
                                <label data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                                  <input class="method" type="radio" name="pay_method" id="airtelmoney" value="airtelmoney" >
                                  Airtel Money | Payment #355555
                                  <img style="display: inline-block" class="img-responsive" src="<?php echo base_url(); ?>assets/images/airtel-money.png"> 
                                </label>
                            </div>
                        </div>
                        <div id="collapseThree" class="panel-collapse collapse">
                            <div class="panel-body">
                                <?php $this->load->view('/recharge/order_table'); ?>
                               <p>Dial *150*60#<br />
                                Select 1 for ENGLISH / Select 2 for KISWAHILI<br />
                                Select 4 MAKE PAYMENTS<br />
                                Select OTHERS<br />
                                Enter business name: 355555<br />
                                Amount in Tshs <span class="amount"><?php echo strval($curr_trans['charge']+$curr_trans['amount']); ?></span><br />
                                Select 1. YES to Send <span class="amount"><?php echo strval($curr_trans['charge']+$curr_trans['amount']); ?></span> Tshs to 355555<br />
                                Enter PIN<br />
                                Enter Reference as: <?php echo $ref_key; ?></p>
                                <p>You will receive message confirming transaction success, below is an example:</p>
                                <p>&#8220;Reference number 1311024598315 you have sent  <span class="amount"><?php echo strval($curr_trans['charge']+$curr_trans['amount']); ?></span> TZS to 355555.&#8221;</p>
                                <div class="form-group">
                                    <label for="airtelmoney">Enter number</label>
                                    <input type="text" class="form-control" name="airtelmoney" id='airtelmoney_num'>
                                </div>
                        </div>
                        </div>
                        </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="radio" >
                              <label data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
                                  <input class="method" style="margin-top: auto !important;" type="radio" name="pay_method" id="paypal" value="paypal" >
                                  Pay with PayPal 
                                  <img style="display: inline-block" class="img-responsive" src="<?php echo base_url(); ?>assets/images/cards.png">
                              </label>
                            </div>
                        </div>
                        <div id="collapseFour" class="panel-collapse collapse">
                            <div class="panel-body">
                                <?php $this->load->view('/recharge/order_table_usd'); ?>
                                <div class="form-group">
                                    <label for="" class="col-sm-3 control-label">Card Type</label>
                                    <label class="radio-inline col-sm-offset-1">
                                      <input name='cardType' type="radio" value="Visa" id="visa">
                                      <img style="display: inline-block" src="<?php echo base_url(); ?>assets/images/visa.png">
                                    </label>
                                    <label class="radio-inline">
                                        <input name='cardType' type="radio" value="MasterCard" id="master">
                                        <img style="display: inline-block" src="<?php echo base_url(); ?>assets/images/master.png">
                                    </label>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group">
                                    <label for="" class="col-sm-3 control-label">Card Holder's Name*</label>
                                    <div class="col-sm-6">
                                        <input type="text" name='cardHolderName' class="form-control" id="cardHolderName">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-3 control-label">Card Number*</label>
                                    <div class="col-sm-6">
                                        <input type="text" name='cardNumber' class="form-control" id="cardNumber">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-3 control-label">Security Code*</label>
                                    <div class="col-sm-6">
                                        <input type="text" name='securityCode' class="form-control" id="securityCode">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-3 control-label">Billing Name</label>
                                    <div class="col-sm-6">
                                        <input type="text" name='billingName' class="form-control" id="billingName">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-3 control-label">Billing Address</label>
                                    <div class="col-sm-6">
                                        <input type="text" name='billingAdd' class="form-control" id="billingAdd">
                                    </div>

                                </div>
                                <span class="col-sm-offset-3 help-block">Fields marked with * are required.</span>
                            </div>
                        </div>
                    </div>
              </div>
              </div>
            </form>
        </div>
          <button id="submit" type="button" class=" btn  btn-grn pull-right">Confirm<span class="glyphicon glyphicon-arrow-right push_left_bit"></span></button>
            <?php if($login == FALSE){ ?>
            <a href="<?php echo site_url() ?>/landing" class="btn btn-grn pull-left" role="button"><span class="glyphicon glyphicon-arrow-left push_right_bit"></span>Back</a>           
            <?php }else { ?>
            <a href="<?php echo site_url() ?>/home" class="btn btn-grn pull-left" role="button"><span class="glyphicon glyphicon-arrow-left push_right_bit"></span>Back</a>           
            <?php } ?>
    </div>

</div>
</div>

<script>
var meth = 'null';
$('.panel-default').on('shown.bs.collapse', function (e) {
   $('.method').prop('checked',false);
   $(this).children('div').children('div').children('label').children('.method').prop('checked',true);
   
   meth = $(e.target).prev('div').children('div').children('label').children('.method').attr('id');
    //scroll functions
    $('html, body').animate({
        scrollTop: $(e.target).offset().top - 175
    }, 600);
    });
    
    
    $('#submit').click(function(){
        if($('#mpesa').is(':checked') || $('#tigopesa').is(':checked') || $('#airtelmoney').is(':checked') || $('#paypal').is(':checked')){
            var num = jQuery.trim($("#"+meth+"_num").val());
            if(meth == 'paypal'){
                if($('#visa').is(':checked') || $('#master').is(':checked')){
                    if($.isNumeric($('#cardNumber').val()) === true && $('#cardNumber').val().length === (14)){
                        if($('#cardHolderName').val().length >= (6)){
                            if($('#securityCode').val().length != (0)){
                                 $('#order_form').submit();
                             }else{
                                alert('Please enter the cards security code');
                             }
                        }else{
                            alert('Please enter the card holder"s name');
                        }                        
                    }else{
                        alert('Please enter a valid card number');
                    }
                }else{
                    alert('please select a card type');
                }    

            }else{
                if(num.length > 0 ){
                    if($('#state').val() == 'false'){
                        $('#myModal').modal();
                    }else{
                        $('#order_form').submit();
                     }
                }else{
                    $("#"+meth+"_num").parent('div').addClass('has-error');
                    alert('please fill in the payment reference number');
                }
            }
        }else{
            alert('please choose a payment method to place order!');
        }
        return false;
        });
    
</script>
