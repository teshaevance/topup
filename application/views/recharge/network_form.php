
<div class="alert alert-success text-center" style="margin-top: 10px;"><h4 style="margin-bottom: 10px;">Welcome To Tigo Sevices<span class=push_left_bit><img style="height: 12%; width: 12%" alt="" src="<?php echo base_url('assets/images/tigo.png'); ?>"></span></h4></div>
<form id='net_form' class="form-horizontal" role="form" action="<?php if($this->session->userdata('user_id') != NULL){echo site_url().'/recharge/confirm/index/false';}else{echo site_url().'/access/login_logout/fly';}?>" method="post">          
    <!-- mobile number Input -->
    <div id='phone_sup_wrap' class="form-group">
      <label for="phone" class="col-sm-3 control-label" style="color: black !important;">Mobile Number</label>
        <div class="col-sm-8">
            <div class="input-group hidden" id='phone_wrap' data-toggle="tooltip" data-placement="top" title="Do not start with Zero, International codes automatically added by system: Eg. 653222222">
 
            </div>
        </div>
    </div>

    <div class="col-sm-10 col-sm-offset-1 no_pad"><hr class="no_mag"></div>
    <div class="clearfix"></div>

    <div class="form-group serv" id="tigo_">  
       <label class="control-label col-sm-3">Services</label>
      <div class="radio col-sm-6">
        <label>
          <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>
          Tigo Extreme
        </label>
      </div>
      <div class="radio col-sm-6 col-sm-offset-3">
        <label>
          <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
          Tigo Megabox
        </label>
      </div>
    </div>

    <div class="col-sm-10 col-sm-offset-1 no_pad"><hr class="bottom_10"></div>
    <div class="clearfix"></div>

    <div class="form-group bottom_10" id='cash_div'>
        <div class="col-sm-6">
            <label class="col-sm-6 control-label">Cost</label>
            <div class="col-sm-6">
                <input name='airtime' id='receive' class="form-control" type="text" placeholder="TZS">
            </div>
        </div>
        <div class="col-sm-6">
            <div class="panel panel-primary" style="margin-bottom: 0px; border-radius: 2px;">
            <div class="panel-heading" style="padding: 5px; border-radius: 0px;">
                <h3 class="panel-title text-center">Recharge Details</h3>
            </div>
            <div class="panel-body">
              Rate: 1USD = TZS 1650<br/>
              Charges: USD <span id='charges'>0</span><br/>
              Total: USD <span id='info_amount'>0</span>
            </div>
          </div>
        </div>
    </div>
    
    <div class="form-group" id="conti">
        <button class="btn btn-primary col-sm-2 col-sm-offset-9">Continue</button>
    </div>
    
</form>

<script>
$(document).ready(function(){  
    
    //On change country
    $('#country').on("hidden.bfhselectbox" , function () {
        $('#phone_wrap').tooltip();
        //diable others
        $('#op_icons').html('');
        $('#op_msg').html('');
        $('#am_msg').addClass('hidden');
        $('#send_now').addClass('hidden');
          
        var country_code = $(this).val();
        $("#phone_wrap").html('<div class="col-sm-1 col-sm-offset-8" ><img style="height: 35px;" class="center-block" src="<?php echo base_url(); ?>/assets/images/ajax-loader.gif"></div>');
        $('#phone_wrap').removeClass('hidden');    
        setTimeout(function(){
            $.post( "<?php echo site_url() ?>/recharge/order/gen_phonecode", { id: country_code } ).done(function( data ) {
                if(data != ''){
                var pre_len = data.length;
                //Write the phone text field
                var j = '<span class="input-group-addon">+'+ data +'</span>'+' <input id="phone_num" name="phone" type="text" class="form-control">';
                $('#phone_wrap').html(j);
                $('#phone_wrap').append('<span id="stat" class="hidden input-group-addon"><span class="glyphicon glyphicon-ok"</span>');
                $('#phone_wrap').append('<span id="statt" class="hidden input-group-addon"><span class="glyphicon glyphicon-remove"</span>');
                //limit the number of characters
                $('#phone_num').keypress(function(event){
                    var Length = $("#phone_num").val().length;
                    if(Length >= (12-pre_len)){
                        if (event.which !== 8) {
                            return false;
                        }
                    }
                });
                
                //Check the number and validate
                $( "#phone_num" ).on("keyup cut paste",function() {
                    $('#phone_wrap').tooltip('hide');
                    setTimeout(function(){
                    if($.isNumeric($('#phone_num').val()) === true && $('#phone_num').val().length === (12-pre_len)){
                            $('#phone_sup_wrap').removeClass('has-error');
                            $('#phone_sup_wrap').addClass('has-success');
                            $('#phone_sup_wrap').addClass('has-feedback');                               
                            $('#stat').removeClass('hidden');
                            $('#statt').addClass('hidden');
                            var number = $('#phone_num').val();
                            num = true;
                            get_ops(country_code,number);
                        }else{
                            $('#phone_sup_wrap').addClass('has-error');
                            $('#phone_sup_wrap').addClass('has-feedback');
                            $('#op_icons').html('');
                            $('#op_msg').html('');
                            $('#am_msg').addClass('hidden');

                            $('#statt').removeClass('hidden');
                            $('#stat').addClass('hidden');
                            num = false;
                        } 
                      },30);  
                });  
                }else{
                    var j = '<span class="input-group-addon">+XXX</span>'+' <input id="phone_num" name="phone" type="text" class="form-control">';
                    $('#phone_wrap').html(j);
                    $("#op_icons").html('<p class="form-control-static col-sm-7">Sorry! No operators supported for this country yet</p>');
            }
            });        
        },100); 
    });

    function get_ops(country_code,num){       
    //get operators
    $("#op_icons").html('<div class="col-sm-1 col-sm-offset-3" ><img style="height: 35px; margin-top: 15px;" class="center-block" src="<?php echo base_url(); ?>/assets/images/ajax-loader.gif"></div>');
    setTimeout(function(){
        $.post( "<?php echo site_url() ?>/recharge/order", { code: country_code, num: num  } ).done(function( data ) {
        var operator_json = JSON.parse(data);
        $("#op_icons").html('');
        if(operator_json['pre'].op_pre !== null){
            var m = operator_json['pre'].op_pre;
            $('#am_label').removeClass('hidden');
            for(var i=0;i < operator_json['result'].length;i++){
                var z = "<?php echo base_url(); ?>";
                var w = operator_json['result'][i].operator_name;
                var u = operator_json['result'][i].operator_id;
                var k = operator_json['result'][i].country_id;                  
                var x = "<div style='padding: 10px 5px' class='radio radio-inline no_indent col-sm-2 col-xs-5 center-block'><label><input type='radio' class='hidden' name='operator'" + " id='operator' value='"+u+" ' >";
                var y = "<img id='"+w+"' src="+z+"assets/images/"+w+".png" + " alt='"+w+"' class='img-thumbnail operator'></label></div>";
                if(u === m){
                    $('#op_msg').html('You have entered a '+operator_json['result'][i].operator_name+' number.');
                     var x = "<div style='padding: 5px 0px' class='radio radio-inline no_indent col-sm-2 col-xs-5 center-block'><label><input type='radio' class='hidden' name='operator'" + " id='operator' value='"+u+"' checked>";
                     var y = "<img id='"+w+"' src="+z+"assets/images/"+w+".png" + " alt='"+w+"' class='img-thumbnail operator operator_active'></label></div>";
                    $("#op_icons").append(x+y);
                }else{
                    $("#op_icons").append(x+y);
                }  
        }   
        }else{
            $("#op_icons").html('<p class="form-control-static col-sm-7">Sorry! Unrecognized operator</p>');
        }
        });
        },300);
    }
    
    $( "#country" ).trigger("hidden.bfhselectbox");
    });
</script>
    