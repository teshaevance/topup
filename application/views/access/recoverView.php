<div class="col-sm-6 col-sm-offset-3" style="margin-top: 40px;">
	<div class="panel panel-primary">
	  <div class="panel-heading">
	    <h3 class="panel-title">Recover Password</h3>
	  </div>
	  <div class="panel-body">
	    	<form role="form" action="<?php echo site_url('access/reg/password_reset'); ?>" method="POST">
	    		<?php if(isset($msg)){ ?>
	    			<div class="alert alert-info"><?php echo $msg; ?></div>
	    		<?php } ?>
	    		<div class="form-group">
	    			<?php show_form_error('email'); ?>
	    			<label class="control-label">Enter Email Address</label>
	    			<input name="email"  type="text" class="form-control">
	    		</div>
	    		<div class="form-group">
	    			<button type="submit" class="btn btn-primary pull-right">Send Recovery Link</button>
	    		</div>
	    	</form>
	  </div>
	  <div class="panel-footer">
	  		*Recovery link expires after 30 minutes
	  </div>
	</div>
</div>