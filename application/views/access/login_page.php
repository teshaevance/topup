
<?php
    $data['type'] = 'inverse';
    $data['title'] = 'TopUp | Login';
    $msg = ': Please sign in to access you account';
    if(isset($operation)){
    if($operation == 'fly'){
        $msg =  ': You are entering a secure area! Please sign in/up to proceed to payment';  
    } 
    }
     $this->load->view('templates/header',$data); ?>
<div class="container-fluid" style="padding-bottom: 10px; margin-bottom: 130px;">
    
    <div class="col-sm-8 col-sm-offset-2">
        <h4 class="text-center text-success _bottom">Welcome<span><?php echo  $msg;?></span></h4>
        <div class="clearfix"></div>
        <hr style="margin-top: 5px; border: none; height: 3px; background:#428BCA;">
    </div>
    <div id="login_box" class="col-sm-4 col-sm-offset-4">
     <?php $this->load->view('access/login_box'); ?> 
    </div> 
    <div class="col-sm-8 col-sm-offset-2"><hr style="border: none; height: 3px; background:#428BCA;"></div>
</div>
 <?php $this->load->view('templates/footer'); ?>