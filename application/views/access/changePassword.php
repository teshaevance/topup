<?php if(!isset($error)) { ?>
<div class="col-sm-6 col-sm-offset-3" style="margin-top: 40px;">
	<div class="panel panel-primary">
	  <div class="panel-heading">
	    <h3 class="panel-title">Recover Password</h3>
	  </div>
	  <div class="panel-body">
	    	<form role="form" action="<?php echo site_url("access/reg/new_password").'/'.$token; ?>" method="POST">
	    		<?php if(isset($msg)){ ?>
	    			<div class="alert alert-info"><?php echo $msg; ?></div>
	    		<?php } ?>
	    		<div class="form-group">
	    			
	    			<label class="control-label">Enter New Password</label>
	    			<input name="password"  type="password" class="form-control">
	    			<?php show_form_error('password'); ?>
	    		</div>
	    		<div class="form-group">
	    			
	    			<label class="control-label">Confirm Password</label>
	    			<input name="password_con"  type="password" class="form-control">
	    			<?php show_form_error('password_con'); ?>
	    		</div>
	    		<div class="form-group">
	    			<button type="submit" class="btn btn-primary pull-right">Save</button>
	    		</div>
	    	</form>
	  </div>
	</div>
</div>

<?php }else{ ?>
	<div class="alert alert-info"><?php echo $error; ?></div>
<?php } ?>