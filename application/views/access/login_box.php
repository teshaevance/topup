<?php
$link = site_url()."/access/login_logout/login";
$link2 = site_url()."/access/reg";
    if(isset($operation)){
    if($operation == 'fly'){
        $link = site_url()."/access/login_logout/fly";
        $link2 = site_url()."/access/reg/fly";
    }
    }
?>
<style>
    .login a{
        background-color: #F8F8F8;
        color: #428BCA !important;
        font-size: 120%;
    }
    
    .social{
        padding-left: 3px;
        padding-right: 3px;
    }
    
</style>
<meta name="google-signin-clientid" content="464705124778-t1b0pabu3490uuofsk6pbo5lnd1gduuc.apps.googleusercontent.com" />
<meta name="google-signin-scope" content="profile https://www.googleapis.com/auth/plus.profile.emails.read" />
<meta name="google-signin-cookiepolicy" content="single_host_origin" />


<ul class="nav nav-tabs nav-justified login">
  <li class="<?php if(!isset($mode)){echo 'active';}  ?>" ><a  href="#login" data-toggle="tab">Login</a></li>
  <li  class="<?php if(isset($mode)){echo 'active';}  ?>"><a  href="#sign_up" data-toggle="tab">New User</a></li>
</ul>

<!-- Tab panes -->
<div class="tab-content" id="login_tab" >
      <div class="tab-pane <?php if(!isset($mode)){echo 'active';}  ?>" id="login" style="padding-top: 10px;">
        <form id="login_form" role="form" method="post" action="<?php echo $link; ?>" accept-charset="utf-8">
        <!-- login box header -->
        <div class="form-group">
            <button class="btn btn-block disabled btn-sys">TopUp Login</button>
        </div>
        
        <!-- wrong password or username error -->
        <?php if(isset($error)){ echo "<div class='alert alert-danger'><p class='text-center'>".$error."</p></div>";} ?>
        <div id="modo"></div>
        <!-- body of login box -->
        <div class="col-sm-10 col-sm-offset-1">
            <div class="form-group">
            <?php show_form_error('username'); ?>
            <div class="input-group">
                <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
                <input name="username" type="text" class="form-control" placeholder="Enter email" value="<?php echo set_value('username'); ?>">                
            </div>
            </div>

            <div class="form-group">
            <?php show_form_error('password'); ?>
            <div class="input-group">
                <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
                <input name="password" type="password" class="form-control" placeholder="Enter password" >
            </div>
            </div>
            <div class="form-group">
                <div class="checkbox">
                    <label class="pull-right"><input name="keep_logged" type="checkbox" value="1"  >Remember Me</label>
                </div>
                <button id="sbm" name="submit" value="submit" type="submit" class="btn btn-primary btn-block">Login</button> 
            </div> 
            
        </div> 
        <div class="clearfix"></div>
        <p class="text-center"><a href="<?php echo site_url('access/reg'); ?>" >New User?</a> <a href="<?php echo site_url('access/reg/recover') ?>">Forgot password?</a></p>
        <div class="hr"><hr/></div>
        <div class="col-sm-6 social">
            <button id="log" class="btn btn-danger btn-block social_btn"><i class="fa fa-google-plus fa-2x"></i>&nbsp;Sign In with Google</button>
        </div>
        <div class="col-sm-6 social">
            <button id='fb_lgn' class="btn btn-primary btn-block"><i class="fa fa-facebook fa-2x"></i>&nbsp;Sign In with Facebook</button>
        </div>
        <div class="clearfix"></div>
          <br/>
  </form>
          <div class="clearfix"></div>
    </div>
    <div class="tab-pane <?php if(isset($mode)){echo 'active';}  ?>" id="sign_up" style="padding-top: 10px;">
        <form id="sign_form" role="form" action="<?php echo $link2; ?>" method="POST">
                <div class="form-group">
                    <button class="btn btn-block disabled btn-sys">TopUp Registration</button>
                </div>
            <?php if(isset($reg_error)){ echo "<div class='alert alert-danger'><p class='text-center'>".$reg_error."</p></div>";} ?>
            <div id="modo2"></div>
              <div class="form-group"> 
                <?php show_form_error('fname'); ?>
                <div class="input-group">
                    <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
                    <input type="text" class="form-control" id="fname" name='fname' placeholder="Enter First Name" value="<?php echo set_value('fname'); ?>">
                </div>
              </div>
              <div class="form-group">  
                 <?php show_form_error('lname'); ?>
                  <div class="input-group">
                    <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
                    <input type="text" class="form-control" id="lname" name="lname" placeholder="Enter Last Name" value="<?php echo set_value('lname'); ?>">
                </div>
              </div>
              <div class="form-group">
                 <?php show_form_error('email'); ?>
                 <div class="input-group">
                    <span class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></span>
                    <input type="email" class="form-control" id="email" name="email" placeholder="Enter a Valid Email" value="<?php echo set_value('email'); ?>">
                </div> 
              </div>
              <div class="form-group">
                <?php show_form_error('phone'); ?>
                   <div class="input-group" id="phone_w" data-toggle="tooltip" data-placement="top" title="Use international codes: Eg. 255XXXXXXXXX">
                    <span class="input-group-addon"><span class="glyphicon glyphicon-phone"></span></span>
                    <input type="phone" class="form-control" id="phone" name="phone" placeholder="Enter your Mobile number" value="<?php echo set_value('phone'); ?>" >
                </div>
              </div>
              <div class="form-group">    
                <?php show_form_error('pass'); ?>
                  <div class="input-group">
                    <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
                    <input type="password" class="form-control" id="Password" name="pass" placeholder="Enter Your Password" >
                </div>
              </div>
              <div class="form-group">
                  <?php show_form_error('pass_con'); ?>
                  <div class="input-group">
                    <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
                    <input type="password" class="form-control" id="pass-con" name="pass_con" placeholder="Confirm Your Password">
                </div>
              </div>
            <div class="form-group">
                <p class="form-control-static pull-left text-info">By clicking submit you agree to <a>Terms and Conditions</a></p>
               
                <button class="btn btn-primary pull-right">Submit</button>
            </div>
            <div class="clearfix"></div>
              <br/>
        </form>
        
    </div>

</div>

<script type="text/javascript" lang="en-US">
$(document).ready(function(){  
    $('#phone_w').tooltip();
     $( "#phone" ).on("keyup",function() {
        $('#phone_w').tooltip('hide');
    });
});

$('#fb_lgn').click(function(){
  userLogin();
});
</script>

<script type="text/javascript" parsetags="explicit">
    window.___gcfg = {
    lang: 'en-US'
  };
    //Get the G+ sign in script
        (function() {
        var po = document.createElement('script');
        po.type = 'text/javascript'; 
        po.async = true;
        po.src = 'https://apis.google.com/js/client:plusone.js?onload=render';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(po, s);
    })();
    
    var profile, email, btn_type;
    //Render the Customized Button with Configurations
    function render() {
   // Additional params including the callback, the rest of the params will
   // come from the page-level configuration.
   var additionalParams = {
     'callback': loginFinishedCallback
   };

   // Attach a click listener to a button to trigger the flow.
   var signinButton = document.getElementById('log');
   signinButton.addEventListener('click', function() {
     gapi.auth.signIn(additionalParams); // Will use page level configuration
   });

    }
  /*
   * Triggered when the user accepts the sign in, cancels, or closes the
   * authorization dialog.
   */
  function loginFinishedCallback(authResult) {
    if (authResult) {
      if (authResult['error'] == undefined){
        gapi.client.load('plus','v1', loadProfile);  // Trigger request to get the email address.
      } else {
        console.log('An error occurred');
        console.log(authResult['error']);
      }
    } else {
      console.log('Empty authResult');  // Something went wrong
    }
  }

  /**
   * Uses the JavaScript API to request the user's profile, which includes
   * their basic information. When the plus.profile.emails.read scope is
   * requested, the response will also include the user's primary email address
   * and any other email addresses that the user made public.
   */
  function loadProfile(){
        var request = gapi.client.plus.people.get( {'userId' : 'me'} );
        request.execute(loadProfileCallback);
  }

  /**
   * Callback for the asynchronous request to the people.get method. The profile
   * and email are set to global variables. Triggers the user's basic profile
   * to display when called.
   */
  function loadProfileCallback(obj) {
    profile = obj;

    // Filter the emails object to find the user's primary account, which might
    // not always be the first in the array. The filter() method supports IE9+.
    // get the email from the filtered results, should always be defined.
     email = obj['emails'].filter(function(v) {
        return v.type === 'account'; // Filter out the primary email
    })[0].value; //
    ini_login(profile);
  }

  /**
   * Display the user's basic profile information from the profile object.
   */
  function ini_login(profile){
    console.log(profile);
    var first_name = profile['name']['givenName'];
    var last_name = profile['name']['familyName'];
    var user_email = email;
    
    alert(first_name+' '+last_name+' '+email);
    /*$.post( "<?php echo site_url() ?>/access/login_logout/social", { fname: first_name, lname: last_name, email: user_email  } ).done(function( data ) {
        var res = JSON.parse(data);
        if(res.state === 'true'){
            var url_to = "<?php echo site_url() ?>/"+res.url;
            window.location = url_to;
        }
    });*/
  }
//=================================================================================================================
  //Initialize the API
window.fbAsyncInit = function() {
    FB.init({
      appId      : '512385022205890',
      status     : true, // check login status
      cookie     : true, // enable cookies to allow the server to access the session
      xfbml      : true  // parse XFBML
    });
};

//Respond to button click
function userLogin(){
    FB.login(function(response) {
       if (response.authResponse) {
           var access_token = response.authResponse.accessToken;
         testAPI();
         
       } 
       else {
         console.log('User cancelled login or did not fully authorize.');
       }
     },{scope: 'email'});
}
 // Load the SDK asynchronously
 (function(d){
   var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
   if (d.getElementById(id)) {return;}
   js = d.createElement('script'); js.id = id; js.async = true;
   js.src = "//connect.facebook.net/en_US/all.js";
   ref.parentNode.insertBefore(js, ref);
  }(document));
 
  //Call the login function
  function testAPI() {
    FB.api('/me', function(response) {
      var first_name = response.first_name;
      var last_name = response.last_name;
      var user_email = response.email;
    
    alert(first_name+' '+last_name+' '+email);
    $.post( "<?php echo site_url() ?>/access/login_logout/social", { fname: first_name, lname: last_name, email: user_email  } ).done(function( data ) {
        var res = JSON.parse(data);
        if(res.state === 'true'){
            var url_to = "<?php echo site_url() ?>/"+res.url;
            window.location = url_to;
        }
    });
    });
    
  }
  </script>
