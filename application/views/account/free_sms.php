<?php 
if($this->session->userdata('stat') == 0){ ?>
    <h4 class="text-center text-danger">You cant send SMS until you verify you number, Click Enter now above.</h4>
<?php }else { ?>
    <div class="alert alert-info text-center home_sub">Send Free SMS<span class="pull-right"> <strong ><span id="sms_left"><?php echo $this->session->userdata('sms'); ?></span></strong> SMS left</span></div>
    <form class="col-sm-8 col-sm-offset-2 form-horizontal" id="sms_form" role="form" action="<?php echo site_url(); ?>/account/my_account/send_free_text" method="POST">
        <div class="form-group">
            <div id="sms_error" class="alert alert-success hidden text-center"></div>  
        </div>
        <div class="form-group">
            <label for="sms_num">To: (Mobile Number)</label>
            <div id='sms_num_wr' data-placement="top" title="Use international codes: Eg. 255XXXXXXXXX">
                <input name="sms_num" type="text" class="form-control" id="sms_num" placeholder="" >
            </div>
          </div>
          <div class="form-group">
              <label for="sms_msg">Message</label>
              <textarea id="sms_msg" name="sms_msg" class="form-control" rows="3"></textarea>
          </div>
        <p id='char_count' class="text-right text-success ">You have 143 characters</p>
        <div class="form-group">
          <button id="snd_sms" type="submit" class="btn btn-primary pull-right">Send Message</button>
        </div>
    </form>
<?php } ?>
<script>
    $('#sms_num_wr').tooltip();
    $('#sms_num_wr').tooltip('hide');
    var characters = 143;
    var send = false;
    $( "#sms_msg" ).on("paste keyup cut",function() {
        if($(this).val().length > characters ){
            $('#char_count').removeClass('text-success');
            $('#char_count').addClass('text-danger');
            $('#char_count').html('You have exceeded number of characters');
            send = false;
        }else{
            var left = characters - $(this).val().length;
            $('#char_count').removeClass('text-danger');
            $('#char_count').addClass('text-success');
            $('#char_count').html(left+' characters left');
            send = true;
        }

    });
    
    var maxLen = 143;
        $('#sms_msg').keypress(function(event){
            var Length = $("#sms_msg").val().length;
            if(Length >= maxLen){
                if (event.which !== 141) {
                    return false;
                }
            }
        });
    
    $("#snd_sms").click(function(){
        var t = "<?php echo site_url(); ?>";
        var c = t+"/account/my_account/send_free_text";
        $('#sms_error').html('<img style="height: 35px;" class="center-block" src="<?php echo base_url(); ?>/assets/images/ajax-loader.gif">');
        $('#sms_error').removeClass('hidden');
        setTimeout(function(){
        $.post( c, $("#sms_form").serialize()).done(function(data) {
            if(data === 'true'){
                $('#sms_error').removeClass('alert-danger');
                $('#sms_error').addClass('alert-success');
                $('#sms_error').html('Message Sent!');
                var left  = parseInt($('#sms_left').text()) - 1;
                $('#sms_left').html(left);
                
            }else{
                $('#sms_error').removeClass('alert-success');
                $('#sms_error').addClass('alert-danger');
                $('#sms_error').html(data);
            }
        });}, 300);
        return false;
    });
</script>

