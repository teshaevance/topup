 <div id="verify" class="alert alert-info col-sm-10 col-sm-offset-1 alert-dismissable text-center" style="margin-top: 3px;">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    You have not entered your Mobile Number yet. <a id='enter' href="#" class="alert-link">Enter now!</a>&nbsp;<a id="know_more" href="#" class="alert-link">Know more!</a>
    <p id="more" class="text-left col-sm-offset-2">
        <strong>Why should you give us your mobile number?</strong><br/>
        &nbsp;&nbsp;&nbsp;&nbsp;You get to send 100 free sms.<br/>
        &nbsp;&nbsp;&nbsp;&nbsp;You get important notifications about your orders.<br/>
        &nbsp;&nbsp;&nbsp;&nbsp;You updates on offers and new features.
    </p>
    <p>
    <form class="form-horizontal col-sm-offset-1" role="form" id="phone_form" method="POST" action="<?php echo site_url(); ?>/account/my_account/verify_num">
            <div class="form-group">
              <label for="phone" class="col-sm-4 control-label">Enter Mobile Number</label>
              <div class="col-sm-4" id='phone_wr' data-toggle="tooltip" data-placement="top" title="Use international codes: Eg. 255XXXXXXXXX">
                <input name='phone' type="text" class="form-control" id="phone">
              </div>
              <div id='snd_phn' class=" col-sm-4">
              <button id="send_btn" type="submit" class="btn btn-success pull-left hidden">Send</button>
              <div id='err'></div>
              </div>
            </div>  
        </form>
    </p>
</div>
<script>
$(document).ready(function(){
    $('#more').hide();
    $('#phone_form').hide();
    $('#know_more').on('mouseover click', function () {
            $('#more').slideToggle(500);
            return false;
    });
    $('#enter').on('click', function () {
            $('#phone_form').slideToggle(500);
            return false;
    });
     $('#phone_wr').tooltip();
     $( "#phone" ).on("keyup",function() {
        $('#phone_wr').tooltip('hide');
        if($.isNumeric($('#phone').val()) === true && $('#phone').val().length === 12){
            $('#send_btn').removeClass('hidden');
        }
    });
    
    $('#send_btn').on('click', function () {
            $("#err").html('<img style="height: 35px;" class="" src="<?php echo base_url(); ?>/assets/images/ajax-loader.gif">');
            $('#send_btn').html('Sending..');
            
            //send post
            setTimeout(function(){
                var t = "<?php echo site_url(); ?>";
                var c = t+"/account/my_account/enter_num";
                 $.post( c, $("#phone_form").serialize()).done(function(data) {
                        if(data == 'true'){
                            $("#err").html('<label class="text-success text-left control-label">Number Entered!</label>');
                            $('#send_btn').addClass('hidden');
                            setTimeout(function(){$('#verify').hide();},3000);
                            setTimeout(function(){$('#verify_num').fadeIn(5000);},3000);
                        }else{                   
                           $("#err").html('<label class="text-danger text-left control-label">'+data+'</label>');
                        }
                });
             },500);
            return false;
    });
});
</script>