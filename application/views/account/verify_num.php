
<div id="verify_num" class="alert alert-warning col-sm-10 col-sm-offset-1 alert-dismissable text-center " style="margin-top: 3px;">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    You have not verified your Mobile Number yet. <a id='en_cd' href="#" class="alert-link">Enter code now!</a>&nbsp;<a id="_more" href="#" class="alert-link">Know more!</a>
    <p id="moree" class="text-left col-sm-offset-2">
        <strong>Why should you give us your mobile number?</strong><br/>
        &nbsp;&nbsp;&nbsp;&nbsp;You get to send 100 free sms.<br/>
        &nbsp;&nbsp;&nbsp;&nbsp;You get important notifications about your orders.<br/>
        &nbsp;&nbsp;&nbsp;&nbsp;You updates on offers and new features.
    </p>
      <form class="form-horizontal col-sm-offset-1" role="form" id="cd_form" method="POST" action="<?php echo site_url(); ?>/account/my_account/verify_num">
            <div class="form-group">
              <label for="phone" class="col-sm-4 control-label">Enter code</label>
              <div class="col-sm-4" id='cd-wr'>
                <input name='cd' type="text" class="form-control" id="cd">
              </div>
              <div id='snd_cd' class=" col-sm-4">
              <button id="send_cd_btn" type="submit" class="btn btn-success pull-left hidden">Send</button>
              <div id='err2'></div>
              </div>
            </div>  
        </form>
    </p>
</div>
<?php if(isset($phn)){ ?>
<script>
    $('#verify_num').hide();
</script>
<?php } ?>
<script>
$(document).ready(function(){
    $('#cd_form').hide();
    $('#moree').hide();
    $('#en_cd').on('click', function () {
            $('#cd_form').slideToggle(500);
            return false;
    });
    
    $('#_more').on('mouseover click', function () {
            $('#moree').slideToggle(500);
            return false;
    });

     $( "#cd" ).on("keyup",function() {
        $('#cd_wr').tooltip('hide');
        if($('#cd').val().length === 5){
            $('#send_cd_btn').removeClass('hidden');
        }
    });
    
    $('#send_cd_btn').on('click', function () {
            $("#err2").html('<img style="height: 35px;" class="" src="<?php echo base_url(); ?>/assets/images/ajax-loader.gif">');
            $('#send_cd_btn').html('Sending..');
            
            //send post
            setTimeout(function(){
                var t = "<?php echo site_url(); ?>";
                var c = t+"/account/my_account/verify_num";
                 $.post( c, $("#cd_form").serialize()).done(function(data) {
                        if(data == 'true'){
                            $("#err2").html('<label class="text-success text-left control-label">Verified!</label>');
                            $('#send_cd_btn').addClass('hidden');
                            setTimeout(function(){$('#verify_num').fadeOut(5000);},10000);
                        }else{                   
                           $("#err2").html('<label class="text-danger text-left control-label">'+data+'</label>');
                        }
                });
             },500);
            return false;
    });
});
</script>