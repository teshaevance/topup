<!-- Nav tabs -->
<ul class="nav nav-tabs">
  <li class="active"><a href="#acc" data-toggle="tab">Account Information</a></li>
  <li><a href="#pass" data-toggle="tab">Change Password</a></li>
</ul>

<!-- Tab panes -->
<div class="tab-content">
  <div class="tab-pane active" id="acc">
  		<form class="form-horizontal" role="form" >
  			<br/>
  			<div class="form-group">
			    <label class="col-sm-2 control-label">First Name</label>
			    <div class="col-sm-10">
			      <p class="form-control-static"><?php echo $this->session->userdata('fname'); ?></p>
			    </div>
			</div>

			<div class="form-group">
			    <label class="col-sm-2 control-label">Last Name</label>
			    <div class="col-sm-10">
			      <p class="form-control-static"><?php echo $this->session->userdata('lname'); ?></p>
			    </div>
			</div>

			<div class="form-group">
			    <label class="col-sm-2 control-label">Email</label>
			    <div class="col-sm-10">
			      <p class="form-control-static"><?php echo $this->session->userdata('username'); ?></p>
			    </div>
			</div>

			<div class="form-group">
			    <label class="col-sm-2 control-label">Mobile <br/> (<?php if($this->session->userdata('stat') == 1){ echo 'Verified';}else{ echo 'Unverified';} ?>)</label>
			    <div class="col-sm-10">
			      <p class="form-control-static"><?php echo $this->session->userdata('phone'); ?></p>
			    </div>
			</div>


			<div class="form-group">
			    <label class="col-sm-2 control-label">Country</label>
			    <div class="col-sm-10">
			      <p class="form-control-static text-danger">not filled</p>
			    </div>
			</div>


			<div class="form-group">
				<button role="button" class="btn btn-primary col-sm-offset-1">Edit Account Information</button>
			</div>
  		</form>
  </div>

  <div class="tab-pane" id="pass">
		<div class="col-sm-8 col-sm-offset-2" style="margin-top: 40px;">
			<div class="panel panel-primary">
			  <div class="panel-heading">
			    <h3 class="panel-title">Recover Password</h3>
			  </div>
			  <div class="panel-body">
			    	<form id="pass_form" role="form" action="<?php echo site_url("account/my_account/change_password"); ?>" method="POST">
			    		<div id="msg_pass"></div>
			    		<div class="form-group">
			    			<label class="control-label">Enter Old Password</label>
			    			<input name="old_password"  type="password" class="form-control">
			    			<?php show_form_error('old_password'); ?>
			    		</div>

			    		<div class="form-group">
			    			<label class="control-label">Enter New Password</label>
			    			<input name="password"  type="password" class="form-control">
			    			<?php show_form_error('password'); ?>
			    		</div>
			    		<div class="form-group">
			    			
			    			<label class="control-label">Confirm New Password</label>
			    			<input name="password_con"  type="password" class="form-control">
			    			<?php show_form_error('password_con'); ?>
			    		</div>
			    		<div class="form-group">
			    			<button id="save_pass" type="submit" class="btn btn-primary pull-right">Save</button>
			    		</div>
			    	</form>
			  </div>
			</div>
		</div>
  </div>
</div>

<script>
	$(document).ready(function(){
		$('#save_pass').click(function(){
			var t = "<?php echo site_url(); ?>";
	        var c = t+"/account/my_account/change_password";
	        $('#msg_pass').html('<img style="height: 35px;" class="center-block" src="<?php echo base_url(); ?>/assets/images/ajax-loader.gif">');
        	
        	setTimeout(function(){
             $.post( c, $("#pass_form").serialize()).done(function(data) {
             	if(data.status == 'true'){
             		$('#msg_pass').html('<div class="alert alert-success">Password has been changed</div>');
             	}else if(data.status == 'false'){
             		$('#msg_pass').html('<div class="alert alert-danger">'+data.data+'</div>');
             	}
            },'json');
         },400);

        	return false;
		});
	});
</script>