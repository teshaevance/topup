<?php if(!isset($no_data)){ ?>
<div id="wrap">
<div id='trans'>
    <div class="table-responsive">
    <table class="table text-center table-striped">
        <tr>
            <th>Reference</th>
            <th>Number</th>
            <th>Amount sent</th>
            <th>Operator</th>
            <th>Date</th>
            <th colspan="2">Action</th>
        <?php foreach ($trans as $value){ ?>
                    <tr>
                        <td><?php echo $value['transaction_id']; ?></td>
                        <td><?php echo $value['number']; ?></td>
                        <td><?php echo $value['amount']; ?></td>                        
                        <td><?php echo ucfirst($value['dest_operator_id']).'-'.$value['dest_country_name']; ?></td>                        
                        <td><?php echo $value['time']; ?></td>
                        <?php if(isset($value['pay_method'])){ ?>
                        <td colspan="2"><button id="<?php echo $value['trans_id']; ?>" class="btn btn-primary view_btn">view</button></td>
                        <?php }else{ ?>
                        <td><a href="<?php echo site_url().'/recharge/confirm/get_for_pay/'.$value['trans_id'];?>" class="btn btn-danger pay_btn">Pay</a></td>
                        <td><button id="<?php echo $value['trans_id']; ?>" class="btn btn-primary view_btn">View</button></td>
                        <?php } ?>
                    </tr>
                <?php } ?>
    </table>
</div>
</div>
<div id="view_trans" class="hidden">
    <button id="back" class="btn btn-info _bottom col-sm-2 pull-left"><span class="glyphicon glyphicon-arrow-left push_right_bit"></span>Order List</button>
    <a id="pay" href="<?php echo site_url().'/recharge/confirm/get_for_pay/'.$value['trans_id'];?>" class="btn btn-info _bottom col-sm-2 pull-right">Pay<span class="glyphicon glyphicon-arrow-right push_left_bit"></span></a><br/>
    <div class="clearfix"></div>
    <div id="invoice"></div>
</div>
</div>
<?php }else{
    echo '<div class="alert alert-info text-center ">You Have Not Placed Any Orders</div>';
} ?>
<script>
$(document).ready(function(){
    $(".view_btn").click(function(){
        var x = $(this).attr('id');
        $.post( "<?php echo site_url(); ?>/recharge/view_trans/index/"+x, function( data ) {
            $( "#invoice" ).html(data);
            $('#trans').addClass('hidden');
            $('#view_trans').removeClass('hidden');
        });
        return false;
    });
    $("#back").click(function(){
        $('#trans').removeClass('hidden');
        $('#view_trans').addClass('hidden');
        return false;
    });
});
</script>
    