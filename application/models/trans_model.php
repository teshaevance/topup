<?php
    class Trans_model extends CI_Model{
        public function insert_trans($data){
             $query = $this->db->insert('transaction', $data);
             if($query){
                $q = $this->db->query('SELECT LAST_INSERT_ID()');
                $row = $q->row_array();
                return $row;
             }
        }
        
        public function get_trans($id) {
            $this->db->limit(15);
            $this->db->order_by("time", "desc"); 
            $query = $this->db->get_where('transaction', array('sender_user_id' => $id));
            if ($query->num_rows() > 0){
                return $query->result_array();
            }
        }
        public function get_trans_id($id) {
           //check for filter 
            $this->db->select('*');
            $this->db->from('transaction');
            $this->db->join('user', 'user.user_id = transaction.sender_user_id');
            $this->db->where(array('trans_id' => $id));

            $query = $this->db->get();
            return $query->row_array();
        }


        public function get_trans_ref($id) {
            $this->db->select('*');
            $this->db->from('transaction');
            $this->db->join('user', 'user.user_id = transaction.sender_user_id');
            $this->db->where(array('transaction_id' => $id));
            
            $query = $this->db->get();
            return $query->row_array();
        }
        
        public function get_last(){
             $query =  $this->db->query("SELECT trans_id FROM transaction ORDER BY trans_id DESC LIMIT 1");
             $row = $query->row_array();
             return $row;
        }
        
        public function update_pay($id, $data) {
            $this->db->where('trans_id', $id);
            $this->db->update('transaction', $data);
            $this->up_time($id);
        }
        
        public function up_time($id) {
            $this->db->query("update transaction set time=now() where trans_id=$id");
        }


        public function getAll($and_filters,$or_filters){
            //check for filter 
            $this->db->select('*');
            $this->db->from('transaction');
            $this->db->join('user', 'user.user_id = transaction.sender_user_id');

            //check for filters
            if($and_filters != null){
                $this->db->where($and_filters);    
            }

            if($or_filters !=  null){
                $this->db->or_where(array('pay_method' => 'visa', 'pay_method' => 'mastercard'));
            }

            $query = $this->db->get();
            return $query->result_array();
        }


        public function get_for_edit($id){
            //check for filter 
            $this->db->select('*');
            $this->db->from('transaction');
            $this->db->join('user', 'user.user_id = transaction.sender_user_id');
            $this->db->where(array('trans_id' => $id));

            $query = $this->db->get();
            return $query->row_array();

        }


         public function monthly_charges_network($and_filters,$like_filters){
            //check for filter 
            $this->db->select('*');
            $this->db->from('transaction');

            //check for filters
            if($and_filters != null){
                $this->db->where($and_filters);    
            }

            if($like_filters !=  null){
                $this->db->like($like_filters);
            }

            $query = $this->db->get();
            $res = $query->result_array();
            $total = 0;

            foreach ($res as $key => $value) {
                $total = $total  + $value['charge'];
            }


            return $total;
        }


        public function monthly_volume($and_filters,$like_filters){
            //check for filter 
            $this->db->select('*');
            $this->db->from('transaction');

            //check for filters
            if($and_filters != null){
                $this->db->where($and_filters);    
            }

            if($like_filters !=  null){
                $this->db->like($like_filters);
            }

            $query = $this->db->get();
            $res = $query->result_array();
            $total = 0;

            foreach ($res as $key => $value) {
                $total = $total  + $value['amount'];
            }


            return $total;
        }



        public function total_charges(){
            $this->db->select('*');
            $this->db->from('transaction');
            $this->db->where(array('status' => 'delivered'));

            $query = $this->db->get();
            $res =  $query->result_array();

            $total = 0;
            foreach ($res as $key => $value) {
                $total = $total + $value['charge'];
            }


            return $total;
        }


        public function total_volume(){
            $this->db->select('*');
            $this->db->from('transaction');
            $this->db->where(array('status' => 'delivered'));

            $query = $this->db->get();
            $res =  $query->result_array();

            $total = 0;
            foreach ($res as $key => $value) {
                $total = $total + $value['amount'];
            }


            return $total;
        }


        public function monthly_net_stat($and_filters,$like_filters){
            //check for filter 
            $this->db->select('*');
            $this->db->from('transaction');

            //check for filters
            if($and_filters != null){
                $this->db->where($and_filters);    
            }

            if($like_filters !=  null){
                $this->db->like($like_filters);
            }

            $query = $this->db->get();
            $res = $query->num_rows();
            

            return $res;
        }
    }