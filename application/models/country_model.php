<?php
    class Country_model extends CI_Model{
        //matches existence of a t user using username and password
        function get_country_field($match,$input,$return){
            $query = $query = $this->db->get_where('country', array($match => $input,'active' => '1'));
                if ($query->num_rows() > 0){
                    $row = $query->row_array();
                    return $row[$return];
                }
        }
        function get_country_iso($id){
            $query = $query = $this->db->get_where('country', array('id' => $id));
                if ($query->num_rows() > 0){
                    $row = $query->row_array();
                    return $row['iso'];
                }
        }
        function get_country_code($id){
            $query = $query = $this->db->get_where('country', array('id' => $id));
                if ($query->num_rows() > 0){
                    $row = $query->row_array();
                    return $row['phonecode'];
                }
        }
        function get_country_name($id){
            $query = $query = $this->db->get_where('country', array('id' => $id));
                if ($query->num_rows() > 0){
                    $row = $query->row_array();
                    return $row['nicename'];
                }
        }
    }
