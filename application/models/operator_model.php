<?php
    class Operator_model extends CI_Model{
        public function get_operators($code){
            //fetch country id first
            $this->load->model('country_model');
            $con_id = $this->country_model->get_country_field('iso',$code,'id');
            
            //get the operators
            $query = $query = $this->db->get_where('operator', array('country_id' => $con_id));
                if ($query->num_rows() > 0){
                    return $query->result_array();
                }
            
        }
        
        public function match_operator($id){
            $query = $query = $this->db->get_where('operator', array('operator_id' => $id));
                if ($query->num_rows() > 0){
                    $row = $query->row_array();
                    return $row['operator_name'];
                }
        }
        
        public function get_operator_id($name){
            $query = $query = $this->db->get_where('operator', array('operator_name' => $name));
                if ($query->num_rows() > 0){
                    $row = $query->row_array();
                    return $row['operator_id'];
                }
        }
        
        public function match_pre($pre){
            $query = $query = $this->db->get_where('operator_prefix', array('prefix' => $pre));
                if ($query->num_rows() > 0){
                    $row = $query->row_array();
                    return $row['op_id'];
                }
        }
        
        public function get_cash_list($id) {
            $query = $query = $this->db->get_where('operator_cash_list', array('operator_id' => $id));
                if ($query->num_rows() > 0){
                    $row = $query->result_array();
                    return $row;
                }
        }
        
        public function get_charge_rate($id){
            $query = $query = $this->db->get_where('operator', array('operator_id' => $id));
                if ($query->num_rows() > 0){
                    $row = $query->row_array();
                    return $row['charge_fraction'];
                }
        }
    }