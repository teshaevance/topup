<?php
    class User_model extends CI_Model{
        //matches existence of a t user using username and password
        function match_user($username, $password){
             $query = $this->db->get_where('user', array('email' => $username,'password' => $password));
                if ($query->num_rows() > 0){
                    return $query->result_array();
                }
        }
        
        function new_user($data){
            $query = $this->db->get_where('user', array('email' => $data['email']));
            $query->result_array();
            if($query->num_rows() > 0){
                return FALSE;
            }else{
               return $this->db->insert('user', $data);
               } 
        }
        
        function update_field($id,$data){
            $this->db->where('user_id', $id);
            return $this->db->update('user', $data); 
        }
        
        function check_exist($email){
            $query = $this->db->get_where('user', array('email' => $email));
            $query->result_array();
            if($query->num_rows() > 0){
                return TRUE;
            }else{
               return FALSE;
               }
        }

        function set_recovery_token($email,$data){
            $this->db->where('email', $email);
            return $this->db->update('user', $data); 
        }


        function match_field($filter){
            $this->db->select('*');
            $this->db->from('user');
            $this->db->where($filter);
            
            $query = $this->db->get();
            return $query->row_array();
        }


        public function mon_register($and_filters,$like_filters){
            //check for filter 
            $this->db->select('*');
            $this->db->from('user');

            //check for filters
            if($and_filters != null){
                $this->db->where($and_filters);    
            }

            if($like_filters !=  null){
                $this->db->like($like_filters);
            }

            $query = $this->db->get();
            $res = $query->num_rows();
            


            return $res;
        }


        public function get_all(){
            $this->db->select('*');
            $this->db->from('user');

            $query = $this->db->get();
            $res = $query->num_rows();
            


            return $res;

        }

}
