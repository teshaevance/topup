<?php

/* 
 * Author: Kilaja Annastella
 * Desscription: Globally (some) available validation rules 
 * Comments: Can add rules, make sure no conflict, no editing without consultation 
 */


$config = array(
             'login' => array(
               array(
                     'field'   => 'username', 
                     'label'   => 'Username', 
                     'rules'   => 'trim|required'
                  ),
               array(
                     'field'   => 'password', 
                     'label'   => 'Password', 
                     'rules'   => 'trim|required|md5'
                  )),
    
             'reg' => array(
               array(
                     'field'   => 'fname', 
                     'label'   => 'First Name', 
                     'rules'   => 'trim|required'
                  ),
               array(
                     'field'   => 'lname', 
                     'label'   => 'last Name', 
                     'rules'   => 'trim|required'
                  ),
               array(
                     'field'   => 'email', 
                     'label'   => 'Email', 
                     'rules'   => 'trim|required'
                  ),
               array(
                     'field'   => 'phone', 
                     'label'   => 'Phone', 
                     'rules'   => 'numeric|trim'
                  ),
               array(
                     'field'   => 'pass', 
                     'label'   => 'Password', 
                     'rules'   => 'trim|required|matches[pass_con]|md5'
                  ),
               array(
                     'field'   => 'pass_con', 
                     'label'   => 'Password confirm', 
                     'rules'   => 'trim|required|md5'
                  ))
    
            );

